# MVIAnalyzer
MVI - Missing Value Imputation

Python source code of MVIAnalyzer Software - created as part of Kai Tejkl's Master's thesis at dbis chair of University of Hagen, supervised by Valerie Restat

## Technical Remarks
*   Created as Eclipse-PyDev-Project on Eclipse IDE 2022-09 (4.25.0) with PyDev-Plugin 10.0.1.
*   Developed in Python 3.10.8.
*   For dependencies check requirements.txt.

## Description
The MVIAnalyzer is a generic approach that can be used to holistically investigate MVI. Multiple constraints are taken into account. These include different characteristics of incomplete data as well as various MVI and ML methods. The MVIAnalyzer was developed particularly against the background of mapping the entire process - from data import and generation, MVS, MVI and the machine learning application. Execution is based on the process presented in the following picture:
![Process of MVIAnalyzer](MVIAnalyzer-process.png "Process of MVIAnalyzer")

The generic approach is implemented by means of a class-based, object-oriented implementation with a corresponding flow script. The calculation process can be carried out specifically in a few steps. Re-use of individual classes is possible. Moreover, the integration of further MVI and ML procedures is possible by the additional supply of appropriate classes. The generalization of the class for data loading also enables the integration of further data formats by implementing the corresponding interfaces. The whole execution can be adapted to different researches by configuration files. These files are created in YAML format. The implementation is done in Python. Additional, freely available packages were used (see requirements.txt). Thus, it can be applied and extended universally.

## Demo Run
The demo run starts *MVIAnalyzer* with the default configuration settings and performs an analysis based on *PersonPseudo Data*. The main configuration settings for the demo run include:
* Random generation of *PersonPseudoData* according to *pseudodata* section in conf/conf_global.yaml
* MVS (missing value simulation), MVI (missing value imputation) and ML (machine learning) methods according to default configuration settings in conf/conf_session_default.yaml
    - MVS with
        - Mixed multivariate pattern and MCAR on *age* and *weight* attribute and univariate pattern and MAR on *salary* attribute.
        - Two multivariate patterns and MCAR on *gender* and *salary* attribute and on *age* and *weight* attribute.
    - MVI with
        - Mixed with *mean* on *age* and *weight* attribute and *median* on *gender* and *salary* attribute.
        - *Mean* method on all missing values.
        - *KNNI* method on all missing values.
        - *Multiple MLP* method on all missing values with three hidden layers and 256/128/64 neurons.
    - ML with
        - *MLP* on original, imputed, zerofilled and deleted data with three hidden layers and 256/128/64 neurons.
        - *SOM* on imputed data with map size rows/cols = 10.

Start Demo Run
1. Create the log directory: 
    ```bash
    mkdir log
    ```
2. Run the Docker Container to perform a MVIAnalyzer Demo Run:
    ```bash
    docker run --rm -it --user $(id -u):$(id -g) \
    --mount type=bind,source="$(pwd)/log",target=/app/MVIAnalyzer/log \
    --mount type=bind,source="$(pwd)/conf",target=/app/MVIAnalyzer/conf \
    --mount type=bind,source="$(pwd)/data",target=/app/MVIAnalyzer/data \
    registry.gitlab.com/d6745/mvianalyzer/mvianalyzer:latest
    ```

## Own Analysis
Alternatively, run MVIAnalyzer directly in Bash:
1. Create the log directory: 
    ```bash
    mkdir log
    ```
2. Run MVIAnalyzer directly in Bash with:
    ```bash
    docker run --rm -it --user $(id -u):$(id -g) \
    --entrypoint "/bin/bash" \
    --mount type=bind,source="$(pwd)/log",target=/app/MVIAnalyzer/log \
    --mount type=bind,source="$(pwd)/conf",target=/app/MVIAnalyzer/conf \
    --mount type=bind,source="$(pwd)/data",target=/app/MVIAnalyzer/data \
    registry.gitlab.com/d6745/mvianalyzer/mvianalyzer:latest
    ```
3. Start MVIAnalyzer with your configuration 
	```bash
	python3 ./src/main/Start.py <SessionConfigurationFile>.yaml info <file | all>
	```

### Add own Data and Configuration
To perform an analysis on own data, place it in the `data` folder. Then create a corresponding configuration file and place it in the `conf` folder. This can then be passed to the start script.

There are two types of configuration: *global* and *session*

#### Global Configuration
The file has to be named `conf_global.yaml`! The configuration is divided into four sections:
- *general*: Contains basic settings. Among other things, you can specify here which device is used to perform the ANN procedure. The randomseed parameter makes it possible to define a standard initialization value for all methods used within the procedure that use a random generator. This allows reproducible results to be achieved.
- *dataanalyer*: Data preparation information
- *dataplotter*: Settings for additional output of different plots during the application
- *pseudodata*: Parameter for generating pseudo personal data if the data generator is used

#### Session Configuration
The session configuration is created individually for each data source under consideration. In accordance with the process flow, this was designed in such a way that, in addition to basic settings, it is possible to specify various MVS cases and MVI or ML methods. It is divided in the following sections:
- *general*: In addition to the data source, also defines key parameters for data preparation. This is the central entry point for the evaluation. The individual parameters include:
    - *datapath*: Directory of the data file. This is also used for the subsequent output and must therefore be specified and present. Remember to put your data in the `data` directory.
    - *datafilename*: Name of the data file in the datapath. If `null` is specified, corresponding pseudo personal data is generated and stored in the datapath.
    - *dropcols*: Enables the specification of individual attribute names in list form, which are removed from the original data for further evaluation.
    - *categoricalcols*: Overrides the automatic detection of categorical attributes for one or more attributes and classifies them as categorical accordingly
    - *encodecols*: Defines the attributes to be encoced.
    - *encoder*: Defines the encoding - `le` for Label Encoding and `ohe` for One Hot Encoding.
    - *idcolname*: Enables the specification of an attribute name for use as a unique ID. If the parameter is not set, a separate unique ID is generated.
- *dataplotter*: Settings for outputting plots of the original data (if provided for in the global configuration). The seaborn package is used for this purpose. `jointplot`, `pairplot`, `catplot` and `relplot` are supported. The respective parameters correspond to the seaborn parameters. They are specified in list form, which means that several plots of the same category are possible.
- *mvisim*: Details of the parameters for performing the MVS, essentially in accordance with the requirements of the PyAmpute package. The specification is made in list form below the amputer parameter. A corresponding MissingData instance is created for each specification. The parameters are divided into:
    - *prop*: Defines the *missing rate*
    - *pattern*: Definition of one or more simultaneously applicable missing patterns for one or more attributes `missattr`, taking into account the missing mechanism and the respective `freq`. For a detailed description, please refer to the PyAmpute documentation according to [SZS22](#SZS22).
- *mviexecutor*: Definition of one or more MVI methods to be applied to the amputated data. To do this, one or more `pipeline` entries are first specified. Each pipeline is applied to each `MissingData` instance. A pipeline enables the simultaneous use of different methods within an MVI on different attributes. Normally, however, only one method is considered. The respective method is defined by the corresponding parameters:
    - *method*: Defines the method to be used.
    - *enabled*: Enables quick switching to deactivate a method.
    - *methodsettings*: Represents a container for method-specific settings, such as the hyperparameters of ANN-based methods. For the description of further parameters or individual method settings, please refer to the comments in `ConfigSession.py`
- *mlmethod*: Definition of one or more ML methods to be used on different data instances. Each method includes the following information:
    - *method*: Definition of the method.
    - *methodsetting*: Represents a container for method-specific settings (see above).
    - *mvi*: Specification of the respective strategy. The values `imputed` for the use of all `ImputedData` instances (explicit strategy), `delete` or `zerofill` for the implicit strategy or `original` for the use of original data as a basis for comparison are possible here.

### Changes in the Code / Additional Libraries
If you added any changes and/or libraries, you have to build a new Docker Image:
1. Create a new Docker Image with all necessary requirements by using
	```bash
	docker build -t mvianalyzer:<your_tag> .
	```
2. Use own image
    ```bash
    docker run --rm -it --user $(id -u):$(id -g) \
    --entrypoint "/bin/bash" \
    --mount type=bind,source="$(pwd)/log",target=/app/MVIAnalyzer/log \
    --mount type=bind,source="$(pwd)/conf",target=/app/MVIAnalyzer/conf \
    --mount type=bind,source="$(pwd)/data",target=/app/MVIAnalyzer/data \
    mvianalyzer:<your_tag>
    ```

3. Start MVIAnalyzer with your configuration 
    ```bash
    python3 ./src/main/Start.py <SessionConfigurationFile>.yaml info <file | all>
    ```


## Data Model
![Data Model](MVIAnalyzer-datamodel.png "Data Model")
The *DataModel* class represents the basic container for all data arising during the application. As its core, *DataModel* necessarily has an instance of the *DataContent* class, which takes over the data management itself. The individual data classes are derived from the *BaseData* class, which holds the respective data frame (from the pandas package) as a basis. *DataContent* contains exactly one instance of the *OriginalData* and an arbitrary number of *MissingData* instances in which the amputated original data of the missing value simulation are stored. For comparisons, the missing value simulation can thus be performed with different parameters and the results are jointly available. Each *MissingData* instance can also contain an arbitrary number of *ImputedData* instances, which means that different MVI methods can be applied to the individual amputated data. For a later evaluation, it is therefore always known on which data which method was carried out. The data model also contains the independent class *DfDataset*, which provides a PyTorch dataset for a given dataframe for the use of neural networks.

## References
<a id="SZS22">[SZS22]</a>R. Schouten, D. Zamanzadeh und P. Singh. pyampute: a Python library for data amputation. Techn. Ber. 2022. doi: 10.25080/majora-212e5952-03e.