import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import os
import logging
from helpers.logger import initLogger
from datamodel.config.ConfigSession import ConfigSession
from datamodel.config.ConfigGlobal import ConfigGlobal
from helpers.filenames import filepathsuffix

class DataPlotter(object):
    '''
    BaseClass for statistical plots of a dataframe
    '''
    def __init__(self, df: pd.DataFrame, cg: ConfigGlobal, cs: ConfigSession):
        logging.debug('Start Init')
        self.__df = df
        self.__cg = cg
        self.__cs = cs
        sns.set_theme(style=cg.getDataPlotterSeabornTheme(), palette='hsv')
        logging.debug('End Init')
    
    def jointplot(self):
        configs = self.__cs.getDataPlotterJointPlot()
        for i, c in enumerate(configs):
            sns.jointplot(data=self.__df, x=c['x'], y=c['y'], hue=c['hue'], palette='Set1')
            sfx = '_orig_JointPlot_{:>03}'.format(str(i))
            fp = filepathsuffix(fp=self.__cs.getDataFilepath(out=True), suffix=sfx, newExt='png')
            plt.savefig(os.fsdecode(fp), dpi=300)
            logging.info('Exported JointPlot-Diagram of Original Data to ' + os.fsdecode(fp))
            plt.close()
            #plt.show()
        
    def pairplot(self):
        configs = self.__cs.getDataPlotterPairPlot()
        for i, c in enumerate(configs):
            df_red = self.__df.loc[:,c['cols']]
            if c['hue'] is not None:
                df_red[c['hue']] = self.__df[c['hue']]
            if c['hue'] is None:
                sns.pairplot(data=df_red)
            else:
                sns.pairplot(data=df_red, hue=c['hue'], palette='Set1')
            sfx = '_orig_PairPlot_{:>03}'.format(str(i))
            fp = filepathsuffix(fp=self.__cs.getDataFilepath(out=True), suffix=sfx, newExt='png')
            plt.savefig(os.fsdecode(fp), dpi=300)
            logging.info('Exported PairPlot-Diagram of Original Data to ' + os.fsdecode(fp))
            plt.close()            
            #plt.show()

    def catplot(self):
        configs = self.__cs.getDataPlotterCatPlot()
        for i, c in enumerate(configs):
            sns.catplot(data=self.__df, kind=c['kind'], x=c['x'], y=c['y'], hue=c['hue'], split=c['split'], palette='Set1')
            sfx = '_orig_CatPlot_{:>03}'.format(str(i))
            fp = filepathsuffix(fp=self.__cs.getDataFilepath(out=True), suffix=sfx, newExt='png')
            plt.savefig(os.fsdecode(fp), dpi=300)
            logging.info('Exported CatPlot-Diagram of Original Data to ' + os.fsdecode(fp))
            plt.close()
            #plt.show()

    def relplot(self):
        configs = self.__cs.getDataPlotterRelPlot()
        for i, c in enumerate(configs):
            sns.relplot(data=self.__df, x=c['x'], y=c['y'], col=c['col'], hue=c['hue'], style=c['style'], size=c['size'], palette='Set1')
            sfx = '_orig_RelPlot_{:>03}'.format(str(i))
            fp = filepathsuffix(fp=self.__cs.getDataFilepath(out=True), suffix=sfx, newExt='png')
            plt.savefig(os.fsdecode(fp), dpi=300)
            logging.info('Exported RelPlot-Diagram of Original Data to ' + os.fsdecode(fp))
            plt.close()
            #plt.show()
    
if __name__ == '__main__':
    initLogger()
    cg = ConfigGlobal()
    cs = ConfigSession()
    df = pd.read_csv(cs.getDataFilepath())
    dp = DataPlotter(df, cg, cs)
    #dp.jointplot()
    dp.pairplot()
    #dp.catplot()
    #dp.relplot()