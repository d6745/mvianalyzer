import logging
import pandas as pd
import numpy as np
import os
from helpers.logger import initLogger
from datamodel.config.ConfigSession import ConfigSession
from datamodel.config.ConfigGlobal import ConfigGlobal
from helpers.filenames import filepathsuffix

class DataStructure(object):
    '''
    Base Class to calculate and store analyzed OriginalData from DataModel
    '''


    def __init__(self, df: pd.DataFrame, cg: ConfigGlobal, cs: ConfigSession):
        '''
        Constructor
        '''
        logging.debug('Start Init')
        #self.__dm = dm
        #self.__odf = dm.getDataContent().getOriginalData().getDf()
        self.__df = df
        self.__cg = cg
        self.__cs = cs
        self.__colNames = self.__df.columns.to_list()
        self.__colIds = self.__calcColIds()
        self.__colDTypes = []
        self.__colTypes = []
        self.__count = []       #For Number of Values in each col
        self.__nancount = []    #For Number of NaN-Values in each col
        self.__zerocount = []   #For Number of 0-Values in each col (Numeric only!)
        self.__min = []         #Minimum Value or Minimum Len if Object
        self.__max = []         #Maximum Value or Maximum Len if Object
        self.__mean = []        #Mean Value or Mean Len if Object
        self.__iscat = []       #Col assumed categorical or not
        self.__sequential = []  #Col sequential ordered
        self.__uniqueValues = [] #Number of Unique Values in Column
        self.__uniqueFrac = []  #Fraction of Unique Values in relation to all Count of all Values in Column
        
        for colid in self.__colIds:
            ds = self.__df.iloc[:,colid]
            self.__colDTypes.append(self.__getColDType(ds))
            self.__colTypes.append(self.__getColType(ds))
            self.__count.append(self.__getCount(ds))
            self.__nancount.append(self.__getMissingValues(ds))
            self.__zerocount.append(self.__getZeroValue(ds))
            self.__min.append(self.__getMinValue(ds))
            self.__max.append(self.__getMaxValue(ds))
            self.__mean.append(self.__getMeanValue(ds))
            self.__iscat.append(self.__assumeCategorical(ds))
            self.__sequential.append(self.__getOrder(ds))
            self.__uniqueValues.append(self.__getUniqueValues(ds))
            self.__uniqueFrac.append(self.__calcUniqueFrac(ds))
        
        reportdict = {
                'colid': self.__colIds,
                'column': self.__colNames,
                'dtype': self.__colDTypes,
                'type': self.__colTypes,
                'count': self.__count,
                'nancount': self.__nancount,
                'zerocount': self.__zerocount,
                'min': self.__min,
                'max': self.__max,
                'mean': self.__mean,
                'categorical': self.__iscat,
                'sequential': self.__sequential,
                'uniqueValues': self.__uniqueValues,
                'uniquefrac': self.__uniqueFrac
            }
        self.__report = pd.DataFrame(reportdict)
        self.__report.set_index('colid', inplace=True)
        logging.debug('End Init')
        
    def getReport(self) -> pd.DataFrame:
        return self.__report
    
    def getColNames(self) -> list:
        return self.__colNames
    
    def exportReport(self) -> bool:
        '''
        Exports the report to directory of data file
        '''
        r = self.getReport()
        fpout = filepathsuffix(fp=self.__cs.getDataFilepath(out=True), suffix='_DataStructureReport', newExt='csv')
        r.to_csv(os.fsdecode(fpout), sep='\t')
        msg = 'Exported DataStructure Report to ' + os.fsdecode(fpout)
        logging.info(msg)
        return True
        
    def setEncodedCols(self, enccolnames: list) -> bool:
        '''
        Adds Column for encoded Columns to Report
        Because Encoding usually afterwards from DataModel needs this extra function call
        '''
        self.__report['encoded'] = False
        for idx, row in self.__report.iterrows():
            if (row['column'] in enccolnames):
                self.__report.loc[idx, 'encoded'] = True
        return True
    
    def checkIntDTypeForColname(self, colname: str) -> bool:
        '''
        Checks if Dtype of Colname is one of Int-Dtypes
        '''
        x = self.getDTypeForColname(colname)
        intlist = ['int', 'int8', 'int16', 'int32']
        return (x in intlist)
    
    def getDTypeForColname(self, colname: str) -> np.ndarray:
        return self.getStructureForColname(colname)['dtype'].iloc[0]
    
    def getStructureForColname(self, colname: str) -> pd.DataFrame:
        return self.__report[self.__report['column'] == colname]

    def __calcColIds(self) -> list:
        cols = self.__df.shape[1]
        return list(range(cols))
    
    def __getColDType(self, ds: pd.Series) -> str:
        '''
        Returns Pandas Default dType of column with colid
        '''
        return ds.dtype
    
    def __calcUniqueFrac(self, ds: pd.Series) -> float:
        '''
        Calculates the fraction of unique values in Series 
        '''
        return ds.drop_duplicates().shape[0] / ds.shape[0]
    
    def __getUniqueValues(self, ds: pd.Series) -> int:
        '''
        Calculates number of unique values in Series
        '''
        return ds.drop_duplicates().shape[0]
        
    
    def __getColType(self, ds: pd.Series) -> str:
        '''
        Assume the type of a column if Pandas DType is object
        '''
        dt = ds.dtype
        if (dt == 'object'):
            samplefrac = self.__cg.getDataAnalyzerSampleFracTypes()
            samp = ds.sample(frac=samplefrac).to_list()    #Takes 5% of Series to determine Type
            typs = []
            for v in samp:
                typs.append(str(type(v)))
            
            if (all(i == typs[0] for i in typs)):
                return typs[0]
            else:
                return 'MIXED TYPES'
        else:
            return dt
    
    def __getCount(self, ds: pd.Series) -> int:
        return len(ds)
    
    def __getMissingValues(self, ds: pd.Series) -> int:
        '''
        Returns Number of NaN values for specified column Id
        '''
        return ds.isnull().sum()
    
    def __getZeroValue(self, ds: pd.Series) -> int:
        '''
        Returns Number of 0-Values for specified column Id
        '''
        return len(ds[ds==0])
    
    def __getMinValue(self, ds: pd.Series) -> float:
        '''
        Returns Minimum Value of Col if Numeric Col or Minimum Length if Object
        '''
        if (ds.dtype == 'object'):
            return ds.apply(len).min()
        else:
            return ds.min()
        
    def __getMaxValue(self, ds: pd.Series) -> float:
        '''
        Returns Maximum Value of Col if Numeric Col or Maximum Length if Object
        '''
        if (ds.dtype == 'object'):
            return ds.apply(len).max()
        else:
            return ds.max()
        
    def __getMeanValue(self, ds: pd.Series) -> float:
        '''
        Returns Mean Value of Col if Numeric or Mean Length if Object
        '''
        if (ds.dtype == 'object'):
            return ds.apply(len).mean()
        else:
            return ds.mean()
        
    def __assumeCategorical(self, ds: pd.Series) -> bool:
        '''
        Checks based on number of identical values in column if attribute is assumed categorical
        '''
        th = self.__cg.getDataAnalyzerCategoricalThreshold()   #Threshold as Percent of number of values in Series
        length = len(ds)
        num = th * length
        
        #Override optionally Categorical Settings from ConfigSession if for this column is set
        d = self.__cs.getCategoricalCols()
        for k, v in d.items():
            if k == ds.name:
                return v

        #If not found for this column in ConfigSession assume isCategorical        
        if (len(ds.unique()) <= num):
            return True
        else:
            return False
        
    def __getOrder(self, ds: pd.Series) -> str:
        '''
        Checks if column is sequential by checking asc/desc ordering
        '''
        if (ds.dtype != 'object'):
            if ds.diff().fillna(0).ge(0).all():
                return 'ASC ORDERED'
            elif ds.diff().fillna(0).le(0).all():
                return 'DESC ORDERED'
            else:
                return 'NOT ORDERED'
        else:
            return 'NOT NUMERIC'
        
if __name__ == '__main__':
    initLogger()
    cs = ConfigSession()
    cg = ConfigGlobal()
    df = pd.read_csv(os.fsdecode(cs.getDataFilepath()))
    ds = DataStructure(df, cg, cs)
    print(df)
            
        
    
