import pandas as pd
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
import logging

class DataEncoder(object):
    '''
    Base Class to Encode categorical data in a dataframe
    '''


    def __init__(self, df: pd.DataFrame, encodecols: list, encoder: str = 'le'):
        '''
        encoder: String which encoder to use: le = LabelEncoder / ohe = OneHotEncoder
        '''
        logging.debug('Start Init')
        self.__df = df
        self.__encodecols = encodecols
        self.__classes = dict()
        if encoder == 'ohe':
            self.__encoder = OneHotEncoder(drop=None)
            self.__dfenc = self.__encodeOneHot()
        else:
            self.__encoder = LabelEncoder()
            self.__dfenc = self.__encodeLabel()
        logging.debug('End Init')
    
    def getClasses(self) -> dict:
        return self.__classes
    
    def getDfEncoded(self) -> pd.DataFrame:
        return self.__dfenc        
    
    def __encodeOneHot(self) -> pd.DataFrame:
        dfenc = self.__df.copy()
        for ecol in self.__encodecols:
            ohedf = pd.DataFrame(self.__encoder.fit_transform(dfenc[[ecol]]).toarray())
            cat = self.__encoder.categories_[0].tolist()
            self.__classes[ecol] = cat
            enccolnames = []
            for i in range(len(cat)):
                enccolnames.append(str(i) + '_' + ecol)
            ohedf.columns = enccolnames   #Rename Columns from 0,1,2,.. to 0_ecol, 1_ecol, ...
            dfenc = dfenc.join(ohedf)
            dfenc = dfenc.drop(ecol, axis=1)
        return dfenc
    
    def __encodeLabel(self) -> pd.DataFrame:
        dfenc = self.__df.copy()
        for ecol in self.__encodecols:
            dfenc[ecol] = self.__encoder.fit_transform(dfenc[ecol])
            self.__classes[ecol] = self.__encoder.classes_.tolist()
        return dfenc

