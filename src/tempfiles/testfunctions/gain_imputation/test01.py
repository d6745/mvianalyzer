from tempfiles.testfunctions.gain_imputation.data_loader import data_loader
from tempfiles.testfunctions.gain_imputation.utils import normalization
import numpy as np

dx, mdx, dm = data_loader(data_name='letter', miss_rate=0.2)

print(dx[:4,:])
print(mdx[:4,:])
print(dm[:4,:])

x2 = 1-np.isnan(mdx)

mdx_norm, mdx_par = normalization(mdx, parameters=None)
#print(mdx_norm)
#print(mdx_par)

mdx2 = (np.nan_to_num(mdx,0))
print(mdx2[:4,:])
