import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from datamodel.config.ConfigGlobal import ConfigGlobal
from datamodel.config.ConfigSession import ConfigSession
from dataloader.LoaderSelector import LoaderSelector
from itertools import combinations

OUTFILEBASE = r'D:\DATA\07_UCI_eshop\plot\eshop_2008_data' # <- CHANGE PATH

cg = ConfigGlobal()
cs = ConfigSession('conf_07_01_TEST.yaml')
ls = LoaderSelector(cs.getDataFilepath())
dl = ls.getLoader(cs, cg)
dl.load()
dm = dl.getDataModel()
df = dm.getDataContent().getOriginalData().getDf()
print(df)

attrs = ['datelong', 'order', 'country', 'sessionID', 'page1', 'page2', 'colour', 'location', 'model', 'price', 'price2', 'page']
list_comb = list(combinations(attrs, 2))


#sns.set_theme(style='whitegrid', palette='Set1')
for i, comb in enumerate(list_comb):
    outsuffix = '_scatter_' + '{0:0>3}'.format(i) + '.png'
    outfile = OUTFILEBASE + outsuffix
    print(outsuffix + '\t' + comb[0] + '\t' + comb[1])
    fig, ax = plt.subplots()
    ax.scatter(df[comb[0]], df[comb[1]], marker='x', s=0.5 )
    ax.set_xlabel(comb[0])
    ax.set_ylabel(comb[1])
    #ax.grid()
    #sns.jointplot(data=df, x=comb[0], y=comb[1], palette='Set1', marker='.')
    #plt.gcf().set_size_inches(5,5)
    plt.tight_layout()
    plt.savefig(outfile, dpi=300)
    plt.close()

for i, a in enumerate(attrs):
    outsuffix = '_hist_' + '{0:0>3}'.format(i) + '.png'
    outfile = OUTFILEBASE + outsuffix
    print(outsuffix + '\t' + a)
    fig, ax = plt.subplots()
    ax.hist(df[a])
    ax.set_xlabel(a)
    #ax.set_ylabel(comb[1])
    #ax.grid()
    #sns.jointplot(data=df, x=comb[0], y=comb[1], palette='Set1', marker='.')
    #plt.gcf().set_size_inches(5,5)
    plt.tight_layout()
    plt.savefig(outfile, dpi=300)
    plt.close()

df2 = dm.getDataContent().getOriginalData().getDfComplete()
df2['Number'] = 1
df2_p = df2.pivot_table(values=['Number'], index=['country'], aggfunc=np.sum)
print(df2_p)
ax = df2_p.plot.bar()
ax.set_ylabel('Frequency')
for p in ax.patches:
    ax.annotate(str(p.get_height()), (p.get_x() * 1.005, p.get_height() * 1.005))

outfile = OUTFILEBASE + '_bar_country.png'
plt.gcf().set_size_inches(15,8)
plt.tight_layout()
plt.savefig(outfile, dpi=300)
#plt.show()
