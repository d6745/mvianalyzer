from others.getDfByConf import getDfByConf
from mvisim.pyampute import MultivariateAmputation
from mvisim.pyampute.exploration.md_patterns import mdPatterns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

CONF = None #'conf_08.yaml'
RS = 123
PATTERNS = [{'incomplete_vars': ['age'],
      'weights': {
          'age': 0.0,
          'weight': -100.0,
          'gender': 0.0,
          'salary': 100.0,
          'state': 0.0,
          'class': 0.0
          },
      'mechanism': 'MAR',
      'freq': 1.0, #0.7,
      'score_to_probability_func': 'sigmoid-right'
    }]

'''
    {'incomplete_vars': ['age', 'salary'],
      'weights': {
          'age': 0,
          'weight': 1,
          'gender': 1,
          'salary': 0,
          'state': 1,
          'class': 1
          },
      'mechanism': 'MAR',
      'freq': 0.3,
      'score_to_probability_func': 'sigmoid-right'
    }]
    '''

#LOAD DATASET
df = getDfByConf(CONF)
print(df)
ma = MultivariateAmputation(prop=0.3, patterns=PATTERNS, seed=RS)
df_amp = ma.fit_transform(df)

'''
dl = []
for i in range(100):
    ma = MultivariateAmputation(prop=0.3, patterns=PATTERNS, seed=RS)
    df_amp = ma.fit_transform(df)
    d = {'run': i,
         'amp_mean_age': df_amp['age'].mean(skipna=True),
         'orig_mean_age': df['age'].mean()}
    dl.append(d)
    
df_res = pd.DataFrame.from_dict(data=dl)
print(df_res)
'''

#print(df_amp)
mdp = mdPatterns()
patterns, fig = mdp.get_patterns(df_amp, count_or_proportion='count', show_plot=True)
#print(patterns)
#print(type(patterns))
plt.show()

