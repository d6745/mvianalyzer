'''
Plots Histogramm of Number of Missing Values for a given Prop over multiple amputation runs
'''

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats
from tqdm import tqdm
from mvisim.pyampute import MultivariateAmputation
from mvisim.pyampute.exploration.md_patterns import mdPatterns
from mvisim.pyampute.exploration.mcar_statistical_tests import MCARTest
from datamodel.config.ConfigGlobal import ConfigGlobal
from datamodel.config.ConfigSession import ConfigSession
from dataloader.LoaderSelector import LoaderSelector

RANDOMINIT = 123
PROP = 0.5
MAXDIFFWITHTARGET = 0.001
BINS = 11
PROBFUNC = 'sigmoid-right'
#PROBFUNC = 'sigmoid-left'
#PROBFUNC = 'sigmoid-mid'
#PROBFUNC = 'sigmoid-tail'
OUTFILE = r'D:\_TEMP\pyampute_missing_pattern_sample_' # <- CHANGE PATH
PAR = [[{'incomplete_vars': ['age', 'weight'],
      'weights': {'age': 0,'weight': 0,'gender': 0,'salary': 0,'state': 0,'class': 0},
      'mechanism': 'MCAR',
      'freq': 1.0, #0.5,
      'score_to_probability_func': PROBFUNC
    }],
    [{'incomplete_vars': ['age', 'weight'],
      'weights': {'age': 0,'weight': 0,'gender': 0,'salary': 0,'state': 0,'class': 0},
      'mechanism': 'MCAR',
      'freq': 0.3,
      'score_to_probability_func': PROBFUNC
    },
    {'incomplete_vars': ['weight'],
      'weights': {'age': 0,'weight': 0,'gender': 0,'salary': 0,'state': 0,'class': 0},
      'mechanism': 'MCAR',
      'freq': 0.7,
      'score_to_probability_func': PROBFUNC
    }]]

cg = ConfigGlobal()
cs = ConfigSession(None)
ls = LoaderSelector(cs.getDataFilepath())
dl = ls.getLoader(cs, cg)
dl.load()
dm = dl.getDataModel()
df = dm.getDataContent().getOriginalData().getDf()
print(df)

misscounter = {'agenans': [],
               'weightnans': []}    #Counter for Number of Missing Values
for i, p in enumerate(tqdm(PAR)):
    ma = MultivariateAmputation(prop=0.5, patterns=p, seed=RANDOMINIT, max_diff_with_target=MAXDIFFWITHTARGET)
    df_amp = ma.fit_transform(df)
    #print(df_amp)
    misscounter['agenans'].append(df_amp['age'].isna().sum())
    misscounter['weightnans'].append(df_amp['weight'].isna().sum())
    mdp = mdPatterns()
    patterns, fig = mdp.get_patterns(df_amp, count_or_proportion='count', show_plot=True)
    #print(patterns)
    #print(type(patterns))
    plt.gcf().set_size_inches(5, 5)
    plt.tight_layout()
    plt.savefig(OUTFILE + str(i) + '.png', dpi=300)
    #plt.show()
    plt.close()
