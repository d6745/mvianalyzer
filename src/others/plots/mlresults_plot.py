import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from helpers.filenames import filepathsuffix
import os

# CHANGE PATH ->
FILEPATH = [r'D:\Data\00_PseudoData\FINALRUNS_v2\01_PseudoData_MLResult_overview.csv',
            r'D:\Data\00_PseudoData\FINALRUNS_v2\02_PseudoData_MLResult_overview.csv',
            r'D:\Data\00_PseudoData\FINALRUNS_v2\03_PseudoData_MLResult_overview.csv',
            r'D:\Data\00_PseudoData\FINALRUNS_v2\04_PseudoData_MLResult_overview.csv',
            r'D:\Data\00_PseudoData\FINALRUNS_v2\05_PseudoData_MLResult_overview.csv',
            r'D:\Data\00_PseudoData\FINALRUNS_v2\06_PseudoData_MLResult_overview.csv',
            r'D:\Data\00_PseudoData\FINALRUNS_v2\07_PseudoData_MLResult_overview.csv',
            r'D:\Data\00_PseudoData\FINALRUNS_v2\08_PseudoData_MLResult_overview.csv',
            r'D:\Data\00_PseudoData\FINALRUNS_v2\09_PseudoData_MLResult_overview.csv']

AMPCONFID = [0] #list(range(60))
IMPCONFID = [0,1,2,3,4,5,6,7,8,9,10,11,12]
    

for fp in FILEPATH:
    df = pd.read_csv(fp, sep='\t')
    df.fillna(value = {'impmethodsettings': ''}, inplace=True)
    df['impmethod_settings'] = df['impmethod'].str.cat(df['impmethodsettings'].values.astype(str), sep=' ')
    df['mlmethod_settings'] = df['method'].str.cat(df['methodsettings'].values.astype(str), sep='\n')

    for ampid in AMPCONFID:
        fileout = filepathsuffix(fp = os.fsencode(fp), suffix='_plot' + str(ampid), newExt='png')
            
        df_org = df.loc[df['mvi'] == 'original']
        print(df_org)
        
        df_zf = df.loc[(df['mvi'] == 'zerofill') & (df['ampconfid'] == ampid)]
        print(df_zf)
        
        df_del = df.loc[(df['mvi'] == 'delete') & (df['ampconfid'] == ampid)]
        print(df_del)
        
        dfl_imp = []
        for ic in IMPCONFID:
            df_imp = df.loc[(df['mvi'] == 'imputed') & (df['ampconfid'] == ampid) & (df['impconfid'] == ic)]
            dfl_imp.append(df_imp)
            print(df_imp)
        
        fig, ax = plt.subplots()
        for df_imp in dfl_imp:
            ax.plot(df_imp['mlmethod_settings'], df_imp['f1macro'], label=df_imp.iloc[0]['impmethod_settings'])
        ax.plot(df_org['mlmethod_settings'], df_org['f1macro'], label=df_org.iloc[0]['impmethod_settings'], linewidth=3, color='black')
        ax.plot(df_zf['mlmethod_settings'], df_zf['f1macro'], label=df_zf.iloc[0]['impmethod_settings'], linewidth=3, dashes=(5,5), color='black')
        ax.plot(df_del['mlmethod_settings'], df_del['f1macro'], label=df_del.iloc[0]['impmethod_settings'], linewidth=3, dashes=(2,2), color='black')
        ax.legend()
        ax.set_xlabel('ML Method')
        ax.set_ylabel('F1 Macro Score')
        ax.set_ylim(0,1)
        ax.grid()
        #plt.show()
        plt.gcf().set_size_inches(10, 5)
        plt.tight_layout()
        plt.savefig(fileout, dpi=300)
