import pandas as pd
import matplotlib.pyplot as plt
import os

PATH = r'..\..\data\tests\MLP_Architecture'
INFILE = r'F1Comparison_HiddenLayer.csv'
OUTFILE = r'MLP_Arch_Comparison_Test'
OUTFILE2 = r'MLP_Arch_Comparison_Train_Test'
HIDDEN_NEURONS = ['[2]','[3]','[4]','[5]','[8]','[10]','[20]','[50]','[100]','[256]',    # 0 - 9
                  '[5, 3]','[10, 5]','[20, 10]','[32, 16]','[64, 32]','[128, 64]',       # 10 - 15
                  '[10, 5, 3]','[20, 10, 5]','[32, 16, 8]','[64, 32, 16]','[128, 64, 32]','[256, 128, 64]',  # 16 - 21
                  '[16, 32, 64]','[32, 64, 128]','[64, 128, 256]']                    # 22 - 24

#INFILE = r'F1Comparison_HiddenLayer_HousePricePred.csv'
#OUTFILE = r'MLP_Arch_Comparison_Test_HousePricePred'
#OUTFILE2 = r'MLP_Arch_Comparison_Train_Test_HousePricePred'
#HIDDEN_NEURONS = ['[16]', '[256]', '[512]', '[1024]', '[2048]', '[4096]']    # FOR HOUSE PRICE PRED DATA

HN_IDXS = [0,9]    #Index from HIDDEN_NEURONS to apply for plot Vergleich Test Train
#HN_IDXS = [10,15]
#HN_IDXS = [0, 10]
#HN_IDXS = [19, 22]
#HN_IDXS = [0, 5]

MARKERS = ['o', 'v', '^', '<', '>', 's', 'p', 'P', '*', 'X', 'D']



infilepath = os.fsdecode(os.path.join(PATH, INFILE))
df = pd.read_csv(infilepath, sep='\t', comment='#')

#df['hidden_neurons'] = df['hidden_neurons'].str.replace('[', '')
#df['hidden_neurons'] = df['hidden_neurons'].str.replace(']', '')

print(df)
dftest = df[df['RUN']=='TEST']
dftrain = df[df['RUN']=='TRAIN']

#VERGLEICH FUER TESTDATEN UEBER ALLE ARCHITEKTUREN
for n_layers in range(3):
    df_n_layers = dftest[dftest['n_layers']==n_layers + 1]
    df_n_layers.set_index('EPOCH', inplace=True)
    df_n_layers_group = df_n_layers.set_index([df_n_layers.index, 'hidden_neurons'])['Loss'].unstack()
    #df_n_layers_group = df_n_layers.set_index([df_n_layers.index, 'hidden_neurons'])['F1Macro'].unstack()
    print(df_n_layers_group)
    
    fig, ax = plt.subplots()
    for i, col in enumerate(df_n_layers_group.columns.tolist()):
        ax.plot(df_n_layers_group[col].index, df_n_layers_group[col], label=col, marker=MARKERS[i], markersize=3, linewidth=0.5)
        ax.legend()
    ax.set_xlabel('Epoch')     
    ax.set_ylabel('Loss')
    ax.grid()
    
    outfilepath = os.fsdecode(os.path.join(PATH, OUTFILE + str(n_layers + 1) + '.png'))
    #plt.show()
    plt.savefig(outfilepath, dpi=300)
    plt.close(fig)


#VERGLEICH ZWISCHEN LOSS TEST UND TRAIN FUER BESTIMMTE ARCHITEKTUR
fig, ax = plt.subplots()
for i, hn_idx in enumerate(HN_IDXS):
    hn = HIDDEN_NEURONS[hn_idx]
    df1 = dftest[dftest['hidden_neurons']==hn]
    df2 = dftrain[dftrain['hidden_neurons']==hn]
    ax.plot(df1['EPOCH'], df1['Loss'], label=hn+' Test', marker=MARKERS[i], markersize=3, linewidth=0.5)
    ax.plot(df2['EPOCH'], df2['Loss'], label=hn+' Train', marker=MARKERS[i], markersize=3, linewidth=0.5)
ax.legend()
ax.set_xlabel('Epoch')     
ax.set_ylabel('Loss')
ax.grid()

outfilepath2 = os.fsdecode(os.path.join(PATH, OUTFILE2 + '.png'))
plt.savefig(outfilepath2, dpi=300)
plt.close(fig)
#plt.show()
