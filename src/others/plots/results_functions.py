'''
Different Functions used for data query and plotting
'''

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def filter(df: pd.DataFrame, filter_lod: list) -> pd.DataFrame:
    df_f = df
    for f in filter_lod:
        if f['crit'] is not None:
            if len(f['val']) > 1:
                raise IndexError('Only 1 value for criteria allowed')
        if f['crit'] is None:
            df_f = df_f[df_f[f['col']].isin(f['val'])]
        elif f['crit'] == 'gt':
            df_f = df_f[df_f[f['col']] > f['val'][0]]
        elif f['crit'] == 'lt':
            df_f = df_f[df_f[f['col']] < f['val'][0]]
        elif f['crit'] == 'get':
            df_f = df_f[df_f[f['col']] >= f['val'][0]]
        elif f['crit'] == 'let':
            df_f = df_f[df_f[f['col']] <= f['val'][0]]
        else:
            raise KeyError('crit not found')
    
    return df_f

def save(ax: plt.axes, outfilepath: bytes, w=10, h=5, dpi=300) -> str:
    '''
    Save Standard Chart
    '''
    plt.gcf().set_size_inches(w,h)
    plt.tight_layout()
    plt.savefig(outfilepath, dpi=dpi)
    plt.close()
    return outfilepath

def boxplot(df: pd.DataFrame, xl: str, yl: str, save: bool = True, xlim = None, ylim = None, n_at_mean = False) -> plt.axes:
    '''
    Draw Standard Boxplot
    '''
    ax, dict = df.plot(kind='box', vert=False, notch=False, showmeans=True, patch_artist=False, 
                       return_type='both', xlim=xlim, ylim=ylim) #, labels=ll) #, meanline=True)
    #print(dict)

    #Add Annotations for count of values per Series and Mean of Series
    for i, line in enumerate(dict['means']): #enumerate(dict['medians']):
        n = df.iloc[:, i].count()
        v = np.round(df.iloc[:, i].mean(), 3)
        if n_at_mean:
            ax.text(line.get_xdata()[0], sum(line.get_ydata()) / len(line.get_ydata()), str(v) + ' (n=' + str(n) + ')', fontsize='x-small', color='grey')
        else:
            ax.text(line.get_xdata()[0], sum(line.get_ydata()) / len(line.get_ydata()), v, fontsize='x-small', color='grey')
            ax.text(ax.get_xlim()[1], ax.get_ylim()[0], 'n =' + str(n), fontsize='small', color='grey', ha='right', va='bottom')
    
    ax.set_xlabel(xl)
    ax.set_ylabel(yl)

    labellist = []
    for ytl in ax.get_yticklabels():
        labellist.append(split_text(ytl.get_text(), 25, 50))
    ax.set_yticks(ax.get_yticks(), labels=labellist)

    ax.grid()
    return ax

def split_text(s, l=20, max=200):
    if len(s) > max:
        s = s[:max] + '...'

    if len(s) > l:
        slist = [s[i:i+l] + '\n' for i in range(0, len(s), l)]
        s_split = ''
        for sline in slist:
            s_split = s_split + sline
    else:
        s_split = s
    return s_split