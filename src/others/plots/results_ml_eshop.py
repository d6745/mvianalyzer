'''
Read Results File and output Pivot Chart
'''

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import results_functions

INFILE = r'D:\_TEMP\eshop\07_01-03_EShop_MLResult_overview_complete.csv' # <- CHANGE PATH
OUTFILEBASE = r'D:\TEMP\eshop\ml\eshop_' # <- CHANGE PATH

df = pd.read_csv(INFILE, sep='\t')
print(df.columns)
ATTRS = [{'name': '01_mlp_[48, 16]', 'score': 'F1 Makro', 'alias': 'MLP 48/16', 'xlim': [0,0.1]},
         {'name': '02_mlp_[128]', 'score': 'F1 Makro', 'alias': 'MLP 128', 'xlim': [0,0.1]},
         {'name': '03_som_10', 'score': 'F1 Makro', 'alias': 'SOM 10', 'xlim': [0,0.1]}]

COLS = [{'name': 'mvi', 'alias': 'MVI'},
        #{'name': 'method2', 'alias': 'ML Method'},
        {'name': 'impmethod2', 'alias': 'MVI Method'},
        {'name': 'ampmech', 'alias': 'Missing Mechanism'},
        {'name': 'ampprop', 'alias': 'Missing Rate'},
        {'name': 'amppattern', 'alias': 'Missing Pattern'}]
IDX = ['id']

#PLOT BOXPLOT F1MACRO OF ALL MLMETHODS IN ATTRS OVER COLS FOR PPD STANDARDFAELLE
for att in ATTRS:
    FILTERS = [{'col': 'method2', 'val':  [att['name']], 'crit': None}]
    VALUES = ['f1macro']
    
    df_f = results_functions.filter(df, FILTERS)
    print(df_f.shape)
    
    for col in COLS:
        outfile = OUTFILEBASE + 'bp_' + col['name'] + '_' + att['score'] + '_' + att['name'] + '.png'
        df_p_count = df_f.pivot_table(values=VALUES, index=IDX, columns=col['name'], aggfunc='count')   #Should be always 1 because grouped by IDX
        df_p_mean = df_f.pivot_table(values=VALUES, index=IDX, columns=col['name'], aggfunc=np.mean)    #Should be real value for each dataset because grouped by IDX
        df_p_mean.columns = df_p_mean.columns.droplevel(0)   #Remove Attributes Top-Level-Multiindex
        ax = results_functions.boxplot(df=df_p_mean, xl=att['alias'] + ' - ' + att['score'], yl=col['alias'], xlim=att['xlim'], n_at_mean=True)
        of = results_functions.save(ax=ax, outfilepath=outfile)
        #['line', 'bar', 'barh', 'hist', 'box', 'kde', 'density', 'area', 'pie', 'scatter', 'hexbin']
        #df_p_mean.plot(kind='line')
        #plt.show()

#PLOT LINES OF F1 MACRO MEAN FOR ALL MLMETHODS
for col in COLS:
    outfile = OUTFILEBASE + 'line_' + col['name'] + '.png'
    df_res = pd.DataFrame()
    count_list = []
    #for att in ATTRS:
    
    FILTERS = [] #[{'col': 'runid', 'val': [9], 'crit': 'let'}]
    VALUES = ['f1macro']
    df_f = results_functions.filter(df, FILTERS)
    df_p_mean = df_f.pivot_table(values=VALUES, columns=col['name'], index='method2', aggfunc=np.mean)
    df_p_count = df_f.pivot_table(values=VALUES, columns=col['name'], index='method2', aggfunc='count')
    df_p_mean.columns = df_p_mean.columns.droplevel(0)   #Remove Value Top-Level-Multiindex
    df_p_count.columns = df_p_count.columns.droplevel(0)
    #print(df_p_mean)
    #print(df_p_count)

    print(f'LINEPLOT - {col["name"]}')
    legend_list = []
    for c, v in df_p_count.items():
        n = v.mean()
        print(f'Col {c} - n = {n}')
        legend_list.append(str(c) + ' (n = ' + f'{n:.0f}' + ')')
        
        
    #n = np.mean(count_list)
    ax = df_p_mean.plot(kind='line', marker='x', fontsize='x-small', table=False)
    ax.set_ylabel(col['alias'] + ' - ' + 'F1 Macro')
    ax.set_ylim([0,0.05])
    ax.set_xlabel('ML Method')
    ax.grid()
    ax.legend(legend_list, fontsize='xx-small', loc=4)

    labellist = []
    for att in ATTRS:
        labellist.append(results_functions.split_text(att['name'], 25))
    ax.set_xticks(range(len(ATTRS)), labels=labellist)

    for label in ax.get_xticklabels():
        label.set_ha("right")
        label.set_rotation(45)
    
    df_p_mean.round(3).to_csv(outfile.replace('.png', '_mean.csv'), sep='\t')

    df_csv_out_stacked = df_p_mean.round(3).stack()
    df_csv_out_stacked.to_csv(outfile.replace('.png', '_mean_stacked.csv'), sep='\t')

    of = results_functions.save(ax=ax, outfilepath=outfile)
