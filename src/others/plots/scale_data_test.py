import pandas as pd
import matplotlib.pyplot as plt

FP = r'..\Data\_Test_ScaleData\compare.CSV' # <- CHANGE PATH
FPOUT = r'..\gfx\Scaler_Test.png' # <- CHANGE PATH

df = pd.read_csv(FP, sep='\t')

ttest = df[(df['RUN']=='TEST') & (df['SCALER']==True)]
ftest = df[(df['RUN']=='TEST') & (df['SCALER']==False)]
print(ttest)
print(ftest)

#plt.rcParams.update({'font.size': 20})

fig, ax = plt.subplots()
ax.plot(ttest['EPOCH'], ttest['Loss'], label='Scaled')
ax.plot(ftest['EPOCH'], ftest['Loss'], label='Not Scaled')
ax.set_xlabel('Epoch')
ax.set_ylabel('Loss')
ax.grid()
ax.legend()
plt.savefig(FPOUT)
plt.show()
