import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelEncoder

FN = r'D:\Data\08_HousePricePrediction\data.csv' # <- CHANGE PATH

d = pd.read_csv(FN)
pd.set_option('display.max_columns', None)
print(d.head())
print(d.describe())
print(d.info())
pd.set_option('display.max_columns', 0)

sns.set_theme()

'''
sns.jointplot(data=d, x='bedrooms', y='price', hue='waterfront')
plt.show()


#d_red = d.drop(['date', 'waterfront', 'view', 'condition', 'sqft_above', 'sqft_basement', 'yr_built', 'yr_renovated', 'street', 'statezip', 'country'], axis=1)
d_red = d.loc[:,['price', 'bedrooms', 'city', 'view', 'sqft_above', 'sqft_basement']]
le = LabelEncoder()
d_red['city_le'] = le.fit_transform(d_red['city'])

sns.pairplot(data=d_red, hue='view')
plt.show()


sns.catplot(data=d, kind='violin', x='condition', y='price', hue='waterfront', split=True)
plt.show()


sns.catplot(data=d, kind='bar', x='condition', y='price', hue='waterfront')
plt.show()
'''

sns.relplot(data=d, x='sqft_lot', y='price', col='waterfront', hue='condition', style='floors', size='view')
plt.show()
