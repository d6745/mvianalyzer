'''
Plots Data from Housepricing Dataset (Conf08)
'''

import seaborn as sns
import matplotlib.pyplot as plt
from others.getDfByConf import getDfByConf

PATH = r'D:\Downloads\temp01.png' # <- CHANGE PATH
CONF = 'conf_08.yaml'

#LOAD DATASET
df = getDfByConf(CONF)

print(df)
sns.set_theme(context='paper', style='whitegrid', palette='hsv', font_scale=1)

#df_red = df.drop(['datelong', 'order', 'sessionID'], axis=1)
df_red=df
print(df.describe())

#sns.jointplot(data=df_red, x='price', y='bedrooms', hue='condition', palette='Set1')
#sns.jointplot(data=df_red, x='page1', y='page2', palette='Set1')
sns.scatterplot(data=df_red, x='condition', y='price', hue='view', palette='Set1')

#sns.pairplot(data=df_red, hue='condition', palette='Set1')
#sns.catplot(data=df_red, kind='violin', x='age', y='weight', hue='gender', split=True, palette='Set1')
#sns.catplot(data=df_red, kind='bar', x='age', y='weight', hue='gender', palette='Set1')
#sns.relplot(data=df_red, x='age', y='weight', col='gender', hue='class', style='state', size='salary', palette='Set1')
plt.show()
#plt.savefig(PATH, dpi=300)
