import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

FILEPATH = r'D:\Data\00_PseudoData\FINALRUNS_v2\01-09_PseudoData_MVIAnalyse_complete.csv' # <- CHANGE PATH
ALIGNED = [True] #[True, False]
ONLYNANS = [True] #[True, False]

SCORES = ['NRMSE'] #['MSE', 'RMSE', 'NRMSE', 'ACC']
COLS = ['age', 'weight', 'salary'] #, 'class']

SCORES = ['ACC'] #['MSE', 'RMSE', 'NRMSE', 'ACC']
COLS = ['gender', 'state'] #, 'class']

RUNID = list(range(1,10))
print(RUNID)

df = pd.read_csv(FILEPATH, sep='\t')

'''
for s in SCORES:
    for a in ALIGNED:
        for on in ONLYNANS:
            for c in COLS:
                df1 = df.loc[(df['score'] == s) & (df['aligned'] == a) & (df['onlyNan'] == on) & (df[c] > 0)]
                df1 = df1.loc[:, ['impId', 'ampId', 'runid', c]]
                print(df1)
                df1_p = pd.pivot_table(data=df1, values=c, index=['impId'], columns=['runid'], aggfunc=[np.mean, 'count'])
                #df1_p = pd.pivot_table(df1, values=c, index=['impId', 'ampId'], columns=['runid'], aggfunc=[np.sum,'count'])
                #df2_p = df1_p.loc[(1, slice(None)) ,'sum']
                #df2_p = df1_p.loc[(slice(None), 2) ,'sum']
                print(df1_p)
                df1_p.loc[:, 'mean'].plot(title=c)
                plt.show()
                #df1_p.hist(bins=5)
                #print(df1_p)
'''

'''
for s in SCORES:
    for a in ALIGNED:
        for on in ONLYNANS:
            for c in COLS:
                #df1 = df.loc[(df['score'] == s) & (df['aligned'] == a) & (df['onlyNan'] == on) & (df[c] > 0)]
                #df1 = df1.loc[:, ['impId', 'ampId', 'runid', c]]
                #df1 = df1.loc[:, ['impId', 'runid', c]]
                for r in RUNID:
                    df1 = df.loc[(df['score'] == s) & (df['aligned'] == a) & (df['onlyNan'] == on) & (df[c] > 0) & (df['runid'] == r)]
                    df1 = df1.loc[:, ['impId', 'ampId', 'runid', c]]
                    sns.histplot(data=df1)
                    plt.show()
'''


'''
for s in SCORES:
    for a in ALIGNED:
        for on in ONLYNANS:
            for c in COLS:
                df1 = df.loc[(df['score'] == s) & (df['aligned'] == a) & (df['onlyNan'] == on) & (df[c] > 0)]
                #df1_p = pd.pivot_table(df1, values=[c], index=['ampId'], columns=['impId', 'runid'], aggfunc=[np.sum])
                #df1_p = pd.pivot_table(df1, values=[c], index=['ampId'], columns=['impId'], aggfunc=[np.mean])
                print(df1)
                #sns.histplot(df1, x=c, hue='impId', element='bars', multiple='fill', palette='hsv') #, cumulative=True) # , multiple='stack')
                #sns.histplot(df1, x=c, hue='impId', element='bars', multiple='stack', palette='hsv')
                #sns.histplot(df1, x=c, hue='impId', element='poly', multiple='stack', palette='hsv')
                #sns.histplot(df1, x=c, hue='impId', element='bars', multiple='dodge', palette='hsv')
                sns.histplot(df1, x=c, hue='runid', element='bars', multiple='stack', palette='hsv')
                #sns.histplot(data=df1_p)
                plt.show()
                                
                #df1_p.plot()


'''