import pandas as pd
import numpy as np
import matplotlib.pyplot as plt



FILEIN = r'..\..\data\tests\MLP_LearningRate\Auswertung_MLResults_Eta.csv'
FILEOUT = r'..\..\data\tests\MLP_LearningRate\Auswertung_MLResults_Eta.png'
df = pd.read_csv(FILEIN, sep='\t')

dfp = pd.pivot_table(data=df, values='F1Macro', index=['EPOCH'], columns=['Eta'], aggfunc=[np.sum])
dfpcount = pd.pivot_table(data=df, values='F1Macro', index=['EPOCH'], columns=['Eta'], aggfunc=['count'])

print(dfp)
print(dfpcount)

ax = dfp.loc[:,'sum'].plot()
ax.grid()
ax.set_xlabel('Epoch')
ax.set_ylabel('F1 Macro Score')

#plt.show()
plt.gcf().set_size_inches(10, 5)
plt.tight_layout()
plt.savefig(FILEOUT, dpi=300)
