'''
Read Results File and output Pivot Chart
'''

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import results_functions

INFILE = r'D:\DATA\00_PseudoData\FINALRUNS\01b-13_PseudoData_MVIAnalyse_complete_NRMSE2.csv' # <- CHANGE PATH
OUTFILEBASE = r'D:\_TEMP\mvi\ppd_standard_' # <- CHANGE PATH

df = pd.read_csv(INFILE, sep='\t')
print(df.columns)
ATTRS = [{'name': 'age', 'score': 'NRMSE2', 'xlim': [0,1]},
         {'name': 'weight', 'score': 'NRMSE2', 'xlim': [0,1]},
         {'name': 'gender', 'score': 'ACC', 'xlim': [0,100]},
         {'name': 'salary', 'score': 'NRMSE2', 'xlim': [0,1]},
         {'name': 'state', 'score': 'ACC', 'xlim': [0,100]}]
COLS = [{'name': 'impmethod2', 'alias': 'MVI Method'},
        {'name': 'ampmech', 'alias': 'Missing Mechniasm'},
        {'name': 'ampprop', 'alias': 'Missing Rate'},
        {'name': 'amppattern', 'alias': 'Missing Pattern'},
        {'name': 'run_rs', 'alias': 'Random Seed'},
        {'name': 'run_std', 'alias': 'Std'}]
IDX = ['id']

#PLOT BOXPLOT NRMSE/ACC OF ALL ATTRIBUTES OVER COLS FOR PPD
for att in ATTRS:
    FILTERS = [{'col': 'score', 'val': [att['score']], 'crit': None},
            {'col': 'onlyNan', 'val': [True], 'crit': None},
            {'col': 'aligned', 'val': [True], 'crit': None},
            {'col': 'runid', 'val': [9], 'crit': 'let'},
            {'col': att['name'] + '_nans', 'val': [0], 'crit': 'gt'}]
    VALUES = [att['name']]
    
    df_f = results_functions.filter(df, FILTERS)
    print(df_f.shape)
    
    for col in COLS:
        outfile = OUTFILEBASE + 'bp_' + col['name'] + '_' + att['score'] + '_' + att['name'] + '.png'
        df_p_count = df_f.pivot_table(values=VALUES, index=IDX, columns=col['name'], aggfunc='count')   #Should be always 1 because grouped by IDX
        df_p_mean = df_f.pivot_table(values=VALUES, index=IDX, columns=col['name'], aggfunc=np.mean)    #Should be real value for each dataset because grouped by IDX
        df_p_mean.columns = df_p_mean.columns.droplevel(0)   #Remove Attributes Top-Level-Multiindex
        ax = results_functions.boxplot(df=df_p_mean, xl=att['name'] + ' - ' + att['score'], yl=col['alias'], xlim=att['xlim'], n_at_mean=False)
        of = results_functions.save(ax=ax, outfilepath=outfile)
        #['line', 'bar', 'barh', 'hist', 'box', 'kde', 'density', 'area', 'pie', 'scatter', 'hexbin']
        #df_p_mean.plot(kind='line')
        #plt.show()
        
#PLOT BARS OF MEAN FOR ALL ATTRIBUTS
for col in COLS:
    outfile = OUTFILEBASE + 'bar_' + col['name'] + '.png'
    df_res_nrmse = pd.DataFrame()
    df_res_acc = pd.DataFrame()
    count_list = []
    for att in ATTRS:
        FILTERS = [{'col': 'score', 'val': [att['score']], 'crit': None},
            {'col': 'onlyNan', 'val': [True], 'crit': None},
            {'col': 'aligned', 'val': [True], 'crit': None},
            {'col': 'runid', 'val': [9], 'crit': 'let'},
            {'col': att['name'] + '_nans', 'val': [0], 'crit': 'gt'}]
        df_f = results_functions.filter(df, FILTERS)
        df_p_mean = df_f.pivot_table(values=att['name'], index=col['name'], aggfunc=np.mean)
        df_p_count = df_f.pivot_table(values=att['name'], index=col['name'], aggfunc='count')
        count_list.append(df_p_count.mean().values)
        #print(df_p_count)
        if att['score'] == 'NRMSE2':
            df_res_nrmse = pd.concat([df_res_nrmse, df_p_mean], axis=1)
        elif att['score'] == 'ACC':
            df_res_acc = pd.concat([df_res_acc, df_p_mean], axis=1)   #Use 1/Acc for plotting to compare with NRMSE2
    #print(df_res_nrmse)
    #print(df_res_acc)

    n = np.mean(count_list)

    fig, axs = plt.subplots(1,2)
    df_res_nrmse.plot(kind='barh', ax=axs[0], colormap='autumn', zorder=3, xlim=[0,0.35])
    df_res_acc.plot(kind='barh', ax=axs[1], colormap='winter', zorder=3, xlim=[0,100])

    #ax = df_res_nrmse.plot(kind='bar')
    #plt.show()
    axs[1].axes.get_yaxis().set_visible(False)
    axs[0].set_ylabel(col['alias'])
    axs[0].set_xlabel('NRMSE2')
    
    labellist = []
    for ytl in axs[0].get_yticklabels():
        labellist.append(results_functions.split_text(ytl.get_text(), 20))
    axs[0].set_yticks(axs[0].get_yticks(), labels=labellist)
    
    axs[1].set_xlabel('ACC')
    axs[0].grid(zorder=0)
    axs[1].grid(zorder=0)
    for a in axs:
        for c in a.containers:
            a.bar_label(c, label_type='center', fmt='%.3f', fontsize='xx-small', color='grey')

    print(f'BARPLOT - col = {col["name"]} - n = {n}')
    axs[1].text(axs[1].get_xlim()[1], axs[1].get_ylim()[0], 'n =' + f'{n:.0f}', fontsize='small', color='grey', ha='right', va='bottom')

    df_csv_out = pd.concat([df_res_nrmse, df_res_acc], axis=1)
    df_csv_out.round(3).to_csv(outfile.replace('.png', '_mean.csv'), sep='\t')

    df_csv_out_stacked = df_csv_out.round(3).stack()
    df_csv_out_stacked.to_csv(outfile.replace('.png', '_mean_stacked.csv'), sep='\t')

    of = results_functions.save(ax=axs, outfilepath=outfile)
    #plt.close()
