import numpy as np
import matplotlib.pyplot as plt

OUTFILE = r'gfx/ReLU_Function.png' # <- CHANGE PATH

x = np.arange(-5, 6, 1)

y = np.maximum(0, x)

print(y)

fig, ax = plt.subplots()
ax.plot(x,y)
ax.grid()
ax.set_xlabel('Input')
ax.set_ylabel('Output')
#plt.show()
plt.savefig(OUTFILE, dpi=300)


