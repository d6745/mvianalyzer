'''
Plots Histogramm of Number of Missing Values for a given Prop over multiple amputation runs
'''

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats
from tqdm import tqdm
from mvisim.pyampute import MultivariateAmputation
from mvisim.pyampute.exploration.md_patterns import mdPatterns
from mvisim.pyampute.exploration.mcar_statistical_tests import MCARTest
from datamodel.config.ConfigGlobal import ConfigGlobal
from datamodel.config.ConfigSession import ConfigSession
from dataloader.LoaderSelector import LoaderSelector

RANDOMINIT = 123
PROP = 0.5
RUNS = 100
MAXDIFFWITHTARGET = 0.001
BINS = 11
PROBFUNC = 'sigmoid-right'
#PROBFUNC = 'sigmoid-left'
#PROBFUNC = 'sigmoid-mid'
#PROBFUNC = 'sigmoid-tail'
OUTFILE = r'D:\_TEMP\pyampute_missing_value_count_hist_' + PROBFUNC + '.png' # <- CHANGE PATH
OUTFILEDF = r'D:\_TEMP\pyampute_missing_value_count_hist_' + PROBFUNC + '.csv' # <- CHANGE PATH
PAR = [{'incomplete_vars': ['age'],
      'weights': {'age': 0,'weight': 0,'gender': 0,'salary': 0,'state': 0,'class': 0},
      'mechanism': 'MCAR',
      'freq': 1.0, #0.5,
      'score_to_probability_func': PROBFUNC
    }] #,
    #{'incomplete_vars': ['weight'],
    #  'weights': {'age': 0,'weight': 0,'gender': 0,'salary': 0,'state': 0,'class': 0},
    #  'mechanism': 'MCAR',
    #  'freq': 0.5,
    #  'score_to_probability_func': PROBFUNC
    #}]


rng = np.random.default_rng(RANDOMINIT)
rsl = rng.integers(0,10000,RUNS)

cg = ConfigGlobal()
cs = ConfigSession(None)
ls = LoaderSelector(cs.getDataFilepath())
dl = ls.getLoader(cs, cg)
dl.load()
dm = dl.getDataModel()
df = dm.getDataContent().getOriginalData().getDf()
print(df)

misscounter = {'agenans': [],
               'weightnans': []}    #Counter for Number of Missing Values
for rs in tqdm(rsl):
    ma = MultivariateAmputation(prop=0.5, patterns=PAR, seed=rs, max_diff_with_target=MAXDIFFWITHTARGET)
    df_amp = ma.fit_transform(df)
    #print(df_amp)
    misscounter['agenans'].append(df_amp['age'].isna().sum())
    misscounter['weightnans'].append(df_amp['weight'].isna().sum())
    #mdp = mdPatterns()
    #patterns, fig = mdp.get_patterns(df_amp, count_or_proportion='count', show_plot=True)
    #print(patterns)
    #print(type(patterns))
    #plt.show()
    plt.close()

misscounterdf = pd.DataFrame(misscounter)
misscounterdf['nans'] = misscounterdf.sum(axis=1)
misscounterdf.to_csv(OUTFILEDF, sep='\t')
print(misscounterdf)
print(misscounterdf.describe())
mw = np.round((misscounterdf.describe().loc['mean', 'nans']), 3)
std = np.round((misscounterdf.describe().loc['std', 'nans']), 3)

#Check standard normal distribution
p_value = stats.normaltest(misscounterdf['nans'])
print(p_value)

fig, ax = plt.subplots(layout='constrained')
ax.hist(misscounterdf['nans'])
#ax.hist(misscounterdf['nans'], bins=BINS)
#ax.hist(misscounterdf['nans'], bins=BINS, cumulative=True)
#ax.hist(misscounterdf, bins=BINS)
#ax.scatter(rsl, misscounter)
ax.set_xlabel('Number of Missing Values')
ax.set_ylabel('Frequency')
ax.set_title(f'Mean = {mw}, $\sigma$ = {std}')
#ax.set_xlim(200,300)
#ax.set_ylim(0,RUNS/3.5)
#ax.legend()
ax.grid()
plt.gcf().set_size_inches(10, 5)
plt.tight_layout()
plt.savefig(OUTFILE, dpi=300)
plt.show()
plt.close()
