from datamodel.config.ConfigGlobal import ConfigGlobal
from datamodel.config.ConfigSession import ConfigSession
from dataloader.LoaderSelector import LoaderSelector
import seaborn as sns
import matplotlib.pyplot as plt

PATH = r'D:\Downloads\temp01.png' # <- CHANGE PATH

#LOAD DATASET
cg = ConfigGlobal()
cs = ConfigSession('conf_07.yaml')
ls = LoaderSelector(cs.getDataFilepath())
dl = ls.getLoader(cs, cg)
dl.load()
dm = dl.getDataModel()
df = dm.getDataContent().getOriginalData().getDf()

print(df)
sns.set_theme(context='paper', style='whitegrid', palette='hsv', font_scale=1)

df_red = df.drop(['datelong', 'order', 'sessionID'], axis=1)

#sns.jointplot(data=df, x='country', y='price', hue='page1')
#sns.jointplot(data=df, x='page1', y='page2', palette='Set1')

sns.pairplot(data=df_red, hue='price2', palette='Set1')
#sns.catplot(data=df, kind='violin', x='age', y='weight', hue='gender', split=True)
#sns.catplot(data=df, kind='bar', x='age', y='weight', hue='gender')
#sns.relplot(data=df, x='age', y='weight', col='gender', hue='class', style='state', size='salary')
#plt.show()
plt.savefig(PATH, dpi=300)
