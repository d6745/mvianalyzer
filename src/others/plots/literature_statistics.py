import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


def ieee_years_1():
    '''
    Statistics annual number of publications in IEEE Xplore with search: 
    1: "missing value imputation" in "All Metadata"
    2: "missing value imputation neural network" in "All Metadata"
    '''
    year = [2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020,2021,2022]
    count_1 = [1,0,1,3,9,7,14,9,17,14,15,15,26,16,34,29,39,38,48,78,86,103,104]
    count_2 = [0,0,1,2,3,0,4,3,7,0,2,0,3,2,3,2,7,3,5,19,26,33,26]

    print(np.array(count_2) / np.array(count_1))

    plt.rcParams.update({'font.size': 20})
    
    fig, ax = plt.subplots()
    ax.bar(year, count_1, label='Missing Value Imputation')
    ax.bar(year, count_2, label='Missing Value Imputation Neural Network')    
    ax.set_xlabel('Year')
    ax.set_ylabel('Annual number of publications in IEEE Xplore')
    ax.grid()
    ax.legend()
    plt.show()
    

ieee_years_1()