import os
import pandas as pd
import matplotlib.pyplot as plt
from helpers.filenames import filepathsuffix

FILEDIR = r'..\..\data\tests\SOM_Architecture\LR_Sigma_Comparison'
FILENAME = '00_PseudoData_mlresults.csv'
#FILENAME = '08_HousePricePrediction_mlresults.csv'
FILEPATH = os.path.join(FILEDIR, FILENAME)

df = pd.read_csv(os.fsdecode(FILEPATH), sep='\t')
print(df)
df2 = df[df['EPOCH']==9]
#df2 = df2.set_index(['LR', 'LRDEC', 'SIGMA', 'SIGMADEC'])['F1Macro'].unstack(0).unstack(1)
df2 = df2.set_index(['SIGMA', 'SIGMADEC', 'LR', 'LRDEC'])['F1Macro'].unstack(2).unstack(2)
print(df2)
df2.plot.bar(rot=45, ylim=(0.0, 1.0), grid=True)
#plt.show()
plt.savefig(filepathsuffix(os.fsencode(FILEPATH), suffix='', newExt='png'), dpi=300)
plt.close()
