import os
import pandas as pd
import matplotlib.pyplot as plt

AMPIDS = [0,4,8,12,16] #list(range(60))
IMPIDS = list(range(13))
IMPMETHODS = ['mean','median','mice','knn',
              'multiplemlp [5]','multiplemlp [3_2]','multiplemlp [2_2_1]',
              'multiplemlp [10]','multiplemlp [6_4]','multiplemlp [5_3_2]',
              'multiplemlp [20]','multiplemlp [12_8]','multiplemlp [10_6_4]']
INFOLDER = r'D:\_TEMP\out_20230326_103759' # <- CHANGE PATH
BASEFILENAME = 'PseudoData_'
SCORE = 'NRMSE'
ALIGNED = True
ONLYNANS = True
COLUMNS = ['age'] #, 'weight']

df_complete = None
for ampid in AMPIDS:
    for impid in IMPIDS:
        print(f'AMPID = {ampid} - IMPID = {impid}')
        ampidstr = "{0:0>3}".format(ampid)
        impidstr = "{0:0>3}".format(impid)
        fn = BASEFILENAME + 'amp' + ampidstr + '_imp' + impidstr + '_MVIAnalyse.csv'
        fp = os.path.join(INFOLDER, fn)
        df = pd.read_csv(os.fsdecode(fp), sep='\t')
        if df_complete is None:
            df_complete = df
        else:
            df_complete = pd.concat([df_complete, df])

print(df_complete) 

df_res = df_complete.loc[(df_complete['score'] == SCORE) & 
                         (df_complete['aligned'] == ALIGNED) &
                         (df_complete['onlyNan'] == ONLYNANS)]


x = len(IMPIDS)
width = 0.25
multiplier = 0

for i, c in enumerate(COLUMNS):
    #for aid in AMPIDS:
    #aidstr = "{0:0>3}".format(aid)
    #fn = BASEFILENAME + aidstr + '_' + c + '.png'
    fn = BASEFILENAME + c + '.png'
    fp = os.path.join(INFOLDER, fn)
    
    
    
    fig, ax = plt.subplots(layout='constrained')
    #ax.bar(df_res2['impId'], df_res2[c], label = c)
    for aid in AMPIDS:
        df_res2 = df_res.loc[(df_res['ampId'] == aid)]
        ax.plot(df_res2['impId'], df_res2[c], label = aid)
        #ax.plot(IMPMETHODS, df_res2[c], label = c)
    ax.set_xlabel('Imputation ID')
    ax.set_ylabel(SCORE)
    ax.set_ylim(0,0.4)
    ax.legend()
    ax.grid()
    #plt.show()
    plt.gcf().set_size_inches(10, 5)
    plt.tight_layout()
    plt.savefig(fp, dpi=300)

print(df_res)
        