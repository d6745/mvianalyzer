import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

OFILE = r'..\..\data\tests\MVI_Samples\PseudoData_orig.csv'
IMPFILES = [['Mean', r'..\..\data\tests\MVI_Samples\PseudoData_amp000_imp000.csv'],
           ['Median', r'..\..\data\tests\MVI_Samples\PseudoData_amp000_imp001.csv'],
           ['KNN', r'..\..\data\tests\MVI_Samples\PseudoData_amp000_imp002.csv'],
           ['MICE', r'..\..\data\tests\MVI_Samples\PseudoData_amp000_imp003.csv'],
           ['Multiple MLP', r'..\..\data\tests\MVI_Samples\PseudoData_amp000_imp004.csv']]

df_o = pd.read_csv(OFILE, sep='\t')

for i, impfile in enumerate(IMPFILES):
    df_imp = pd.read_csv(impfile[1], sep='\t')
    print(df_imp)

    fig, ax = plt.subplots()
    ax.plot(df_o.index, df_o['age'], 'o-', ms=5, label='Original')
    ax.plot(df_imp.index, df_imp['age'], 'x-', ms=5, label='Imputed')
    ax.set_xlabel('ID')
    ax.set_ylabel('age')
    ax.legend()
    ax.grid()
    plt.gcf().set_size_inches(10, 5)
    plt.tight_layout()
    plt.savefig(impfile[1].replace('.csv', '.png'), dpi=300)
    #plt.show()
    plt.close()
