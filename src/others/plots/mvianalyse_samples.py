import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

INFILE = r'..\..\data\tests\MVI_Samples\PseudoData_MVIAnalyse_Complete.csv'

df = pd.read_csv(INFILE, sep='\t')
print(df)

df0_f = df[(df['ampId']==0) & (df['score'] == 'NRMSE') & (df['aligned'] == True) & (df['onlyNan'] == True)].loc[:, ['method', 'age']]
df1_f = df[(df['ampId']==1) & (df['score'] == 'ACC') & (df['aligned'] == True) & (df['onlyNan'] == True)].loc[:, ['method', 'state']]

print(df0_f)
print(df1_f)

fig, ax = plt.subplots()
ax.bar(df0_f['method'], df0_f['age'])
ax.set_xlabel('MVI Methode')
ax.set_ylabel('NRMSE')
ax.grid()
plt.gcf().set_size_inches(10, 5)
plt.tight_layout()
plt.savefig(INFILE.replace('.csv', '_age_NRMSE.png'), dpi=300)
#plt.show()
plt.close()


fig, ax = plt.subplots()
ax.bar(df1_f['method'], df1_f['state'])
ax.set_xlabel('MVI Methode')
ax.set_ylabel('ACC')
ax.grid()
plt.gcf().set_size_inches(10, 5)
plt.tight_layout()
plt.savefig(INFILE.replace('.csv', '_state_ACC.png'), dpi=300)
#plt.show()
plt.close()
