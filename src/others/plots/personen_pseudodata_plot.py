from datamodel.config.ConfigGlobal import ConfigGlobal
from datamodel.config.ConfigSession import ConfigSession
from dataloader.LoaderSelector import LoaderSelector
import seaborn as sns
import matplotlib.pyplot as plt

PATH = r'gfx\PersonenPseudoDaten_SampleDistribution.png' # <- CHANGE PATH

#IMPORT DATASET
cg = ConfigGlobal()
cs = ConfigSession(None)
ls = LoaderSelector(cs.getDataFilepath())
dl = ls.getLoader(cs, cg)
dl.load()
dm = dl.getDataModel()
df = dm.getDataContent().getOriginalData().getDf()

print(df)
#sns.set_theme(context='paper', style='whitegrid', palette='hsv', font_scale=1)
sns.set_theme(context='paper', style='whitegrid', palette='Set1', font_scale=1)


#sns.jointplot(data=df, x='age', y='weight', hue='gender')
sns.pairplot(data=df, hue='class', palette='Set1')
#sns.catplot(data=df, kind='violin', x='age', y='weight', hue='gender', split=True)
#sns.catplot(data=df, kind='bar', x='age', y='weight', hue='gender')
#sns.relplot(data=df, x='age', y='weight', col='gender', hue='class', style='state', size='salary')
#plt.show()
plt.savefig(PATH, dpi=300)
#plt.savefig(PATH, dpi=96)




