'''
Plots Histogramm of Number of Missing Values for a given Prop over multiple amputation runs
'''

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats
from mvisim.pyampute import MultivariateAmputation
from mvisim.pyampute.exploration.md_patterns import mdPatterns
from mvisim.pyampute.exploration.mcar_statistical_tests import MCARTest
from datamodel.config.ConfigGlobal import ConfigGlobal
from datamodel.config.ConfigSession import ConfigSession
from dataloader.LoaderSelector import LoaderSelector

RANDOMINIT = 123
PROP = 0.5
RUNS = 100
MECH = ['MAR']
WEIGHTS = [[0,100,-100,50,-50,0]]
FREQ = [1.0]
RESCOLS = ['weight', 'gender', 'salary', 'state']
PROBFUNCS = ['sigmoid-right', 'sigmoid-left', 'sigmoid-mid', 'sigmoid-tail']
OUTFILE = r'D:\_TEMP\pyampute_weights_mar_' # <- CHANGE PATH

rng = np.random.default_rng(RANDOMINIT)
rsl = rng.integers(0,10000,RUNS)

cg = ConfigGlobal()
cs = ConfigSession(None)
ls = LoaderSelector(cs.getDataFilepath())
dl = ls.getLoader(cs, cg)
dl.load()
dm = dl.getDataModel()
df = dm.getDataContent().getOriginalData().getDf()
print(df)


for pf in PROBFUNCS:
    PAR = [[{'incomplete_vars': ['age'],
        'weights': {'age': WEIGHTS[0][0],'weight': WEIGHTS[0][1],'gender': WEIGHTS[0][2],'salary': WEIGHTS[0][3],'state': WEIGHTS[0][4],'class': WEIGHTS[0][5]},
        'mechanism': MECH[0],
        'freq': FREQ[0],
        'score_to_probability_func': pf
        }]]

    res_dictl = []

    for i, p in enumerate(PAR):
        for j, r in enumerate(rsl):
            ma = MultivariateAmputation(prop=PROP, patterns=p, seed=r)
            df_amp = ma.fit_transform(df)           #Complete amputed Dataframe
            df_nans = df_amp[df_amp['age'].isna()]   #Dataframe with only Nans in age
            res_dict = {'parid': i,
                        'runid': j}
            for k, rc in enumerate(RESCOLS):
                res_dict[rc+'_mean_compl'] = df_amp[rc].mean()
                res_dict[rc+'_mean_nan'] = df_nans[rc].mean()
            res_dictl.append(res_dict)

    df_res = pd.DataFrame(res_dictl)
    print(df_res)

    fig,ax = plt.subplots(2,2)
    for i, rc in enumerate(RESCOLS):
        #print(ax.flat[i])
        ax.flat[i].plot(df_res['runid'], df_res[rc+'_mean_compl'], label='Total')
        ax.flat[i].plot(df_res['runid'], df_res[rc+'_mean_nan'], label='Only age=NaN')
        ax.flat[i].set_xlabel('MVS run')
        ax.flat[i].set_ylabel('Mean of attribute values')
        ax.flat[i].set_ylim((df[rc].min(), df[rc].max()))
        ax.flat[i].set_title(rc + ' Weight = ' + str(WEIGHTS[0][i+1]))
        ax.flat[i].legend()
        ax.flat[i].grid()

    plt.gcf().set_size_inches(10, 5)
    plt.tight_layout()
    plt.savefig(OUTFILE + pf + '.png', dpi=300)
    #plt.show()
    plt.close()
