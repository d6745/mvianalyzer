from sklearn.datasets import make_blobs
import matplotlib.pyplot as plt
import numpy as np

OUTFILE = r'D:\_TEMP\Make_Blobs_Sample.png' # <- CHANGE PATH

CENTERS = 5
N = 200   #Number of samples
FEAT = 2   #Number of features (dimension of target array)
STD = [1.0, 2.0, 3.0]  #Stddev Cluster
RD = 10    #Init for Rand - int for repeat, None for random
BOX = (-10, 10)    #Range of Clusters

fig, ax = plt.subplots(1,len(STD))

for i, stddev in enumerate(STD):
    (xy, cl, centers) = make_blobs(n_samples=N, n_features=FEAT, centers=CENTERS, cluster_std=stddev, 
                                   random_state=RD, shuffle=True, return_centers=True, center_box=BOX)

    print(xy)
    print(cl)
    print(np.shape(xy))
    print(centers)
    
    '''
    for j in range(CENTERS):
        mask = cl == j
        x = xy[mask][:,0]
        y = xy[mask][:,1]
        ax[0].scatter(x,y, label='Cluster ' + str(j), marker='x')
        ax[0].scatter(centers[j,0], centers[j,1], label='Center ' + str(j) , marker='o')
    ax[0].grid()
    ax[0].legend()
    '''
    
    ax[i].scatter(xy[:,0], xy[:,1], c=cl, marker='x')
    ax[i].scatter(centers[:,0], centers[:,1], c=np.arange(CENTERS), marker='o')
    ax[i].set_title(f'STD = {STD[i]}')
    ax[i].set_xlabel('x')
    ax[i].set_ylabel('y')
    ax[i].grid()
plt.plot()
#plt.show()
plt.savefig(OUTFILE, dpi=300)
