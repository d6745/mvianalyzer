
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import results_functions

INFILE = r'D:\_TEMP\RESULTS.csv' # <- CHANGE PATH
OUTFILEBASE = r'D:\_TEMP\results_' # <- CHANGE PATH

df = pd.read_csv(INFILE, sep='\t')
print(df)

############################################################################################################

FILTERS = [{'col': 'stufe', 'val': ['mvi'], 'crit': None},
            {'col': 'critgroup', 'val':  ['ampmech', 'ampprop', 'amppattern', 'impmethod'], 'crit': None},
            {'col': 'score', 'val':  ['nrmse2'], 'crit': None},]

df_f = results_functions.filter(df, FILTERS)

df_p_mean = df_f.pivot_table(values='value', index='id', columns='critgroup', aggfunc=np.mean)
df_p_count = df_f.pivot_table(values='value', index='id', columns='critgroup', aggfunc='count')

print(df_f)

ax = results_functions.boxplot(df = df_p_mean, xl='NRMSE2', yl='Einflussgröße', n_at_mean=True)
outfile = OUTFILEBASE + 'bp_mvi_nrmse2.png'
results_functions.save(ax=ax, outfilepath=outfile)
#plt.show()

print(df_p_mean)
print(df_p_count)

############################################################################################################

FILTERS = [{'col': 'stufe', 'val': ['mvi'], 'crit': None},
            {'col': 'critgroup', 'val':  ['ampmech', 'ampprop', 'amppattern', 'impmethod'], 'crit': None},
            {'col': 'score', 'val':  ['acc'], 'crit': None},]

df_f = results_functions.filter(df, FILTERS)

df_p_mean = df_f.pivot_table(values='value', index='id', columns='critgroup', aggfunc=np.mean)
df_p_count = df_f.pivot_table(values='value', index='id', columns='critgroup', aggfunc='count')

print(df_f)

ax = results_functions.boxplot(df = df_p_mean, xl='ACC', yl='Einflussgröße', n_at_mean=True)
outfile = OUTFILEBASE + 'bp_mvi_acc.png'
results_functions.save(ax=ax, outfilepath=outfile)
#plt.show()

print(df_p_mean)
print(df_p_count)

############################################################################################################

FILTERS = [{'col': 'stufe', 'val': ['ml'], 'crit': None},
            {'col': 'critgroup', 'val':  ['ampmech', 'ampprop', 'amppattern', 'impmethod'], 'crit': None}]
            #{'col': 'score', 'val':  ['nrmse2'], 'crit': None},]

df_f = results_functions.filter(df, FILTERS)

df_p_mean = df_f.pivot_table(values='value', index='id', columns='critgroup', aggfunc=np.mean)
df_p_count = df_f.pivot_table(values='value', index='id', columns='critgroup', aggfunc='count')

print(df_f)

ax = results_functions.boxplot(df = df_p_mean, xl='F1 Makro', yl='Einflussgröße', n_at_mean=True)
outfile = OUTFILEBASE + 'bp_ml_f1.png'
results_functions.save(ax=ax, outfilepath=outfile)
#plt.show()

print(df_p_mean)
print(df_p_count)

############################################################################################################

FILTERS = [{'col': 'stufe', 'val': ['mvi'], 'crit': None},
            {'col': 'critgroup', 'val':  ['impmethod'], 'crit': None},
            {'col': 'crit', 'val':  ['04_mlp_2_mid','05_mlp_3_mid','06_mlp_4_mid','07_mlp_2_max',
                                     '08_mlp_3_max','09_mlp_4_max','knn','mean','median','mice'], 'crit': None},
            {'col': 'score', 'val':  ['nrmse2'], 'crit': None}]

df_f = results_functions.filter(df, FILTERS)

df_p_mean = df_f.pivot_table(values='value', index='id', columns='crit', aggfunc=np.mean)
df_p_count = df_f.pivot_table(values='value', index='id', columns='crit', aggfunc='count')

print(df_f)

ax = results_functions.boxplot(df = df_p_mean, xl='NRMSE2', yl='MVI Methode', n_at_mean=True)
outfile = OUTFILEBASE + 'bp_mvi_methode_nrmse2.png'
results_functions.save(ax=ax, outfilepath=outfile)
#plt.show()

print(df_p_mean)
print(df_p_count)

############################################################################################################

FILTERS = [{'col': 'stufe', 'val': ['mvi'], 'crit': None},
            {'col': 'critgroup', 'val':  ['impmethod'], 'crit': None},
            {'col': 'crit', 'val':  ['04_mlp_2_mid','05_mlp_3_mid','06_mlp_4_mid','07_mlp_2_max',
                                     '08_mlp_3_max','09_mlp_4_max','knn','mean','median','mice'], 'crit': None},
            {'col': 'score', 'val':  ['acc'], 'crit': None}]

df_f = results_functions.filter(df, FILTERS)

df_p_mean = df_f.pivot_table(values='value', index='id', columns='crit', aggfunc=np.mean)
df_p_count = df_f.pivot_table(values='value', index='id', columns='crit', aggfunc='count')

print(df_f)

ax = results_functions.boxplot(df = df_p_mean, xl='ACC', yl='MVI Methode', n_at_mean=True)
outfile = OUTFILEBASE + 'bp_mvi_methode_acc.png'
results_functions.save(ax=ax, outfilepath=outfile)
#plt.show()

print(df_p_mean)
print(df_p_count)

############################################################################################################

FILTERS = [{'col': 'stufe', 'val': ['ml'], 'crit': None},
            {'col': 'critgroup', 'val':  ['mvistrategie'], 'crit': None}]
            #{'col': 'score', 'val':  ['acc'], 'crit': None}]

df_f = results_functions.filter(df, FILTERS)

df_p_mean = df_f.pivot_table(values='value', index='id', columns='crit', aggfunc=np.mean)
df_p_count = df_f.pivot_table(values='value', index='id', columns='crit', aggfunc='count')

print(df_f)

ax = results_functions.boxplot(df = df_p_mean, xl='F1 Makro', yl='MVI Strategie', n_at_mean=True)
outfile = OUTFILEBASE + 'bp_ml_strategie.png'
results_functions.save(ax=ax, outfilepath=outfile)
#plt.show()

print(df_p_mean)
print(df_p_count)

############################################################################################################

FILTERS = [{'col': 'stufe', 'val': ['ml'], 'crit': None},
            {'col': 'critgroup', 'val':  ['mvistrategie'], 'crit': None},
            {'col': 'crit', 'val':  ['original'], 'crit': None}]
            #{'col': 'score', 'val':  ['acc'], 'crit': None}]

df_f = results_functions.filter(df, FILTERS)

df_p_mean = df_f.pivot_table(values='value', index='id', columns='group', aggfunc=np.mean)
df_p_count = df_f.pivot_table(values='value', index='id', columns='group', aggfunc='count')

print(df_f)

ax = results_functions.boxplot(df = df_p_mean, xl='F1 Makro', yl='MVI strategy', n_at_mean=True)
outfile = OUTFILEBASE + 'bp_ml_methode_original.png'
results_functions.save(ax=ax, outfilepath=outfile)
#plt.show()

print(df_p_mean)
print(df_p_count)

