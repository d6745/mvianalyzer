
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import results_functions

INFILE = r'D:\_TEMP\RESULTS_MVI_ML.csv' # <- CHANGE PATH
OUTFILEBASE = r'D:\_TEMP\results_' # <- CHANGE PATH

df = pd.read_csv(INFILE, sep='\t')
print(df)

############################################################################################################

FILTERS = [{'col': 'data', 'val': ['hpp', 'ppde', 'ppds'], 'crit': None},
            {'col': 'group', 'val':  ['04_mlp_2_mid','05_mlp_3_mid','06_mlp_4_mid','07_mlp_2_max',
                                          '08_mlp_3_max','09_mlp_4_max','10_som_min','11_som_mid',
                                          '12_som_max','13_mlp_2_max1','14_mlp_2_max2','15_mlp_2_max3','16_mlp_2_max4'], 'crit': None}]
            #{'col': 'score', 'val':  ['nrmse2'], 'crit': None},]

df_f = results_functions.filter(df, FILTERS)
print(df_f)

ax = df_f.plot.scatter('f1macro_rank', 'imp_mw_rank')
ax.set_xlabel('F1 Macro rank')
ax.set_ylabel('Average MVI method rank on NRMSE2 and ACC')
ax.grid()
#plt.show()

outfile = OUTFILEBASE + 'scatter_mvi_ml_rank.png'
results_functions.save(ax=ax, outfilepath=outfile)
