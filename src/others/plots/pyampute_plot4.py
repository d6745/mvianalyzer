'''
Plots Histogramm of Number of Missing Values for a given Prop over multiple amputation runs
'''

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats
from mvisim.pyampute import MultivariateAmputation
from mvisim.pyampute.exploration.md_patterns import mdPatterns
from mvisim.pyampute.exploration.mcar_statistical_tests import MCARTest
from datamodel.config.ConfigGlobal import ConfigGlobal
from datamodel.config.ConfigSession import ConfigSession
from dataloader.LoaderSelector import LoaderSelector

RANDOMINIT = 123
PROP = 0.5
RUNS = 100
MECH = ['MNAR']
WEIGHTS = [[100,0,0,0,0,0], [-100,0,0,0,0,0]]
FREQ = [1.0]
PROBFUNCS = ['sigmoid-right', 'sigmoid-left', 'sigmoid-mid', 'sigmoid-tail']
OUTFILE = r'D:\_TEMP\pyampute_weights_mnar_' # <- CHANGE PATH

rng = np.random.default_rng(RANDOMINIT)
rsl = rng.integers(0,10000,RUNS)

cg = ConfigGlobal()
cs = ConfigSession(None)
ls = LoaderSelector(cs.getDataFilepath())
dl = ls.getLoader(cs, cg)
dl.load()
dm = dl.getDataModel()
df = dm.getDataContent().getOriginalData().getDf()
print(df)


for pf in PROBFUNCS:
    res_dictl = []
    for i, w in enumerate(WEIGHTS):
        PAR = [[{'incomplete_vars': ['age'],
            'weights': {'age': WEIGHTS[i][0],'weight': WEIGHTS[i][1],'gender': WEIGHTS[i][2],'salary': WEIGHTS[i][3],'state': WEIGHTS[i][4],'class': WEIGHTS[i][5]},
            'mechanism': MECH[0],
            'freq': FREQ[0],
            'score_to_probability_func': pf
            }]]

        for j, p in enumerate(PAR):
            for k, r in enumerate(rsl):
                ma = MultivariateAmputation(prop=PROP, patterns=p, seed=r)
                df_amp = ma.fit_transform(df)           #Complete amputed Dataframe
                nans_idx = df_amp[df_amp['age'].isna()].index   #Indexes of Nan Values in attribute age
                res_dict = {'weightid': i,
                            'parid': j,
                            'runid': k,
                            'age_mean_compl': df['age'].mean(),
                            'age_mean_nan': df.iloc[nans_idx]['age'].mean()}
                res_dictl.append(res_dict)

        
    df_res = pd.DataFrame(res_dictl)
    print(df_res)

    fig,ax = plt.subplots(2,1)
    for i, w in enumerate(WEIGHTS):
        #print(ax.flat[i])
        df_w = df_res[df_res['weightid']==i]
        ax.flat[i].plot(df_w['runid'], df_w['age_mean_compl'], label='Total')
        ax.flat[i].plot(df_w['runid'], df_w['age_mean_nan'], label='Only age=NaN')
        ax.flat[i].set_xlabel('MVS run')
        ax.flat[i].set_ylabel('Mean of attribute values')
        ax.flat[i].set_ylim((df['age'].min(), df['age'].max()))
        ax.flat[i].set_title('Weight = ' + str(WEIGHTS[i][0]))
        ax.flat[i].legend()
        ax.flat[i].grid()

    plt.gcf().set_size_inches(10, 5)
    plt.tight_layout()
    plt.savefig(OUTFILE + pf + '.png', dpi=300)
    #plt.show()
    plt.close()
