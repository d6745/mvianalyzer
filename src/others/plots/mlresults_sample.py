import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

INFILE = r'..\..\data\tests\MLResults_Overview\PseudoDaten_01b_09b_MLResults_overview_complete.csv'

sns.set_theme(style='whitegrid', palette='hsv')

df = pd.read_csv(INFILE, sep='\t')
df.set_index('id', inplace=True)
print(df)

df_red = df[df['runid'] == 1]
df_red = df_red.loc[:, ['method2', 'mvi', 'impmethod2', 'f1macro', 'ampmech', 'ampprop']]
print(df_red)

#sns.pairplot(df_red)
sns.boxplot(data=df_red, x='f1macro', y='method2', hue='mvi', orient='h', palette='Set1', dodge=True, fliersize=1)
#plt.show()
plt.gcf().set_size_inches(10, 5)
plt.tight_layout()
plt.savefig(INFILE.replace('.csv', '.png'), dpi=300)
plt.close()

