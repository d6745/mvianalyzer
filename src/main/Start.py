#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Main Script to perform MVI Analysis
'''
import sys
import logging
from datamodel.config.ConfigGlobal import ConfigGlobal
from datamodel.config.ConfigSession import ConfigSession
from helpers.logger import initLogger
from helpers.check_rhome import check_r_home
from dataloader.LoaderSelector import LoaderSelector
import pandas as pd
from mviexecutor.MVIRunner import MVIRunner
from helpers.filenames import filepathsuffix
from mlmethods.MLRunner import MLRunner



SESSIONCONFIGFILE = 'conf_demosession.yaml'
LOGLEVEL = 'info'
LOGTARGET = 'all'   #'file'

def main(config, loglevel='info', logtarget='file'):
    #Use anything else than file for logtarget to log in file and stdout

    #Initializing
    initLogger(loglevel=loglevel, target=logtarget)
    logging.info('MVIAnalyzer started')
    pd.set_option('display.max_columns', None)
    
    #Get Configs
    cglob = ConfigGlobal()
    logging.info(cglob.getAppName() + ' ' + cglob.getAppVersion())
    #check_r_home(cglob)
    csession = ConfigSession(filename=config)

    #Load Data and prepare DataModel
    ls = LoaderSelector(csession.getDataFilepath())
    dl = ls.getLoader(csession, cglob)
    dl.load()
    dm = dl.getDataModel()
    dm.plotOriginalData()
    print(dm.getDataStructure().getReport())
    print(dm.getDataContent().getOriginalData().getEncodingClasses())
    #print(dm.getDataContent().getOriginalData().getDf().describe())

    #Run MVI Sim
    dm.runAmputation()
    
    #Run MVI
    mr = MVIRunner(dm=dm)
    mr.runMVI()
    
    #Run ML Method
    ml = MLRunner(dm=dm)
    ml.runML()

    #Output results
    cl = ml.getResList()
    mlresout = filepathsuffix(fp=dm.getConfigSession().getDataFilepath(out=True), suffix='_MLResult', newExt='csv')
    with open(mlresout, 'w') as conftxtfile:
        for x in cl:
            conftxtfile.writelines('%s\n' % line for line in x)
    dm.writeOriginalDfToFile()
    dm.writeAmputedDfToIndexedFile()
    dm.writeImputedDfToIndexedFile()
    dm.writeMVIAnalyseDfToIndexedFile()
    #dm.plotImputedData()

    #Reset and finish       
    pd.set_option('display.max_columns', 0)
    logging.info('MVIAnalyzer finished')

    return dm
    
if __name__ == '__main__':
    #main(config=SESSIONCONFIGFILE, loglevel=LOGLEVEL, logtarget=LOGTARGET)
    if len(sys.argv) == 1:
        mvidm = main(config=None)
    elif len(sys.argv) == 2:
        mvidm = main(config=sys.argv[1])
    elif len(sys.argv) == 3:
        mvidm = main(config=sys.argv[1], loglevel=sys.argv[2])
    elif len(sys.argv) == 4:
        mvidm = main(config=sys.argv[1], loglevel=sys.argv[2], logtarget=sys.argv[3])
    else:
        print('Wrong Number of Args - Use Start.py [configname] [loglevel]')
