import yaml
import logging
import os
from helpers import logger, basedir
import random

class ConfigGlobal(object):
    '''
    Global Config Settings for MVIAnalyzer
    '''

    def __init__(self):
        '''
        Init Global Config from YAML File in conf-Folder
        '''
        self.__defconf = self.__getDefaultConfig()
        self.__confdir = os.path.join(basedir.getBaseDir(), 'conf')
        self.__filepath = os.path.abspath(os.path.join(self.__confdir, 'conf_global.yaml'))
        
        if not os.path.exists(self.__filepath):
            self.createDefaultConfigFile()
            logging.warning('Global Config File not found. Create new default Config at ' + os.fsdecode(self.__filepath))
            input('Press Enter to continue...')
            
        with open(os.fsdecode(self.__filepath), 'r') as ymlin:
            self.__config = yaml.safe_load(ymlin)
        
        logging.info('Loaded Global Configuration from ' + os.fsdecode(self.__filepath))
        logging.debug('Used Global Config: ' + str(self.__config))
        
    def __getDefaultConfig(self) -> dict:
        defconf = {
                        'general': {
                                'appname': 'MVIAnalyzer',
                                'appversion': '0.1',
                                #'r_home': 'D:\Develop\R\R-4.2.2',   #Home Dir for installed R needed for rpy2 Settings
                                'outdecodedimpfile': False,      #Output a decoded or encoded imputation file from DataModel.writeImputedDfToIndexedFile()
                                'randomseed': None,              #Global Random Seed for all necessary functions. Set to None to provide a np.random.randint(0,1000)
                                'device': 'cuda'                #One of cuda or cpu - Device to use for PyTorch ANNs. If cuda not found cpu will be used.
                            },
                        'dataanalyzer': {
                                'samplefractypes': 0.05,        #Percent of samples to take from a Series to determine Column Type in DataStructure.__getColType
                                'categoricalthreshold': 0.1,     #Percent of identical values in a column to be assumed as categorical variable
                                'exportreport': True            #Export DataStructure Report to Data-Folder
                            },
                        'dataplotter': {
                                'seaborntheme': 'darkgrid',      #One of darkgrid, whitegrid, dark, white, ticks
                                'plotoriginaldata': True,        #Create statistical plots of the original dataset and save to Data Folder
                                'plotoriginaldatauseenc': True,  #Use Encoded Df for plotting or original Df
                                'plotmvisimresults': True,       #Plot Results of Missing Values after MVISim
                                'plotmvisimhoriz': True,         #Align MVI Sim PlotResult Horizontal (vertical if false)
                                'plotmvisimpatterns': True,       #Plot MVI Patterns with PyAmputer Exploration
                                'plotsommaps': True              #Plot the Maps of SOMs for each Epoch and comparison of LR and SigmaSq
                            },
                        'pseudodata': {                         #Settings for PersonenPseudo-Daten Generator
                                'n_samples': 1000,              #Number of samples to generate
                                'n_classes': 5,                 #Number of classes to generate
                                'std': 1.5,                     #Stddev to use for values
                                'box': [100,200],               #Needs Tuple. List will be returned as tuple in self.getPseudoDataBox
                                'returnstateasstr': True        #Return the state attribute as str or as int
                            }
                        }
        return defconf
        
    def createDefaultConfigFile(self) -> bool:
        with open(os.fsdecode(self.__filepath), 'w') as ymlout:
            yaml.dump(self.__defconf, ymlout)
        return True
    
    def getConfig(self) -> dict:
        return self.__config
    
    def getAppName(self) -> str:
        v = self.__config.get('general', {}).get('appname', self.__defconf['general']['appname'])
        return v
    
    def getAppVersion(self) -> str:
        v = self.__config.get('general', {}).get('appversion', self.__defconf['general']['appversion'])
        return v
    
    def getRHome(self) -> str:
        v = self.__config.get('general', {}).get('r_home', self.__defconf['general']['r_home'])
        return v
    
    def getDevice(self) -> str:
        v = self.__config.get('general', {}).get('device', self.__defconf['general']['device'])
        return v
    
    def getRandomSeed(self) -> int:
        v = self.__config.get('general', {}).get('randomseed', self.__defconf['general']['randomseed'])
        if v == None:
            v = random.randint(0,1000)
        return v
    
    def getOutDecdodedImpFile(self) -> str:
        v = self.__config.get('general', {}).get('outdecodedimpfile', self.__defconf['general']['outdecodedimpfile'])
        return v
    
    def getDataAnalyzerSampleFracTypes(self) -> float:
        v = self.__config.get('dataanalyzer', {}).get('samplefractypes', self.__defconf['dataanalyzer']['samplefractypes'])
        return v
    
    def getDataAnalyzerCategoricalThreshold(self) -> float:
        v = self.__config.get('dataanalyzer', {}).get('categoricalthreshold', self.__defconf['dataanalyzer']['categoricalthreshold'])
        return v
    
    def getDataAnalyzerExportReport(self) -> bool:
        v = self.__config.get('dataanalyzer', {}).get('exportreport', self.__defconf['dataanalyzer']['exportreport'])
        return v
    
    def getDataPlotterPlotOriginalData(self) -> bool:
        v = self.__config.get('dataplotter', {}).get('plotoriginaldata', self.__defconf['dataplotter']['plotoriginaldata'])
        return v
    
    def getDataPlotterSeabornTheme(self) -> str:
        v = self.__config.get('dataplotter', {}).get('seaborntheme', self.__defconf['dataplotter']['seaborntheme'])
        return v
    
    def getDataPlotterPlotOriginalDataUseEnc(self) -> bool:
        v = self.__config.get('dataplotter', {}).get('plotoriginaldatauseenc', self.__defconf['dataplotter']['plotoriginaldatauseenc'])
        return v
    
    def getDataPlotterPlotMVISimResults(self) -> bool:
        v = self.__config.get('dataplotter', {}).get('plotmvisimresults', self.__defconf['dataplotter']['plotmvisimresults'])
        return v
    
    def getDataPlotterPlotMVISimHoriz(self) -> bool:
        v = self.__config.get('dataplotter', {}).get('plotmvisimhoriz', self.__defconf['dataplotter']['plotmvisimhoriz'])
        return v
    
    def getDataPlotterPlotMVISimPatterns(self) -> bool:
        v = self.__config.get('dataplotter', {}).get('plotmvisimpatterns', self.__defconf['dataplotter']['plotmvisimpatterns'])
        return v
    
    def getDataPlotterPlotSOMMaps(self) -> bool:
        v = self.__config.get('dataplotter', {}).get('plotsommaps', self.__defconf['dataplotter']['plotsommaps'])
        return v   
    
    def getPseudoDataNSamples(self) -> int:
        v = self.__config.get('pseudodata', {}).get('n_samples', self.__defconf['pseudodata']['n_samples'])
        return v
    
    def getPseudoDataNClasses(self) -> int:
        v = self.__config.get('pseudodata', {}).get('n_classes', self.__defconf['pseudodata']['n_classes'])
        return v
    
    def getPseudoDataStd(self) -> float:
        v = self.__config.get('pseudodata', {}).get('std', self.__defconf['pseudodata']['std'])
        return v
    
    def getPseudoDataBox(self) -> tuple:
        v = self.__config.get('pseudodata', {}).get('box', self.__defconf['pseudodata']['box'])
        return tuple(v)
    
    def getPseudoDataReturnStateAsStr(self) -> bool:
        v = self.__config.get('pseudodata', {}).get('returnstateasstr', self.__defconf['pseudodata']['returnstateasstr'])
        return v

        
if __name__ == '__main__':
        logger.initLogger()
        c = ConfigGlobal()
        c.createDefaultConfigFile()
        print(c)
        print(c.getAppName())
        print(c.getAppVersion())