import os
import yaml
import logging
import datetime
from helpers import logger, basedir

class ConfigSession(object):
    '''
    Session Configuration for MVIAnalyzer
    '''


    def __init__(self, filename: str = None):
        '''
        Init Session Configuration from YAML File in conf Folder
        '''
        DEMOFILENAME = 'conf_session_default.yaml'
        self.__confdir = os.path.join(basedir.getBaseDir(), 'conf')
        
        if filename == None:
            self.__filepath = os.path.abspath(os.path.join(self.__confdir, DEMOFILENAME))
            self.createDefaultConfigFile()
        else:
            self.__filepath = os.path.abspath(os.path.join(self.__confdir, filename))
            
        self.__datapathout = None
        
        if not os.path.exists(self.__filepath):
            msg = f'Session Config File not found at {self.__filepath}.'
            logging.error(msg)
            raise FileNotFoundError(msg)
            
        with open(os.fsdecode(self.__filepath), 'r') as ymlin:
            self.__config = yaml.safe_load(ymlin)
        
        logging.info('Loaded Session Configuration from ' + os.fsdecode(self.__filepath))
        logging.debug('Used Session Config: ' + str(self.__config))
        
    def __getDefaultConfig(self) -> dict:
        defconf = {
                'general': {
                        #'datafilename': 'Random_01.csv',
                        'datafilename': None,    #Define Filename in Datapath - if null use PseudoDataGenerator
                        'datapath': 'data',   #Must be set for output data
                        'idcolname': None,      #Define ColName as String to use for id. If null set id automatically.
                        'dropcols': ['name'],      #Columns to drop from original Data for further Process
                        #'dropcols': ['salary'],      #Columns to drop from original Data for further Process
                        'encodecols': ['state'],   #Columns to encode data in original data, if null nothing will be encoded
                        'categoricalcols': {        #Overrides the automoatic Category Detection
                            'state': True,
                            'age': False
                            },
                        #'encodecols': ['state', 'name'],   #Columns to encode data in original data, if null nothing will be encoded
                        'encoder': 'le'         #Encoder to use: Default le = LabelEncoder oder ohe = OneHotEncoder
                    },
                'dataplotter': {                            #Plot Settings for Original Data Statistics Plot according to Seaborn Settings
                        'jointplot': [{'x': 'weight',       #Set empty list for not plotting
                                       'y': 'age',
                                       'hue': 'gender'},
                                       {'x': 'state',
                                        'y': 'salary',
                                        'hue': None}
                                ],
                        'pairplot': [{'cols': ['gender', 'weight', 'age'],  #Set empty list for not plotting
                                      'hue': 'state'},
                                      {'cols': ['gender', 'state', 'salary'],
                                       'hue': None}
                                ],
                        'catplot': [{'x': 'age',    #Set empty list for not plotting
                                     'y': 'weight',
                                     'hue': None,
                                     'kind': 'violin',
                                     'split': True},
                                    {'x': 'state',
                                     'y': 'salary',
                                     'hue': None,
                                     'kind': 'violin',
                                     'split': False}
                                ],
                        'relplot': [{'x': 'age',    #Set empty list for not plotting
                                     'y': 'weight',
                                     'col': 'gender',
                                     'hue': 'state',
                                     'style': 'gender',
                                     'size': 'age'},
                                    {'x': 'age',
                                     'y': 'weight',
                                     'col': 'state',
                                     'hue': None,
                                     'style': None,
                                     'size': None}
                                ]
                    },
                'mvisim':   {                             #Settings for MVISim with Amputer according to https://rianneschouten.github.io/mice_ampute/vignette/ampute.html
                    'amputer':  [
                                    {
                                        'prop': 0.2,
                                        'patterns': [
                                                        {
                                                            'mech': 'MCAR',
                                                            'missattr': ['age', 'weight'],
                                                            'weights': None,
                                                            'freq': 0.4,
                                                            'type': 'sigmoid-right'
                                                        },
                                                        {
                                                            'mech': 'MAR',
                                                            'missattr': ['salary'],
                                                            'weights': [1,1,1,0,1,1],
                                                            'freq': 0.6,
                                                            'type': 'sigmoid-right'
                                                        }
                                                    ]
                                    },
                                    {
                                        'prop': 0.3,
                                        'patterns': [
                                                        {
                                                            'mech': 'MCAR',
                                                            'missattr': ['gender', 'salary'],
                                                            'weights': None,
                                                            'freq': 0.4,
                                                            'type': 'sigmoid-right'
                                                        },
                                                        {
                                                            'mech': 'MCAR',
                                                            'missattr': ['age', 'weight'],
                                                            'weights': None,
                                                            'freq': 0.6,
                                                            'type': 'sigmoid-right'
                                                        }
                                                    ]
                                    },
                                ]
                            },
                'mviexecutor': [{                   #Defines one or more Pipelines of MVIMethods to apply on all Amputed Data from DataModel
                        'pipeline': [{              #Defines Methods to apply in sequential order of running
                                  #TODO: Clean Up MVIExecutor Settings - which are needed?
                                'method': 'mean',   #Name of Method - must be one from MVIRunner.MVICatalog
                                'enabled': True,    #Skip this method if False (mainly for testing purpose to quickly disable a method from running)
                                'colstoapply': ['age', 'weight'],    #Keep empty to use for all Attributes in Dataset. Name of Attributes from Amputed Data on which to apply this method, should of course contain NaNs and thus be part of Amputer
                                'colstouse': [],         #Keep empty to use for all Attributes. Name of Attributes which should be used to calculate Missing Values. Mainly for Training Purposes. Makes no sense e.g. for statistical Methods like Mean
                                'modelfile': None,  #Modelfile to use for prediction from DataPath. If None Train/Test Run will be performed to create a Modelfile. Not used for Methods without Models e.g. Mean Method
                                'usecompletefortrain': False,    #Use the original complete Data for Training or (default) the amputed data. Can be used for Model Comparison on Performance with Original and Amputed Data
                                'nanreplacer': None,    #Int Value to optionally replace NaN Values for Training. Can be used for Training with incomplete Dataset (implicit Strategy)
                                'methodsettings': {}    #Dict with individual settings for this method (e.g. Hidden Dims in MLP Method)
                            },
                                {'method': 'median',
                                'enabled': True,
                                'colstoapply': ['gender', 'salary'],
                                'colstouse': [],
                                'modelfile': None,  
                                'usecompletefortrain': False,
                                'nanreplacer': None,
                                'methodsettings': {}
                            }]},
                        {'pipeline': [{
                                'method': 'mean',
                                'enabled': True,
                                'colstoapply': [],
                                'colstouse': [],
                                'modelfile': None,  
                                'usecompletefortrain': False,
                                'nanreplacer': None,
                                'methodsettings': {}
                            }]},
                        {'pipeline': [{
                                'method': 'knn',
                                'enabled': True,
                                'colstoapply': [],
                                'colstouse': [],
                                'modelfile': None,  
                                'usecompletefortrain': False,
                                'nanreplacer': None,
                                'methodsettings': {}
                            }]},
                        {'pipeline': [{
                                'method': 'multiplemlp',
                                'enabled': True,
                                'colstoapply': [],                  #Is IGNORED for MultipleMLP MVI
                                'colstouse': [],
                                'modelfile': None,                  #Not used
                                'usecompletefortrain': False,       #Not used
                                'nanreplacer': None,                #Not used
                                'methodsettings': {
                                    'train_frac': 0.8,         #Fraction of Data from complete Data Instances to use for Training
                                    'valid_frac': 0.05,        #Fraction of Data for Validation calculated based on rest of TEST_FRAC = 1-TRAIN_FRAC - Validation currently not used in this MVI Model
                                    'scale_data': True,        #Scale the Data-Part with StandardScaler before Training with Mean and Std
                                    'batch_size': 64,          #Batch Size for Train and Test
                                    'shuffle_dl': True,        #Shuffle Data in Dataloader
                                    'epochs': 10,              #Number of Epochs for Train and Test
                                    'hidden_dims': [256,128,64],    #List of number neurons per hidden layer. One Hidden Layer will be created for each entry. Use empty list to work without hidden layers.
                                    'use_relu': True,          #Apply a ReLU Activation Layer after each Layer 
                                    'loss_function': 'mse',    #One of mse or l1 to use as loss function for Regressor Model - For Classificator Model CrossEntropyLoss is always set automatically in __prepareModel
                                    'optimizer': 'Adam',       #One of SGD, RMSProp - Optimizer to Use for Model
                                    'learning_rate': 1e-4      #Learning Rate to use for Backpropagation
                                    }
                                }]} 
                    ],
                'mlmethod': [{                                  #Defines one or more ML-Method/Classifier to apply on imputed/amputed data as implemented in the MLBaseImpl
                        'method': 'mlp',                        #Using a MLP Classifier
                        'mvi': 'imputed',                       #with all available imputed data
                        'methodsettings': {
                            'target_col': 'class',      #Name of Target Column for Classification
                            'train_frac': 0.8,         #Fraction of Data from complete Data Instances to use for Training
                            'valid_frac': 0.05,        #Fraction of Data for Validation calculated based on rest of TEST_FRAC = 1-TRAIN_FRAC - Validation currently not used in this MVI Model
                            'scale_data': True,        #Scale the Data-Part with StandardScaler before Training with Mean and Std
                            'batch_size': 64,          #Batch Size for Train and Test
                            'shuffle_dl': True,        #Shuffle Data in Dataloader
                            'epochs': 10,              #Number of Epochs for Train and Test
                            'hidden_dims': [256,128,64],    #List of number neurons per hidden layer. One Hidden Layer will be created for each entry. Use empty list to work without hidden layers.
                            'use_relu': True,          #Apply a ReLU Activation Layer after each Layer 
                            'optimizer': 'Adam',       #One of Adam, SGD, RMSProp - Optimizer to Use for Model
                            'learning_rate': 1e-4      #Learning Rate to use for Backpropagation
                            }
                        },
                        {                                  
                        'method': 'mlp',                        #Using a MLP Classifier
                        'mvi': 'original',                      #with the Original Data for comparison
                        'methodsettings': {
                            'target_col': 'class',     
                            'train_frac': 0.8,         
                            'valid_frac': 0.05,        
                            'scale_data': True,
                            'batch_size': 64,
                            'shuffle_dl': True,
                            'epochs': 10,
                            'hidden_dims': [256,128,64],
                            'use_relu': True, 
                            'optimizer': 'Adam',
                            'learning_rate': 1e-4
                            }
                        },
                        {                
                        'method': 'mlp',                        #Using a MLP Classifier
                        'mvi': 'zerofill',                      #with amputed data and zero filling all missing values
                        'methodsettings': {
                            'target_col': 'class',     
                            'train_frac': 0.8,         
                            'valid_frac': 0.05,        
                            'scale_data': True,
                            'batch_size': 64,
                            'shuffle_dl': True,
                            'epochs': 10,
                            'hidden_dims': [256,128,64],
                            'use_relu': True, 
                            'optimizer': 'Adam',
                            'learning_rate': 1e-4
                            }
                        },
                        { 
                        'method': 'mlp',                        #Using a MLP Classifier
                        'mvi': 'delete',                        #with amputed data and deleting all missing value data instances
                        'methodsettings': {
                            'target_col': 'class',     
                            'train_frac': 0.8,         
                            'valid_frac': 0.05,        
                            'scale_data': True,
                            'batch_size': 64,
                            'shuffle_dl': True,
                            'epochs': 10,
                            'hidden_dims': [256,128,64],
                            'use_relu': True, 
                            'optimizer': 'Adam',
                            'learning_rate': 1e-4
                            }
                        },
                        { 
                        'method': 'som',                        #Using a SOM Classifier
                        'mvi': 'imputed',                        #with all available imputed data
                        'methodsettings': {
                            'target_col': 'class',
                            'train_frac': 0.8,
                            'valid_frac': 0.05,
                            'shuffle_dl': True,        #Shuffle Data Training and Testdata
                            'map_rows': 10,    #Number of Rows for Feature Map
                            'map_cols': 10,    #Number of Cols for Feature Map
                            'sigma_sq': 1,     #Initial Radius of Neighbourhood function
                            'sigma_dec': 0.1,   #Decay value for radius of each epoch
                            'learning_rate': 0.5,      #Max Learning Rate (will be decayed during training)
                            'learning_rate_dec': 0.1,   #Decay value for Learning Rate over each Epoch
                            'nb_rng_max': 3,           #Maximum Neighbour Cells to use regardless of current radius
                            'epochs': 10,            #Number of epochs to perform training
                            'scaler': 'minmax'  #standard    Scaler to use on Data-Array, possible values are minmax or standard for scikit-MinMax-Scaler or StandardScaler
                            }
                        },
                        #{ 
                        #'method': 'ensemble',                   #Using an Ensemble Classifier
                        #'mvi': 'implicit',                      #in implicit method with amputed data
                        #'methodsettings': {}
                        #},
                        #{ 
                        #'method': 'dropout',                     #Using a Dropout Classifier
                        #'mvi': 'implicit',                      #in implicit method with amputed data
                        #'methodsettings': {}
                        #}
                    ]
                }
        return defconf
        
    
    def createDefaultConfigFile(self) -> bool:
        with open(os.fsdecode(self.__filepath), 'w') as ymlout:
            yaml.dump(self.__getDefaultConfig(), ymlout)
        return True
    
    def setDataFileName(self, fn: str) -> bool:
        self.__config['general']['datafilename'] = fn
    
    def getConfig(self) -> dict:
        return self.__config
    
    def getDataPath(self) -> bytes:
        v = self.__config.get('general', {}).get('datapath', self.__getDefaultConfig()['general']['datapath'])
        return os.path.abspath(os.fsencode(v))
            
    def getDataFilename(self) -> str:
        v = self.__config.get('general', {}).get('datafilename', self.__getDefaultConfig()['general']['datafilename'])
        return v 
    
    def getDataFilepath(self, out = False) -> bytes:
        if out:  #Get Out path
            p = os.fsdecode(self.getDataPathOut())
        else:   #Default Infilepath
            p = os.fsdecode(self.getDataPath())
        fn = self.getDataFilename()
        if fn == None:
            return None
        else:
            return os.fsencode((os.path.abspath(os.path.join(p, fn))))
        
    def getDataPathOut(self) -> bytes:
        if self.__datapathout is None:
            dt = datetime.datetime.now()
            p = os.path.join(os.fsdecode(self.getDataPath()), 'out_' + dt.strftime('%Y%m%d_%H%M%S'))
            if not os.path.exists(p):
                os.makedirs(p)
            self.__datapathout = p
        return self.__datapathout
        
    
    def getIdColName(self) -> str:
        v = self.__config.get('general', {}).get('idcolname', self.__getDefaultConfig()['general']['idcolname'])
        return v
    
    def getDropCols(self) -> list:
        v = self.__config.get('general', {}).get('dropcols', self.__getDefaultConfig()['general']['dropcols'])
        return v
    
    def getEncodeCols(self) -> list:
        v = self.__config.get('general', {}).get('encodecols', self.__getDefaultConfig()['general']['encodecols'])
        return v
    
    def getCategoricalCols(self) -> dict:
        v = self.__config.get('general', {}).get('categoricalcols', self.__getDefaultConfig()['general']['categoricalcols'])
        return v
    
    def getEncoder(self) -> list:
        v = self.__config.get('general', {}).get('encoder', self.__getDefaultConfig()['general']['encoder'])
        return v
    
    def getDataPlotterJointPlot(self) -> list:
        v = self.__config.get('dataplotter', {}).get('jointplot', self.__getDefaultConfig()['dataplotter']['jointplot'])
        return v
    
    def getDataPlotterPairPlot(self) -> list:
        v = self.__config.get('dataplotter', {}).get('pairplot', self.__getDefaultConfig()['dataplotter']['pairplot'])
        return v
    
    def getDataPlotterCatPlot(self) -> list:
        v = self.__config.get('dataplotter', {}).get('catplot', self.__getDefaultConfig()['dataplotter']['catplot'])
        return v
    
    def getDataPlotterRelPlot(self) -> list:
        v = self.__config.get('dataplotter', {}).get('relplot', self.__getDefaultConfig()['dataplotter']['relplot'])
        return v
    
    def getMVISimAmputer(self) -> list:
        v = self.__config.get('mvisim', {}).get('amputer', self.__getDefaultConfig()['mvisim']['amputer'])
        return v
    
    def getMVIExecutor(self) -> list:
        v = self.__config.get('mviexecutor', self.__getDefaultConfig()['mviexecutor'])
        return v
    
    def getMLMethod(self) -> list:
        v = self.__config.get('mlmethod', self.__getDefaultConfig()['mlmethod'])
        return v
    
if __name__ == '__main__':
        logger.initLogger()
        c = ConfigSession()
        c.createDefaultConfigFile()
        print(c)
        print(c.getConfig())
        print(c.getDataFilename())
        print(c.getDataFilepath())
        print(c.getDataPath())
        print(c.getIdColName())
        