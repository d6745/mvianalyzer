import pandas as pd
#from sklearn.preprocessing import OneHotEncoder
import torch
from torch.utils.data import Dataset
from sklearn.preprocessing._data import StandardScaler
#from torchvision.transforms import ToTensor, Lambda

class DfDataset(Dataset):
    '''
    Prepares PyTorch Dataset based on a DataFrame with a single target column for prediction
    '''


    def __init__(self, df: pd.DataFrame, target_cols: list, device: str='cpu', 
                 scale_data=True, scale_target=True, scaler_data=None, scaler_target=None,
                 target_dtype = torch.float):
        '''
        df: Complete DataFrame with all columns including target column
        target_cols: List of Names of Target Columns to separate for Model Training and Prediction
        device: Torch Device cuda or cpu to use for generated tensors
        scale_data: Bool, Scale the Data-Part of Df with sklearn StandardScaler with Mean and Std
        scale_target: Bool, Scale the Target-Part of Df with sklearn StandardScaler with Mean and Std
        scaler_data: sklearn-StandardScaler, Use an existing StandardScaler for Transformation, If None create a new one and fit with DataPart of DF
        scaler_target: sklearn-StandardScaler, Use an existing StandardScaler for Transformation, If None create a new one and fit with TargetPart of DF
        target_dtype: Defines the torch.dtype format for Target-Tensor, e.g. for CrossEntropy Loss FN type torch.int64 is needed.
        '''
        super().__init__()
        self.__df = df
        self.__dfdata = self.__df.drop(target_cols, axis=1)
        self.__dftargets = self.__df[target_cols]
        self.__target_cols = target_cols
        
        #SCALE DATA AND TARGETS IF TRUE USING EXISTING SCALER OR CREATE A NEW ONE
        if scale_data:
            if scaler_data == None:
                self.__scaler_data = StandardScaler(with_mean=True, with_std=True)
                self.__scaler_data.fit(self.__dfdata.values)
            else:
                self.__scaler_data = scaler_data
            data = self.__scaler_data.transform(self.__dfdata.values)    
        else:
            self.__scaler_data = None
            data = self.__dfdata.values
        
        if scale_target:
            if scaler_target == None:
                self.__scaler_target = StandardScaler(with_mean=True, with_std=True)
                self.__scaler_target.fit(self.__dftargets.values)
            else:
                self.__scaler_target = scaler_target
            target = self.__scaler_target.transform(self.__dftargets.values)
        else:
            self.__scaler_target = None
            target = self.__dftargets.values
            
        self.__data = torch.tensor(data=data, dtype=torch.float, 
                                   device=device, requires_grad=False)
        self.__targets = torch.tensor(data=target, dtype=target_dtype,
                                      device=device, requires_grad=False).squeeze()
        #print(self.__df)
        #print(self.__data)
        #print(self.__classes)
        
    def __len__(self):
        return len(self.__dftargets)
    
    def __getitem__(self, idx):
        if len(self.__target_cols) == 1:
            label = self.__targets[idx]
        else:
            label = self.__targets[idx, :]
        data = self.__data[idx,:]
        return data, label
    
    def getData(self) -> torch.Tensor:
        return self.__data
    
    def getTargets(self) -> torch.Tensor:
        return self.__targets
    
    def getDf(self) -> pd.DataFrame:
        return self.__df
    
    def getTargetCol(self) -> str:
        return self.__target_cols
    
    def getScalerData(self):
        return self.__scaler_data
    
    def getScalerTarget(self):
        return self.__scaler_target
