from pandas.core.frame import DataFrame
import logging
from dataanalyzer.DataEncoder import DataEncoder
from datamodel.data.BaseData import BaseData

class OriginalData(BaseData):
    '''
    Class containing original data from BaseLoader as DataFrame
    '''


    def __init__(self, df: DataFrame, dropCols: list = None, encodeCols: list = None, encoder: str = 'le'):
        logging.debug('Start Init')
        self.__dfcompl = df
        self.__dropcols = dropCols
        self.__encodecols = encodeCols
        self.__encoderstr = encoder
        self.__encclasses = None
        self.__df_wo_enc = self.__dropColumns()
        df = self.__encodeCols()
        super().__init__(df)    #Uses the working Df with dropped and encoded columns
        logging.debug('End Init')
        
    def getEncodingClasses(self) -> dict:
        return self.__encclasses
    
    def getDfWoEncoding(self) -> DataFrame:
        '''
        Return the Original Df with all columns including dropped and without encoding
        '''
        return self.__df_wo_enc
    
    def getDfComplete(self) -> DataFrame:
        '''
        Returns Complete Original DataFrame without dropped and encoded Columns
        '''
        msg = 'Returned complete DataFrame without dropped columns!'
        logging.info(msg)
        return self.__dfcompl
    
    def __dropColumns(self) -> DataFrame:
        if self.__dropcols is None:
            return self.__dfcompl
        else:
            if not set(self.__dropcols).issubset(self.__dfcompl.columns):
                msg = 'Drop Column from ConfigSession not found in OriginalData. Check ConfigSession'
                logging.error(msg)
                raise KeyError(msg)
            dfdropped = self.__dfcompl.drop(self.__dropcols, axis=1)
            msg = 'Dropped Columnes from Original Data ' + str(self.__dropcols)
            logging.info(msg)
            return dfdropped
        
    def __encodeCols(self) -> DataFrame:
        if self.__encodecols is None:
            return self.__df_wo_enc
        else:
            if not set(self.__encodecols).issubset(self.__dfcompl.columns):
                msg = 'Encode Column from ConfigSession not found in OriginalData. Check ConfigSession'
                logging.error(msg)
                raise KeyError(msg)
        de = DataEncoder(df=self.__df_wo_enc, encodecols=self.__encodecols, encoder=self.__encoderstr)
        self.__encclasses = de.getClasses()
        return de.getDfEncoded()

        
        
        
        