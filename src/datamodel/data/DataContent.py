import logging
from pandas.core.frame import DataFrame
from datamodel.data.OriginalData import OriginalData
from datamodel.data.MissingData import MissingData

class DataContent(object):
    '''
    Collection Class for different DataContents with OriginalData, MissingData and ImputedData
    '''


    def __init__(self, origDf: DataFrame, dropCols: list = None, encodeCols: list = None, encoder: str = 'le'):
        '''
        Initialize DataContent
        '''
        logging.debug('Start Init')
        self.__odata = OriginalData(df=origDf, dropCols=dropCols, encodeCols=encodeCols, encoder=encoder)
        self.__misdata = []
        logging.debug('End Init')
        
    def getOriginalData(self) -> OriginalData:
        return self.__odata
    
    def getMissingData(self) -> list:
        return self.__misdata
    
    def addMissingData(self, misd: MissingData) -> bool:
        self.__misdata.append(misd)
        return True

    