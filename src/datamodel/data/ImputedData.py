import logging
from pandas.core.frame import DataFrame
from datamodel.data.BaseData import BaseData

class ImputedData(BaseData):
    '''
    Class containing DataFrames with imputed Data based on MissingData
    '''


    def __init__(self, df: DataFrame, conf: list):
        '''
        Constructor
        '''
        logging.debug('Start Init')
        super().__init__(df)
        self.__conf = conf
        self.__mvianalysedf = None
        logging.debug('End Init')

    def getConfig(self) -> list:
        return self.__conf
    
    def setMVIAnalyseDf(self, madf: DataFrame):
        '''
        madf: Analyse Resulting Dataframe from MVIAnalyse.getAnalyse()
        '''
        self.__mvianalysedf = madf
        
    def getMVIAnalyseDf(self) -> DataFrame:
        return self.__mvianalysedf