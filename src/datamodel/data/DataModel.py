import os
import logging
import numpy as np
from pandas.core.frame import DataFrame
from datamodel.data.DataContent import DataContent
from dataanalyzer.DataStructure import DataStructure
from datamodel.config.ConfigSession import ConfigSession
from datamodel.config.ConfigGlobal import ConfigGlobal
from dataanalyzer.DataPlotter import DataPlotter
from datamodel.data.MissingData import MissingData
from mvisim.Amputer import Amputer
from helpers.filenames import filepathsuffix
import matplotlib.pyplot as plt

class DataModel(object):
    '''
    Common overall class to store Datamodel
    '''


    def __init__(self, filepath: bytes, origDf: DataFrame, configsession: ConfigSession, configglobal: ConfigGlobal):
        '''
        Constructor
        '''
        logging.debug('Start Init')
        self.__filepath = filepath
        self.__cs = configsession
        self.__cg = configglobal
        self.__dc = DataContent(origDf, dropCols=self.__cs.getDropCols(), encodeCols = self.__cs.getEncodeCols(), encoder = self.__cs.getEncoder())
        self.__ds = None
        logging.debug('End Init')
        
        
    def getFilepath(self) -> bytes:
        return self.__filepath
    
    def getFilepathStr(self) -> str:
        return os.fsdecode(self.__filepath)
    
    def getDataContent(self) -> DataContent:
        return self.__dc
    
    def getDataStructure(self) -> DataStructure:
        if self.__ds is None:
            self.__ds = DataStructure(self.__dc.getOriginalData().getDf(), self.__cg, self.__cs)
            self.__ds.setEncodedCols(list(self.__dc.getOriginalData().getEncodingClasses().keys()))
            if self.__cg.getDataAnalyzerExportReport():
                self.__ds.exportReport()
            return self.__ds
        else:
            return self.__ds
        
    def getConfigSession(self) -> ConfigSession:
        return self.__cs
    
    def getConfigGlobal(self) -> ConfigGlobal:
        return self.__cg
    
    def writeOriginalDfToFile(self) -> bool:
        '''
        Writes the Original Df with dropped and encoded Columns to CSV-Outputfile
        Can be used for later comparison between Original, amputed and imputed data
        '''
        sfx = '_orig'
        fp = filepathsuffix(fp=self.__cs.getDataFilepath(out=True), suffix=sfx, newExt='csv')
        self.getDataContent().getOriginalData().getDf().to_csv(os.fsdecode(fp), sep='\t')
        logging.info('Exported Amputed Dataframe to ' + os.fsdecode(fp))
        return True
    
    def writeAmputedDfToIndexedFile(self) -> bool:
        '''
        Writes all amputed data to a file with Index of MissingData Instance
        '''
        for i, md in enumerate(self.getDataContent().getMissingData()):
            sfx = '_amp{:>03}'.format(str(i))
            fp = filepathsuffix(fp=self.__cs.getDataFilepath(out=True), suffix=sfx, newExt='csv')
            md.getDf().to_csv(os.fsdecode(fp), sep='\t')
            logging.info('Exported Amputed Dataframe to ' + os.fsdecode(fp))
        return True
            
    def writeImputedDfToIndexedFile(self) -> bool:
        '''
        Wrties all imputed data for each amputed Data to a file with Index of MissingData Instance and ImputedData Instance
        '''
        for i, md in enumerate(self.getDataContent().getMissingData()):
            for j, impd in enumerate(md.getImputedData()):
                sfx = '_amp{:>03}'.format(str(i)) + '_imp{:>03}'.format(str(j))
                fp = filepathsuffix(fp=self.__cs.getDataFilepath(out=True), suffix=sfx, newExt='csv')
                if self.getConfigGlobal().getOutDecdodedImpFile():
                    self.__decodeDf(impd.getDf()).to_csv(os.fsdecode(fp), sep='\t')
                else:
                    impd.getDf().to_csv(os.fsdecode(fp), sep='\t')
                logging.info('Exported Imputed Dataframe to ' + os.fsdecode(fp))
        return True
    
    def writeMVIAnalyseDfToIndexedFile(self) -> bool:
        '''
        Writes all MVIAnalyze Dataframes for each imputed Data to a file with Index of MissingData Instance and ImputedData Instance
        '''
        for i, md in enumerate(self.getDataContent().getMissingData()):
            for j, impd in enumerate(md.getImputedData()):
                sfx = '_amp{:>03}'.format(str(i)) + '_imp{:>03}'.format(str(j)) + '_MVIAnalyse'
                fp = filepathsuffix(fp=self.__cs.getDataFilepath(out=True), suffix=sfx, newExt='csv')
                df = impd.getMVIAnalyseDf()
                df['ampId'] = i
                df['impId'] = j
                df.to_csv(os.fsdecode(fp), sep='\t')
                logging.info('Exported MVIAnalyze Dataframe to ' + os.fsdecode(fp))
        return True
    
    def __decodeDf(self, dfenc: DataFrame) -> DataFrame:
        '''
        Creates a copy of DfEnc and decodes the columns in Dataframe based on the stored encoding Classes 
        '''
        classesdict = self.getDataContent().getOriginalData().getEncodingClasses()
        dfdec = dfenc.copy(deep=True)
        for colname in classesdict:     #Iterates over each Dict Key with Colname as Key and Classnames as List
            for code, classname in enumerate(classesdict[colname]):     #Iterates through list of Classnames
                dfdec.loc[dfdec[colname] == code, colname] = classname
        return dfdec
    
    def plotOriginalData(self) -> None:
        if self.__cg.getDataPlotterPlotOriginalData():
            if self.__cg.getDataPlotterPlotOriginalDataUseEnc():
                df = self.__dc.getOriginalData().getDf()
            else:
                df = self.__dc.getOriginalData().getDfWoEncoding()
    
            dp = DataPlotter(df=df, cg=self.__cg, cs=self.__cs)
            dp.catplot()
            dp.jointplot()
            dp.pairplot()
            dp.relplot()
        return None
    
    def plotImputedData(self) -> None:
        cols = self.getDataContent().getOriginalData().getDf().shape[1]
        plt_cols = 3
        plt_rows = np.ceil(cols / plt_cols).astype(int)
        dfO = self.getDataContent().getOriginalData().getDf()
        x_values = dfO.index
        
        for ampid, amp in enumerate(self.getDataContent().getMissingData()):
            for impid, imp in enumerate(amp.getImputedData()):
                fig, ax = plt.subplots(plt_rows, plt_cols)
                for i in range(cols):
                    r = np.floor(i/plt_cols).astype(int)
                    c = np.mod(i, plt_cols).astype(int)
                    ax[r][c].plot(x_values, dfO.iloc[:,i], label='Original')
                    #ax[r][c].plot(x_values, amp.getDf().iloc[:,i], label='Amputed')
                    ax[r][c].plot(x_values, imp.getDf().iloc[:,i], label='Imputed')
                    ax[r][c].set_title(dfO.iloc[:,i].name)
                    ax[r][c].grid()
                    ax[r][c].legend()
                fig.suptitle('AmpId = ' + str(ampid) + ' - ImpId = ' + str(impid))
                sfx = '_amp{:>03}'.format(str(ampid)) + '_imp{:>03}'.format(str(impid)) 
                fp = filepathsuffix(fp=self.__cs.getDataFilepath(out=True), suffix=sfx, newExt='png')
                plt.gcf().set_size_inches(10, 5)
                plt.tight_layout()
                plt.savefig(os.fsdecode(fp), dpi=300)
                #plt.show()
                logging.info('Exported Imputed-Diagram to ' + os.fsdecode(fp))
                plt.close()
    
    def runAmputation(self) -> bool:
        for i, c in enumerate(self.getConfigSession().getMVISimAmputer()):
            a = Amputer(df=self.__dc.getOriginalData().getDf(), config=c, ampid=i, rs=self.getConfigGlobal().getRandomSeed())
            a.ampute()
            if self.getConfigGlobal().getDataPlotterPlotMVISimResults():
                a.plotMissing(outdir=self.getConfigSession().getDataFilepath(out=True), plothoriz=self.getConfigGlobal().getDataPlotterPlotMVISimHoriz())
            if self.getConfigGlobal().getDataPlotterPlotMVISimPatterns():
                a.plotMissingPatterns(outdir=self.getConfigSession().getDataFilepath(out=True))
            self.__dc.addMissingData(MissingData(a))
        return True
    
        
if __name__ == '__main__':
    FILEPATH = r'D:\Data\00_Demo01\Demo01.csv' # <- CHANGE PATH
    dm = DataModel(FILEPATH)
    print(dm.getColNames())
    