import logging
import pandas as pd
from datamodel.data.ImputedData import ImputedData
from mvisim.Amputer import Amputer
from datamodel.data.BaseData import BaseData

class MissingData(BaseData):
    '''
    Class containing DataFrames with Missing Data derived from OriginalData
    '''


    def __init__(self, amp: Amputer):
        '''
        Constructor
        '''
        logging.debug('Start Init')
        super().__init__(amp.getAmputeDf())
        self.__amp = amp
        self.__impd = []
        logging.debug('End Init')
        
    def getConfig(self) -> dict:
        return self.__amp.getAmputeConfig()
    
    def addImputedData(self, impd: ImputedData) -> bool:
        self.__impd.append(impd)
        return True
    
    def getAmputer(self) -> Amputer:
        return self.__amp
    
    def getImputedData(self) -> list:
        return self.__impd
    
    def getDfZeroFilled(self) -> pd.DataFrame:
        '''
        Returns a Df-Copy of amputed Df where all NaNs are Zero Filled
        '''
        return self.__amp.getAmputeDf().fillna(value=0, method=None, inplace=False)
    
    def getDfDeletedNans(self) -> pd.DataFrame:
        '''
        Returns the amputed Df where all NaN Rows are deleted
        '''
        return self.__amp.getAmputeDf().dropna()
    
    def getMissingMatrix(self) -> pd.DataFrame:
        '''
        Returns a DataFrame of MissingData as Missing Matrix where Missing Values are marked as 1  and complete values as 0
        '''
        mm = self.getDf().mask(self.getDf().isna()==False, other=0)   #Replace Not-Nans by 0
        mm.mask(mm.isna()==True, other=1, inplace=True)         #Replace Nans by 1
        return mm

        
        
        