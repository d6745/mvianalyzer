import logging
from pandas.core.frame import DataFrame
from torch.utils.data.dataloader import DataLoader
from torch import nn
from torch import optim
from enum import Enum
import numpy as np

class DataLoaderTypeEnum(Enum):
    TRAIN = 1
    TEST = 2

class BaseData():
    '''
    Base Class for Data Contents in DataModel
    '''


    def __init__(self, df: DataFrame):
        '''
        df: DataFrame to set for all extended classes
        '''
        logging.debug('Start Init')
        self._df = df
        self._dls = []
        self._das = []
        self._model = None
        self._optimizer = None
        self._configId = None
        logging.debug('End Init')
        
    def getDf(self) -> DataFrame:
        return self._df
    
    def setConfigId(self, ampId: int, impId: int):
        '''
        Allows to set the Id of Amputation and Imputation according to order in ConfigSession for this specific BaseData Instance
        For OriginalData set both to None
        '''
        self._configId = {
            'ampId': ampId,
            'impId': impId
            }
    
    def getConfigId(self) -> dict:
        '''
        Returns Dictionary with ConfigIds according to order in ConfigSession
        '''
        return self._configId
    
    def getConfig(self):
        '''
        Returns Config from Data - To override in implementation
        '''
        return 'No Config Available'
    
    def addDataArray(self, da_data: np.ndarray, da_labels: np.ndarray, dltype: DataLoaderTypeEnum):
        '''
        Adds a Numpy Array to this BaseData Instance to perform MLMethods --> Used for SOM
        da_data: n*m Numpy Array with Data
        da_labels: n*1 Numpy Array with Labels
        '''
        d = {'da_data': da_data,
             'da_labels': da_labels,
             'dltype': dltype
            }
        self._das.append(d)
        
    def getDataArrays(self) -> list:
        return self._das
    
    def getDataArray(self, dltype: DataLoaderTypeEnum) -> np.ndarray:
        for d in self._das:
            if d['dltype'] == dltype:
                return d['da_data'], d['da_labels']
        return None
    
    def addDataLoader(self, dl: DataLoader, dltype: DataLoaderTypeEnum):
        '''
        Adds a torch DataLoader to this BaseData Instance to perform MLMethods --> Used for MLP
        '''
        d = {'dl': dl,
             'dltype': dltype
            }
        self._dls.append(d)
    
    def getDataLoaders(self) -> list:
        return self._dls
    
    def getDataLoader(self, dltype: DataLoaderTypeEnum) -> DataLoader:
        for d in self._dls:
            if d['dltype'] == dltype:
                
                #for (X,y) in d['dl']:
                #    print(X)
                #print(d['dltype'])
                #input('PRESS')
                return d['dl']
        return None
    
    def setModel(self, m: nn.Module):
        '''
        Adds a Torch Classificator Model for the specific BaseData Instance 
        '''
        self._model = m
    
    def getModel(self) -> nn.Module:
        return self._model
    
    def setOptimizer(self, optimizer: optim.Optimizer):
        self._optimizer = optimizer
        
    def getOptimizer(self):
        return self._optimizer
        
    