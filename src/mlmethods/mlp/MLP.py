from mlmethods.MLBase import MLBase
from datamodel.data.DataModel import DataModel
import logging
import torch
from datamodel.data.DfDataset import DfDataset
from helpers.torchdevice import torchdevice
from torch.utils.data.dataloader import DataLoader
from datamodel.data.BaseData import DataLoaderTypeEnum
from mviexecutor.multiplemlp.MLPModel import MLPModel
from sklearn.metrics import precision_score, recall_score, f1_score

#TARGET_COL = 'class'
#TRAIN_FRAC = 0.8
#VALID_FRAC = 0.05
#SCALE_DATA = True
#BATCH_SIZE = 64
#SHUFFLE_DL = True
#HIDDEN_DIMS = [256,128,64]
#USE_RELU = True
#OPTIMIZER = 'Adam'  #'SGD' 'RMSProp'
#LEARNING_RATE = 1e-4
#EPOCHS = 5

class MLP(MLBase):
    '''
    Simple MLP Model for Classification
    This Method uses the MLP-Model from mviexecutor.multiplemlp.MLPModel as Classificator
    '''

    def __init__(self, dm:DataModel, confId: int):
        '''
        see MLBase
        '''
        logging.info('Start Init')
        super().__init__(dm, confId)
        self.__ms = self._conf['methodsettings']
        
        self.__device = torchdevice(dev=self._dm.getConfigGlobal().getDevice())
        self.__l_basedata = self.__prepareDataLoaders()
        self.__usesoftmax = True                            #Softmax to reproduce finally Class Labels from Logits
        self.__lossfunction = torch.nn.CrossEntropyLoss()   #Because we use only Classificator Cross Entropy Loss is used
        self.__comparelist = []
        self.__prepareModel()
    
        logging.info('End Init')
    
    def train(self):
        basedata_counter = 0
        basedata_total = len(self.__l_basedata)
        for bd in self.__l_basedata:
            basedata_counter += 1
            print(f'Running Train/Test {basedata_counter}/{basedata_total} for {self._conf}:')
            logging.info(f'Running Train/Test {basedata_counter}/{basedata_total} for {self._conf}:')
            logging.info(f'Using Data Config {bd.getConfig()}')
            
            self.__comparelist.append('-'*150)
            self.__comparelist.append(str(self._conf))
            self.__comparelist.append(str(bd.getConfig()))
            train_dl = bd.getDataLoader(dltype=DataLoaderTypeEnum.TRAIN)
            test_dl = bd.getDataLoader(dltype=DataLoaderTypeEnum.TEST)
            
            for epoch in range(self.__ms['epochs']):
                #RUN THE TRAINING
                bd.getModel().train()
                running_loss = 0
                total = 0
                correct = 0
                for batch, (X, y) in enumerate(train_dl):
                    bd.getOptimizer().zero_grad()   # 3. Optimizer zero grad
                    y_logits = bd.getModel()(X)     # model outputs raw logits
                    loss = self.__lossfunction(y_logits, y.squeeze()) 
                    loss.backward()
                    bd.getOptimizer().step()
                    if self.__usesoftmax:
                        y_pred = torch.softmax(y_logits, dim=1).argmax(dim=1) # go from logits -> prediction probabilities -> prediction labels
                    else:
                        y_pred = y_logits
                    
                    running_loss += loss.item()
                    
                    if self.__usesoftmax:      #For Softmax Calculate Accuracy
                        total += y.size(0)
                        correct_batch = y.squeeze().eq(y_pred).sum().item()
                        correct += correct_batch
                    msg = f'TRAIN - Epoche {epoch} - Batch {batch} - Loss Batch: {loss:.5f}' #, Acc Batch: {acc_batch:.2f}%'
                    #self.__comparelist.append(msg)
                    #logging.info(msg)
                    
                train_loss=running_loss/len(train_dl)
                if self.__usesoftmax:
                    acc=100.*correct/total
                else:
                    acc = -1
                msg = f'TRAIN\tEPOCH\t{epoch}\tLoss\t{train_loss:.5f}\tAcc\t{acc:.2f}%'
                self.__comparelist.append(msg)
                logging.info(msg)
                
                #RUN THE TEST
                bd.getModel().eval()
                running_loss = 0
                total = 0
                correct = 0
                y_true_list = []
                y_pred_list = []
                for batch, (X,y) in enumerate(test_dl):
                    with torch.inference_mode():
                        y_logits = bd.getModel()(X)
                        if self.__usesoftmax:
                            y_pred = torch.softmax(y_logits, dim=1).argmax(dim=1)
                        else:
                            y_pred = y_logits
                        loss = self.__lossfunction(y_logits, y.squeeze())
                        running_loss += loss.item()
                        
                        if self.__usesoftmax:      #For Softmax Calculate Accuracy
                            total += y.size(0)
                            correct_batch = y.squeeze().eq(y_pred).sum().item()
                            correct += correct_batch
                        
                        #Extend the list with true and predicted classes for later calculation of Precision, Recall and F1-Score
                        y_true_list.extend(y.tolist())
                        y_pred_list.extend(y_pred.tolist())
                
                #print(f'y_true = {y_true_list}')
                #print(f'y_pred = {y_pred_list}')
                precision = precision_score(y_true_list, y_pred_list, average=None, zero_division=0)
                recall = recall_score(y_true_list, y_pred_list, average=None, zero_division=0)
                f1 = f1_score(y_true_list, y_pred_list, average=None, zero_division=0)
                pmicro = precision_score(y_true_list, y_pred_list, average='micro', zero_division=0) 
                pmacro = precision_score(y_true_list, y_pred_list, average='macro', zero_division=0)
                pweight = precision_score(y_true_list, y_pred_list, average='weighted', zero_division=0)
                rmicro = recall_score(y_true_list, y_pred_list, average='micro', zero_division=0) 
                rmacro = recall_score(y_true_list, y_pred_list, average='macro', zero_division=0)
                rweight = recall_score(y_true_list, y_pred_list, average='weighted', zero_division=0)
                f1micro = f1_score(y_true_list, y_pred_list, average='micro', zero_division=0) 
                f1macro = f1_score(y_true_list, y_pred_list, average='macro', zero_division=0)
                f1weight = f1_score(y_true_list, y_pred_list, average='weighted', zero_division=0)
                
                test_loss=running_loss/len(test_dl)
                if self.__usesoftmax:
                    acc=100.*correct/total
                else:
                    acc = -1
                
                msg = f'TEST\tEPOCH\t{epoch}\tLoss\t{test_loss:.5f}\tAcc\t{acc:.2f}%\tF1Macro\t{f1macro:.5f}\tF1Micro\t{f1micro:.5f}\tF1Weighted\t{f1weight:.5f}'

                msg2 = f'\t{self._conf["method"]}\t{self._conf["methodsettings"]["hidden_dims"]}'
                msg = msg + msg2
                
                d = {
                    'epoch': epoch,
                    'loss': test_loss,
                    'accuracy': acc,
                    'precision': precision,
                    'recall': recall,
                    'f1': f1,
                    'precisionmicro': pmicro,
                    'recallmicro': rmicro,
                    'f1micro': f1micro,
                    'precisionmacro': pmacro,
                    'recallmacro': rmacro,
                    'f1macro': f1macro,
                    'precisionweighted': pweight,
                    'recallweighted': rweight,
                    'f1weighted': f1weight
                    }
                self.__comparelist.append(msg)
                self.__comparelist.append(d)
                logging.info(msg)
            
    
    def test(self):
        #Test is included in train function
        pass
    
    def predict(self):
        pass
    
    def compare(self):
        return self.__comparelist
    
    def __prepareDataLoaders(self) -> list:
        l_basedata = []
        #Add BaseData instances based on current MVI Method for this ML Method
        if self._conf['mvi'] == 'original':
            bd = self._dm.getDataContent().getOriginalData()
            l_basedata.append(bd)
        elif self._conf['mvi'] == 'imputed':
            for i, amp in enumerate(self._dm.getDataContent().getMissingData()):
                for j, imp in enumerate(amp.getImputedData()):
                    l_basedata.append(imp)
        elif self._conf['mvi'] == 'zerofill':
            for i, amp in enumerate(self._dm.getDataContent().getMissingData()):
                l_basedata.append(amp)
        elif self._conf['mvi'] == 'delete':
            for i, amp in enumerate(self._dm.getDataContent().getMissingData()):
                l_basedata.append(amp)
        else:
            msg = 'MVI for this method in ConfigSession not available. Check ConfigSession.'
            logging.error(msg)
            raise NameError(msg)
        
        #Create Datasets for each BaseData Instance
        for i, bd in enumerate(l_basedata):
            if self._conf['mvi'] == 'zerofill':
                df = bd.getDfZeroFilled()
            elif self._conf['mvi'] == 'delete':
                df = bd.getDfDeletedNans()
            else:
                df = bd.getDf()
            
            #Splitting to Train, Test and Valid
            rs = self._dm.getConfigGlobal().getRandomSeed()
            train_df = df.sample(frac=self.__ms['train_frac'], axis=0, random_state=rs)
            test_df = df.drop(index=train_df.index)
            valid_df = test_df.sample(frac=self.__ms['valid_frac'], random_state=rs)
            test_df = test_df.drop(valid_df.index)
            
            #Creating Torch Dataset and DataLoader for this BaseData Instance
            #Because we use only Classificator as MLP Model set target Dtype to int64
            scale_target = False
            target_dtype = torch.int64
            train_ds = DfDataset(df=train_df, target_cols=[self.__ms['target_col']], device=self.__device, 
                                 scale_data=self.__ms['scale_data'], scale_target=scale_target,
                                 scaler_data=None, scaler_target=None,
                                 target_dtype=target_dtype)
            test_ds = DfDataset(df=test_df, target_cols=[self.__ms['target_col']], device=self.__device,
                                scale_data=self.__ms['scale_data'], scale_target=scale_target,
                                scaler_data=train_ds.getScalerData(), scaler_target=train_ds.getScalerTarget(),
                                target_dtype=target_dtype)
            train_dl = DataLoader(dataset=train_ds, batch_size=self.__ms['batch_size'], shuffle=self.__ms['shuffle_dl'])
            test_dl = DataLoader(dataset=test_ds, batch_size=self.__ms['batch_size'], shuffle=self.__ms['shuffle_dl'])
            
            bd.addDataLoader(train_dl, DataLoaderTypeEnum.TRAIN)
            bd.addDataLoader(test_dl, DataLoaderTypeEnum.TEST)

        return l_basedata
        
    
    def __prepareModel(self) -> bool:
        '''
        Prepares the MLP Model for this MLMethod Run based on methodsettings from ConfigSession
        Uses the MLPModel from mviexecutor.multiplemlp.MLPModel as Classificator        
        '''
        for bd in self.__l_basedata:
            num_classes = bd.getDf()[self.__ms['target_col']].drop_duplicates().shape[0]  #Number of Class Labels in Target Column
            num_features = bd.getDf().shape[1] - 1                        #Number of Df Columns minus the target column
            model = MLPModel(in_dim=num_features, hidden_dims=self.__ms['hidden_dims'], 
                             out_dim=num_classes, use_relu=self.__ms['use_relu']).to(self.__device)
            bd.setModel(model)
            
            #Get Optimizer from Settings
            if self.__ms['optimizer'] == 'Adam':
                bd.setOptimizer(torch.optim.Adam(params=bd.getModel().parameters(), lr=self.__ms['learning_rate']))
            elif self.__ms['optimizer'] == 'SGD':
                bd.setOptimizer(torch.optim.SGD(params=bd.getModel().parameters(), lr=self.__ms['learning_rate']))
            elif self.__ms['optimizer'] == 'RMSProp':
                bd.setOptimizer(torch.optim.RMSprop(params=bd.getModel().parameters(), lr=self.__ms['learning_rate']))
            else:
                bd.setOptimizer(torch.optim.Adam(params=bd.getModel().parameters(), lr=self.__ms['learning_rate']))
            
        return True