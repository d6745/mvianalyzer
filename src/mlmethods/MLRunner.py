import logging
from datamodel.data.DataModel import DataModel
from mlmethods.mlp.MLP import MLP
from mlmethods.ensemble.Ensemble import Ensemble
from mlmethods.dropout.Dropout import Dropout
from mlmethods.MLBase import MLBase
from mlmethods.som.SOM import SOM

class MLRunner(object):
    '''
    Execution Class to run MLMethods on DataModel with imputed or amputed data depending on implicit or explicit strategy
    with one or multiple of MLBase Method(s) according to ConfigSession settings.
    '''


    def __init__(self, dm: DataModel):
        '''
        Args:
        dm: data.DataModel Object with amputed and imputed Data for explicit strategy
        '''
        logging.info('Start Init')
        self.__dm = dm
        self.__mlconf = self.__dm.getConfigSession().getMLMethod()
        self.__reslist = []
        logging.info('End Init')
        
    def __getMLBaseImpl(self, method: str, confId: int):
        '''
        Returns an instantiaded implementation of MLBase based on the method-Entry from Config
        '''
        if method == 'mlp':
            return MLP(self.__dm, confId)
        elif method == 'ensemble':
            return Ensemble(self.__dm, confId)
        elif method == 'dropout':
            return Dropout(self.__dm, confId)
        elif method == 'som':
            return SOM(self.__dm, confId)
        elif method == None:        #Alternative to pass MLMethod without usage sr
            return None 
        else:
            msg = 'ML Method ' + method + ' not found. Check ConfigSession.'
            logging.error(msg)
            raise NameError(msg)
            return None
    
    def runML(self) -> DataModel:
        '''
        For each defined Method in in mlmethod Settings from ConfigSession execute the ML Method
        to predict classes
        '''
        mlconf_total = len(self.__mlconf)        
        for i, method in enumerate(self.__mlconf):
            m1 = method['method']
            m2 = method['mvi']
            print(f'Running MLMethod {i}/{mlconf_total}: {m1} with {m2}')
            logging.info(f'Running MLMethod {i}/{self.__mlconf}: {m1} with {m2}')
            mlbase = self.__getMLBaseImpl(m1, i)
            if not mlbase == None:
                mlbase.train()
                mlbase.test()
                mlbase.predict()
                self.__reslist.append(mlbase.compare())
            else:
                res = [150*'-', 'NO ML METHOD USER', 150*'-']
                self.__reslist.append(res)
        
        return self.__dm
        
    def getResList(self) -> list:
        return self.__reslist