from abc import ABC, abstractmethod
import logging
from datamodel.data.DataModel import DataModel

class MLBase(ABC):
    '''
    Abstract Base Class for all ML Methods 
    '''


    def __init__(self, dm:DataModel, confId: int):
        '''
        Args:
        dm: DataModel Model
        confId: 0-based Index of mlmethod-List Entry from ConfigSession (will be automatically set by MLRunner.runMVI-Loop)  
        '''
        logging.info('Start Init')
        self._dm = dm
        self._conf = self._dm.getConfigSession().getMLMethod()[confId]
        self._confId = confId
        logging.info('End Init')
    
    @abstractmethod
    def train(self):
        pass
    
    @abstractmethod
    def test(self):
        pass
    
    @abstractmethod
    def predict(self):
        pass
    
    @abstractmethod
    def compare(self):
        pass
        