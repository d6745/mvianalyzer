from mlmethods.MLBase import MLBase
from datamodel.data.DataModel import DataModel
import logging

class Dropout(MLBase):
    '''
    Performs implicit MVI with Dropout
    '''

    def __init__(self, dm:DataModel, confId: int):
        '''
        Constructor
        '''
        logging.info('Start Init')
        msg = 'METHOD NOT YET IMPLEMENTED'
        logging.error(msg)
        raise NotImplementedError(msg)
        logging.info('End Init')
    
    def train(self):
        pass
    
    def test(self):
        pass
    
    def predict(self):
        pass
    
    def compare(self):
        pass
        