'''
Parts of SOM-Class based on 
https://towardsdatascience.com/understanding-self-organising-map-neural-network-with-python-code-7a77f501e985
and
https://stackabuse.com/self-organizing-maps-theory-and-implementation-in-python-with-numpy/
'''

import logging
import os

from datamodel.config.ConfigGlobal import ConfigGlobal
from datamodel.config.ConfigSession import ConfigSession
from dataloader.LoaderSelector import LoaderSelector


import numpy as np
from sklearn.preprocessing import MinMaxScaler #normalisation
from sklearn.metrics import accuracy_score #scoring
import matplotlib.pyplot as plt
from mlmethods.MLBase import MLBase
from datamodel.data.DataModel import DataModel
from helpers.logger import initLogger
from datamodel.data.BaseData import DataLoaderTypeEnum
from sklearn.preprocessing._data import StandardScaler
from helpers.filenames import filepathsuffix
from sklearn.utils import shuffle
from sklearn.metrics._classification import precision_score, recall_score,\
    f1_score

class SOM(MLBase):
    '''
    Simple SOM Model for Classification/Clustering
    '''

    def __init__(self, dm:DataModel, confId: int):
        '''
        see MLBase
        '''
        logging.info('Start Init')
        super().__init__(dm, confId)
        self.__ms = self._conf['methodsettings']
        self.__maprows = self.__ms['map_rows']
        self.__mapcols = self.__ms['map_cols']
        self.__epochs = self.__ms['epochs']
        self.__lr = self.__ms['learning_rate']
        self.__lrdecay = self.__ms['learning_rate_dec']
        self.__shuffledl = self.__ms['shuffle_dl']
        self.__sigmasq = self.__ms['sigma_sq']
        self.__sigmadec = self.__ms['sigma_dec']
        self.__nbrngmax = self.__ms['nb_rng_max']
        self.__plotres = self._dm.getConfigGlobal().getDataPlotterPlotSOMMaps()
        
        self.__l_basedata = self.__prepareDataLoaders()
        self.__comparelist = []
        self.__scaler = None
        self.__som = None
        
        logging.info('End Init')

    def train(self):
        basedata_counter = 0
        basedata_total = len(self.__l_basedata)
        for bd in self.__l_basedata:
            basedata_counter += 1
            print(f'Running Train/Test {basedata_counter}/{basedata_total} for {self._conf}:')
            logging.info(f'Running Train/Test {basedata_counter}/{basedata_total} for {self._conf}:')
            logging.info(f'Using Data Config {bd.getConfig()}')
            
            self.__comparelist.append('-'*150)
            self.__comparelist.append(str(self._conf))
            self.__comparelist.append(str(bd.getConfig()))
            
            # Init SOM
            train_x, train_y = bd.getDataArray(DataLoaderTypeEnum.TRAIN)
            train_x_norm = self.__scaleData(train_x)
            dims = train_x_norm.shape[1]    #Dimensions of input data
            rng = np.random.default_rng(seed=self._dm.getConfigGlobal().getRandomSeed())
            self.__som = rng.random(size=(self.__maprows, self.__mapcols, dims), dtype=np.float64)
            
            #RUN THE TRAINING
            epoch_l = []
            sigma_l = []
            lr_l = []
            
            lr_cur = self.__lr
            sigma_cur = self.__sigmasq
            for epoch in range(self.__epochs):
                if self.__shuffledl:
                    train_x_norm, train_y = shuffle(train_x_norm, train_y, random_state=None)
                    #Dont use random_state here as every epoch will then produce the same results.
                for t_x in train_x_norm:
                    r,c  = self.__getBMU(t_x)
                    self.__updateWeights(t_x, lr_cur, sigma_cur, r, c)
                # Update learning rate and radius
                lr_cur = self.__lr * np.exp(-epoch * self.__lrdecay)
                sigma_cur = self.__sigmasq * np.exp(-epoch * self.__sigmadec)
                epoch_l.append(epoch)
                sigma_l.append(sigma_cur)
                lr_l.append(lr_cur)
                logging.info(f'Finished Epoch {epoch} of {self.__epochs}')
                    
                #Plot Label Map for this Epoch 
                ampId = bd.getConfigId()['ampId']
                impId = bd.getConfigId()['impId']
                sfx = '_som{:>03}'.format(str(self._confId)) + '_amp{:>03}'.format(str(ampId)) + '_imp{:>03}'.format(str(impId)) + '_' + str(epoch)
                fp = filepathsuffix(fp=self._dm.getConfigSession().getDataFilepath(out=True), suffix=sfx, newExt='png')
                lm = self.__createLabelMap(epoch, train_x_norm, train_y, filepath=os.fsdecode(fp))
                logging.info(f'Exported SOM Feature Map to {os.fsdecode(fp)}')
                
                #STARTING TEST FOR CURRENT BASEDATA INSTANCE
                #Get the final Label Map for Testing
                logging.info(f'Running Test for {self._conf}:')
                logging.info(f'Using Data Config {bd.getConfig()}')
                test_x, test_y = bd.getDataArray(DataLoaderTypeEnum.TEST)
                test_x_norm = self.__scaleData(test_x)

                if self.__shuffledl:
                    test_x_norm, test_y = shuffle(test_x_norm, test_y, random_state=None)
                    #Dont use random_state here as every epoch will then produce the same results.
                
                winner_labels = []
                
                for t_x in test_x_norm:
                    #winner = self.__getWinningNeuron(test_x_norm, t, self.__ms['map_rows'], self.__ms['map_cols'])
                    r_win, c_win = self.__getBMU(t_x)
                    predicted = lm[r_win][c_win]
                    winner_labels.append(predicted)

                y_true_list = test_y
                y_pred_list = winner_labels
                
                accscore = accuracy_score(y_true_list, np.array(y_pred_list))
                precision = precision_score(y_true_list, y_pred_list, average=None, zero_division=0)
                recall = recall_score(y_true_list, y_pred_list, average=None, zero_division=0)
                f1 = f1_score(y_true_list, y_pred_list, average=None, zero_division=0)
                pmicro = precision_score(y_true_list, y_pred_list, average='micro', zero_division=0) 
                pmacro = precision_score(y_true_list, y_pred_list, average='macro', zero_division=0)
                pweight = precision_score(y_true_list, y_pred_list, average='weighted', zero_division=0)
                rmicro = recall_score(y_true_list, y_pred_list, average='micro', zero_division=0) 
                rmacro = recall_score(y_true_list, y_pred_list, average='macro', zero_division=0)
                rweight = recall_score(y_true_list, y_pred_list, average='weighted', zero_division=0)
                f1micro = f1_score(y_true_list, y_pred_list, average='micro', zero_division=0) 
                f1macro = f1_score(y_true_list, y_pred_list, average='macro', zero_division=0)
                f1weight = f1_score(y_true_list, y_pred_list, average='weighted', zero_division=0)
                
                msg = f'TEST\tEPOCH\t{epoch}\t\tAcc\t{accscore:.5f}\tF1Macro\t{f1macro:.5f}\tF1Micro\t{f1micro:.5f}\tF1Weighted\t{f1weight:.5f}'
                msg2 = f'\t{self._conf["method"]}\t{self.__ms["learning_rate"]}\t{self.__ms["learning_rate_dec"]}\t{self.__ms["sigma_sq"]}\t{self.__ms["sigma_dec"]}\t{self.__ms["map_rows"]}\t{self.__ms["map_cols"]}'
                msg = msg + msg2
                
                d = {
                    'epoch': epoch,
                    'accuracy': accscore,
                    'precision': precision,
                    'recall': recall,
                    'f1': f1,
                    'precisionmicro': pmicro,
                    'recallmicro': rmicro,
                    'f1micro': f1micro,
                    'precisionmacro': pmacro,
                    'recallmacro': rmacro,
                    'f1macro': f1macro,
                    'precisionweighted': pweight,
                    'recallweighted': rweight,
                    'f1weighted': f1weight
                    }
                self.__comparelist.append(msg)
                self.__comparelist.append(d)
            
                logging.info(msg)
            
            logging.info('SOM training/Test completed')
            
            if self.__plotres:
                #PLOTTING LEARNING RATE AND NEIGHBOURHOOD-RANGE
                fig, ax = plt.subplots(1,2)
                ax[0].plot(epoch_l, lr_l)
                ax[0].grid()
                ax[0].set_xlabel('Epoch')
                ax[0].set_ylabel('Learning Rate')
                ax[1].plot(epoch_l, sigma_l)
                ax[1].grid()
                ax[1].set_xlabel('Epoch')
                ax[1].set_ylabel('Sigma Squared')
                sfx = '_som{:>03}'.format(str(self._confId)) + '_amp{:>03}'.format(str(ampId)) + '_imp{:>03}'.format(str(impId)) + '_LR_Sigma'
                fp = filepathsuffix(fp=self._dm.getConfigSession().getDataFilepath(out=True), suffix=sfx, newExt='png')
                #plt.show()
                plt.savefig(os.fsdecode(fp), dpi=300)
                plt.close()
            
    def test(self):
        #Included in train
        pass
    
    def predict(self):
        pass
    
    def compare(self):
        return self.__comparelist
    
    def __createLabelMap(self, epoch, train_x_norm, train_y, filepath=None) -> np.ndarray:
        #ASSIGN THE LABELS TO MAP (Training is unsupervised therefore we assign additionally the labels to map neurons)
        #Each cell of the SOM contains now a list of Labels if this cell was the winning neuron 
        labels_map = np.empty(shape=(self.__maprows, self.__mapcols), dtype=object)
        
        for row in range(self.__maprows):
            for col in range(self.__mapcols):
                labels_map[row][col] = [] # empty list to store the label
        
        for i, t_x in enumerate(train_x_norm):
            #if (t+1) % 1000 == 0:
            #    logging.info(f'Sample data: \tStep = \t{step}\tDataIdx = \t{t}')
            r_win, c_win = self.__getBMU(t_x)
            labels_map[r_win][c_win].append(train_y[i])        # label of winning neuron
        #print(labels_map)
        
        #CREATE LABEL_MAP
        #Based on the labels-List for each winning neuron we search for most often label in this cell and assign this label to the map
        #If the labels-List for a cell is empty we assign the value of -1 (this means that this cell was never winning during training)
        label_map = np.zeros(shape=(self.__maprows, self.__mapcols),dtype=np.int64)
        for row in range(self.__maprows):
            for col in range(self.__mapcols):
                label_list = labels_map[row][col]
                if len(label_list)==0:
                    label = -1
                else:
                    label = max(label_list, key=label_list.count)
                label_map[row][col] = label
        
        if self.__plotres:
            #cmap = colors.ListedColormap(['tab:green', 'tab:red', 'tab:orange'])
            fig, ax = plt.subplots()
            plt.imshow(label_map) #, cmap=cmap)
            plt.colorbar()
            plt.title('Epoch ' + str(epoch))     #Use last Step from Training above
            if filepath is not None:
                plt.savefig(filepath, dpi=300)
                
            plt.close(fig)
        
        return label_map
    
    def __scaleData(self, data: np.ndarray) -> np.ndarray:
        '''
        Scales the Data in Numpy Array based on Scaler from Methodsettings and returns scaled Array
        '''
        if self.__ms['scaler'] == 'minmax':
            self.__scaler = MinMaxScaler(feature_range=(0,1), copy=True, clip=False)
        elif self.__ms['scaler'] == 'standard':
            self.__scaler = StandardScaler()
        else:
            msg = 'Scaler for this method in ConfigSession not available. Check ConfigSession.'
            logging.error(msg)
            raise NameError(msg)
        
        return self.__scaler.fit_transform(data)
    
    def __getBMU(self, x):
        '''
        Row and Col of Winning Neuron
        '''
        distSq = (np.square(self.__som - x)).sum(axis=2)
        r, c = np.unravel_index(np.argmin(distSq, axis=None), distSq.shape)
        return r, c

    def __updateWeights(self, x, lr_cur, sigma_sq_cur, row, col):
        if sigma_sq_cur < 1e-3:    #if radius is close to zero then only BMU is changed
            self.__som[row][col] += lr_cur * (x - self.__som[row][col])
        else:
            # Change all cells in a small neighborhood of BMU
            for i in range(max(0, row-self.__nbrngmax), min(self.__som.shape[0], row+self.__nbrngmax)):
                for j in range(max(0, col-self.__nbrngmax), min(self.__som.shape[1], col+self.__nbrngmax)):
                    dist_sq = np.square(i - row) + np.square(j - col)
                    dist_func = np.exp(-dist_sq / 2 / sigma_sq_cur)
                    self.__som[i][j] += lr_cur * dist_func * (x - self.__som[i][j])
    
    def __prepareDataLoaders(self) -> list:
        l_basedata = []
        #Add BaseData instances based on current MVI Method for this ML Method
        if self._conf['mvi'] == 'original':
            bd = self._dm.getDataContent().getOriginalData()
            bd.setConfigId(ampId=None, impId=None)
            l_basedata.append(bd)
        elif self._conf['mvi'] == 'imputed':
            for i, amp in enumerate(self._dm.getDataContent().getMissingData()):
                for j, imp in enumerate(amp.getImputedData()):
                    imp.setConfigId(ampId=i, impId=j)
                    l_basedata.append(imp)
        elif self._conf['mvi'] == 'zerofill':
            for i, amp in enumerate(self._dm.getDataContent().getMissingData()):
                amp.setConfigId(ampId=i, impId=None)
                l_basedata.append(amp)
        elif self._conf['mvi'] == 'delete':
            for i, amp in enumerate(self._dm.getDataContent().getMissingData()):
                amp.setConfigId(ampId=i, impId=None)
                l_basedata.append(amp)
        else:
            msg = 'MVI for this method in ConfigSession not available. Check ConfigSession.'
            logging.error(msg)
            raise NameError(msg)
        
        #Create Numpy-Array for each BaseData Instance
        for i, bd in enumerate(l_basedata):
            if self._conf['mvi'] == 'zerofill':
                df = bd.getDfZeroFilled()
            elif self._conf['mvi'] == 'delete':
                df = bd.getDfDeletedNans()
            else:
                df = bd.getDf()
            
            #Splitting to Train, Test and Valid
            rs = self._dm.getConfigGlobal().getRandomSeed()
            train_df = df.sample(frac=self.__ms['train_frac'], axis=0, random_state=rs)
            test_df = df.drop(index=train_df.index)
            valid_df = test_df.sample(frac=self.__ms['valid_frac'], random_state=rs)
            test_df = test_df.drop(valid_df.index)
            
            #Numpy Arrays and adding them to current Basedata Instance
            #Umwandlung der Dataframe Spalten in Numpy Arrays fuer Eingangsdaten und Labels
            tc = self.__ms['target_col']
            da_data_train = train_df.drop(tc, axis=1).to_numpy()  #Use all columns for Data from Train_Df except Target Col
            da_labels_train = train_df[tc].to_numpy()              #Use target Col for Labels from Train_Df
            da_data_test = test_df.drop(tc, axis=1).to_numpy()
            da_labels_test = test_df[tc].to_numpy()
            bd.addDataArray(da_data=da_data_train, da_labels=da_labels_train, dltype=DataLoaderTypeEnum.TRAIN)
            bd.addDataArray(da_data=da_data_test, da_labels=da_labels_test, dltype=DataLoaderTypeEnum.TEST)
        
        return l_basedata

if __name__ == '__main__':
    #IMPORT DATASET
    initLogger()
    cg = ConfigGlobal()
    cs = ConfigSession('conf_00_somtest.yaml')
    ls = LoaderSelector(cs.getDataFilepath())
    dl = ls.getLoader(cs, cg)
    dl.load()
    dm = dl.getDataModel()
    som = SOM(dm=dm, confId=0)
    som.train()
    #df = dm.getDataContent().getOriginalData().getDf()

