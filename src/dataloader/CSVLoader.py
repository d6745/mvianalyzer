import pandas as pd
import os
import logging
from dataloader.Baseloader import Baseloader
from datamodel.config.ConfigSession import ConfigSession
from datamodel.config.ConfigGlobal import ConfigGlobal

class CSVLoader(Baseloader):
    '''
    Loads Standard CSV Files
    '''


    def __init__(self, filepath: str, sessionconfig: ConfigSession, sessionglobal: ConfigGlobal):
        '''
        Constructor
        '''
        logging.debug('Start Init')
        super().__init__(filepath, sessionconfig, sessionglobal)
        self.__delimiter = ','
        logging.debug('End Init')
    
    def _setDelimiter(self, sep=',') -> None:
        self.__delimiter = sep

    def load(self) -> bool:
        fp = os.fsdecode(self._filepath)
        df = pd.read_csv(fp, sep=self.__delimiter)
        self._addOriginalData(df)
        logging.info('Loadad data from ' + self.getFilepathStr())
        return True
    
if __name__ == '__main__':
    FILEPATH = r'D:\Data\00_Demo01\Demo02.csv' # <- CHANGE PATH
    cl = CSVLoader(FILEPATH)
    print(cl.load())
    dm = cl.getDataModel()
    print(dm.getDataContent(0).getDataFrame())