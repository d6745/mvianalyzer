from dataloader.CSVLoader import CSVLoader
import logging
from datamodel.config.ConfigSession import ConfigSession
from datamodel.config.ConfigGlobal import ConfigGlobal

class CSVLoaderTab(CSVLoader):
    '''
    Extends CSVLoader Class for usage of Tab as CSV Delimiter
    '''


    def __init__(self, filepath: str, sessionconfig: ConfigSession, sessionglobal: ConfigGlobal):
        '''
        Constructor
        '''
        logging.debug('Start Init')
        super().__init__(filepath, sessionconfig, sessionglobal)
        self._setDelimiter('\t')
        logging.debug('End Init')
    
if __name__ == '__main__':
    FILEPATH = r'D:\Data\00_Demo01\Demo01.csv' # <- CHANGE PATH
    cl = CSVLoaderTab(FILEPATH)
    print(cl.load())
    dm = cl.getDataModel()
    print(dm.getDataContent(0).getDataFrame())
    print(type(cl._filepath))
    
        