import os
from datamodel.data.DataModel import DataModel
from abc import ABC, abstractmethod
import logging
import pandas as pd
from datamodel.config.ConfigSession import ConfigSession
from pandas.core.frame import DataFrame
import numpy as np
from datamodel.config.ConfigGlobal import ConfigGlobal

class Baseloader(ABC):
    '''
    Base class to load data
    '''


    def __init__(self, filepath: str, sessionconfig: ConfigSession, sessionglobal: ConfigGlobal):
        '''
        Constructor
        '''
        logging.debug('Start Init')
        self._filepath = None if filepath == None else os.fsencode(filepath)
        self._datamodel = None
        self._csession = sessionconfig
        self._cglobal = sessionglobal
        logging.debug('End Init')
        
    @abstractmethod
    def load(self):
        pass 
        
    def getFilepathOS(self) -> bytes:
        return self._filepath
    
    def getFilepathStr(self) -> str:
        return os.fsdecode(self._filepath)
    
    def getDataModel(self) -> DataModel:
        if type(self._datamodel) == None:
            msg = 'DataModel not existing. Call load on dataloader first.'
            logging.error(msg)
            raise ValueError(msg)
        return self._datamodel
    
    def _addOriginalData(self, df: DataFrame) -> None:
        if not isinstance(df, DataFrame):
            msg = 'Invalid Type: DataFrame needed'
            logging.error(msg)
            raise TypeError(msg)
        
        #Sets the index to DataFrame according to Session Config
        if self._csession.getIdColName() is None:
            if 'id' in df.columns:
                df['id_orig'] = df['id']
                print(df.head())
                df.drop(['id'], axis=1)
                msg = 'id Column in data found. Renamed to id_orig.'
                logging.warning(msg)
                input(msg + '...')
            df['id'] = pd.Series(np.arange(0, df.shape[0]))
            df.set_index('id', inplace= True) 
        else:
            df.set_index(self._csession.getIdColName(), inplace=True)
        
        self._datamodel = DataModel(self._filepath, df, self._csession, self._cglobal)
    