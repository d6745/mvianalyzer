import os
import logging
import pandas as pd
import numpy as np
from sklearn import datasets
import matplotlib.pyplot as plt
import seaborn as sns
from faker import Faker
from dataloader.Baseloader import Baseloader
from datamodel.config.ConfigSession import ConfigSession
from datamodel.config.ConfigGlobal import ConfigGlobal

class PseudoDataGenerator(Baseloader):
    '''
    Creating a Pseudo Data set with different categorized and float values optional with Name column as Textfield
    Based on the sklearn makeBlobs Function to create also classes
    '''


    def __init__(self, sessionconfig: ConfigSession, sessionglobal: ConfigGlobal, createNameStr: bool = False):
        '''
        Loads and prepares the data
        createNameStr: Creates an additional column with fake First Names
        returnStateAsStr: Replaces the default Int-Numbers for a State by Real Names
        '''
        logging.debug('Start Init')
        super().__init__(None, sessionconfig, sessionglobal)
        self.__createNameStr = createNameStr
        if self._cglobal == None:
            self.__returnStateAsStr = False
        else:
            self.__returnStateAsStr = self._cglobal.getPseudoDataReturnStateAsStr()
        logging.debug('End Init')
        
    def load(self) -> bool:
        df = self.getBlobs(n=self._cglobal.getPseudoDataNSamples(), 
                           centers=self._cglobal.getPseudoDataNClasses(), 
                           rd=self._cglobal.getRandomSeed(), 
                           std=self._cglobal.getPseudoDataStd(), 
                           box=self._cglobal.getPseudoDataBox())
        self._csession.setDataFileName('PseudoData.csv')
        df.to_csv(os.fsdecode(self._csession.getDataFilepath()), sep='\t')
        self._addOriginalData(df)
        logging.info('Loaded data from PseudoDataGenerator')
        logging.info('Exported PseudoData to ' + os.fsdecode(self._csession.getDataFilepath()))
        return True
        
        
    def getBlobs(self, n=10000, centers=10, rd=10, std=1.0, box=(100,200)) -> pd.DataFrame:
        '''
        n: Number of samples
        features: Number of features (dimension of target array)
        std: Stddev des Cluster
        rd: Init for Rand - int for repeat, None for random
        box: Range of clusters
        '''
        
        #Sample Blobs for age, weight, gender, salary, state with 10 Classes
        FEATURES = 5
       
        (d1, cl, centercoords) = datasets.make_blobs(n_samples=n, n_features=FEATURES, centers=centers, cluster_std=std, 
                    random_state=rd, shuffle=True, return_centers=True, center_box=box)
        
        columns = ['age', 'weight', 'gender', 'salary', 'state']
        df = pd.DataFrame(data=d1, columns=columns)
        
        
        #Anpassen/Skalieren der Daten
        df['age'] = np.abs(df['age'] / 4)
        df['age'] = df['age'].astype(np.int8)
        df['weight'] = np.round(np.abs(df['weight'] / 2), 1)
        
        
        x1 = df[(df['gender']<150)]
        df.loc[x1.index, 'gender'] = 0
        x2 = df[(df['gender']>=150)]
        df.loc[x2.index, 'gender'] = 1
        df['gender'] = df['gender'].astype(np.int8)
        
        df['salary'] = np.round(np.abs(df['salary'] * 20), 2)
        
        df['state'] = np.abs(df['state'] / 10)
        df['state'] = df['state'].astype(np.int8)
        if self.__returnStateAsStr:
            df['state'] = self.__replaceStatesByStr(df['state'], rd)
        
        if self.__createNameStr:
            df['name'] = self.__getNames(n, rd)
        
        df['class'] = cl
        df['class'] = df['class'].astype(np.int64)
        
        return df
    
    def __getNames(self, n: int, rd: int) -> list:
        names = []
        f = Faker(locale='de_DE')
        Faker.seed(rd)
        for _ in range(n):
            names.append(f.first_name())
        return names
    
    def __replaceStatesByStr(self, stateDs: pd.Series, rd: int) -> list:
        sl = np.array(stateDs.tolist())
        states = ['Baden-Württemberg','Bayern','Berlin','Brandenburg','Bremen','Hamburg','Hessen','Mecklenburg-Vorpommern','Niedersachsen',
          'Nordrhein-Westfalen','Rheinland-Pfalz','Saarland','Sachsen','Sachsen-Anhalt','Schleswig-Holstein','Thüringen']
        
        #Normalize stateNumbers to 0 to 15
        a = (-1) * (15 / (np.min(sl) - np.max(sl)))
        b = 15 - a * np.max(sl)
        slnorm = np.round((a * sl + b),0).astype(int)
        
        slstr = [states[i] for i in slnorm]
        return slstr
        
         

if __name__ == '__main__':
    cs = ConfigSession(None)
    cg = ConfigGlobal()
    d = PseudoDataGenerator(sessionconfig=cs, sessionglobal=cg, createNameStr=True)
    df = d.getBlobs(n=20, centers=4, rd=42, std=1.5, box=(100,200))
    #df = d.getBlobs(n=20, centers=10, rd=None, std=1.5, box=(100,200))
    #sns.set_theme()
    #sns.pairplot(data=df)
    #plt.show()
    print(df)
    #print(df['state'].unique())
