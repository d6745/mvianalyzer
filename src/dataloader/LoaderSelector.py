import os
import logging
import csv
from helpers.logger import initLogger
from dataloader.Baseloader import Baseloader
from dataloader.CSVLoader import CSVLoader
from dataloader.CSVLoaderTab import CSVLoaderTab
from datamodel.config.ConfigSession import ConfigSession
from datamodel.config.ConfigGlobal import ConfigGlobal
from dataloader.PseudoDataGenerator import PseudoDataGenerator

class LoaderSelector(object):
    '''
    Helper Class to select correct Loader according to Filetype
    '''


    def __init__(self, filepath: bytes):
        '''
        Constructor
        '''
        logging.debug('Start Init')
        self.__fp = filepath
        self.__ext = os.fsdecode(os.path.splitext(self.__fp)[1]) if self.__fp != None else None
        logging.debug('End Init')
        
    def getLoader(self, sessionconfig: ConfigSession, sessionglobal: ConfigGlobal) -> Baseloader:
        if self.__ext == '.csv':
            sep = self.__getCSVSeparator()
            if sep == ',':
                return CSVLoader(os.fsdecode(self.__fp), sessionconfig, sessionglobal)
            elif sep == '\t':
                return CSVLoaderTab(os.fsdecode(self.__fp), sessionconfig, sessionglobal)
            else:
                msg = 'CSV Delimiter not Support.'
                logging.error(msg)
                raise ValueError(msg)
        elif self.__ext == None:
            return PseudoDataGenerator(sessionconfig, sessionglobal, createNameStr=True)
        else:
            msg = 'File Type not Supported.'
            logging.error(msg)
            raise ValueError(msg)
            
        

    def __getCSVSeparator(self) -> str:
        sniffer = csv.Sniffer()
        with open(os.fsdecode(self.__fp)) as fp:
            try:
                dialect = sniffer.sniff(fp.read(1000))
                sep = dialect.delimiter
            except:
                sep = '\t'   #In case of error a Tab-Delimiter is assumed. Sniffer doesnt recognize Tab-Delimiters
        return sep
        
        

if __name__ == '__main__':
    initLogger()
    FN = r'D:\Data\00_Demo01\Demo01.csv' # <- CHANGE PATH
    ls = LoaderSelector(os.fsencode(FN))
    print(ls.getLoader())
    