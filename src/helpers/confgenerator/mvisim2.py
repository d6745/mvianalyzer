'''
Automates creation of mvisim-Part for a config
'''

import yaml
from copy import deepcopy
YAMLOUT = r'D:\Downloads\x_mvisim.yaml' # <- CHANGE PATH

dmvisim = {'mvisim': {
            'amputer': []
        }
    }

l_prop = [0.1, 0.5, 0.9]

ld_patternbase = [{
    'pattern': [[0],[1],[3],[5],[7],[9],[11]],
    'freq': [0.15,0.14,0.14,0.14,0.14,0.14,0.15],
    'weights': {
        'MCAR': None,
        'MAR': [[0,100,80,60,40,20,1,-20,-40,-60,-80,-100],[100,0,80,60,40,20,1,-20,-40,-60,-80,-100],
                [100,80,60,0,40,20,1,-20,-40,-60,-80,-100],[100,80,60,40,20,0,1,-20,-40,-60,-80,-100],
                [100,80,60,40,20,1,-20,0,-40,-60,-80,-100],[100,80,60,40,20,1,-20,-40,-60,0,-80,-100],
                [100,80,60,40,20,1,-20,-40,-60,-80,-100,0]],
        'MNAR': [[100,0,0,0,0,0,0,0,0,0,0,0],[0,100,0,0,0,0,0,0,0,0,0,0],
                 [0,0,0,100,0,0,0,0,0,0,0,0],[0,0,0,0,0,100,0,0,0,0,0,0],
                 [0,0,0,0,0,0,0,100,0,0,0,0],[0,0,0,0,0,0,0,0,0,100,0,0],
                 [0,0,0,0,0,0,0,0,0,0,0,100]
],
        }
    },
    {
    'pattern': [[1,5,9],[3,7,11]],
    'freq': [0.5,0.5],
    'weights': {
        'MCAR': None,
        'MAR': [[0,100,75,50,25,0,1,-25,-50,0,-75,-100],[100,75,50,0,25,1,-25,0,-50,-75,-100,0]],
        'MNAR': [[0,100,0,0,0,1,0,0,0,-100,0,0],[0,0,0,100,0,0,0,1,0,0,0,-100]],
    }
    }
    ]

for mech in ['MCAR', 'MAR', 'MNAR']:
    for prop in l_prop:
        for pattern in ld_patternbase:
            d = {'patterns': [],
                 'prop': deepcopy(prop)}
            for i, p in enumerate(pattern['pattern']):
                d1 = {'missattr': deepcopy(p),
                      'freq': deepcopy(pattern['freq'][i]),
                      'mech': deepcopy(mech),
                      'type': 'sigmoid-right'
                     }
                if pattern['weights'][mech] == None:
                    d1['weights'] = None
                else:
                    d1['weights'] = deepcopy(pattern['weights'][mech][i])
                
                d['patterns'].append(d1)
            dmvisim['mvisim']['amputer'].append(d)


with open(YAMLOUT, 'w') as ymlout:
    yaml.dump(dmvisim, ymlout)