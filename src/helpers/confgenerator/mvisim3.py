'''
Automates creation of mvisim-Part for a config
'''

import yaml
from copy import deepcopy
YAMLOUT = r'D:\Downloads\x_mvisim.yaml' # <- CHANGE PATH

dmvisim = {'mvisim': {
            'amputer': []
        }
    }

l_prop = [0.1, 0.3, 0.5, 0.7, 0.9]
mechs = ['MCAR', 'MAR', 'MNAR']

#l_prop = [0.5]
mechs = ['MNAR']

ld_patternbase = [
    {
    'pattern': [[1]],
    'freq': [1],
    'weights': {
        'MCAR': None,
        'MAR': [[100,0,84,67,50,32,14,-3,-20,-36,-52,-68,-84,-100,0]],
        'MNAR': [[0,100,0,0,0,0,0,0,0,0,0,0,0,0,0]],
        }
    },
    {
    'pattern': [[0,2,3,4,5,6,7,8,9,10,11,12,13]],
    'freq': [1],
    'weights': {
        'MCAR': None,
        'MAR': [[0,100,0,0,0,0,0,0,0,0,0,0,0,0,-100]],
        'MNAR': [[100,0,84,67,50,32,14,-3,-20,-36,-52,-68,-84,-100,0]],
        }
    },
    {
    'pattern': [[0],[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13]],
    'freq': [0.08,0.07,0.07,0.07,0.07,0.07,0.07,0.07,0.07,0.07,0.07,0.07,0.07,0.08],
    'weights': {
        'MCAR': None,
        'MAR': [[0,100,84,67,50,32,14,-3,-20,-36,-52,-68,-84,-100,0],[100,0,84,67,50,32,14,-3,-20,-36,-52,-68,-84,-100,0],
                [100,84,0,67,50,32,14,-3,-20,-36,-52,-68,-84,-100,0],[100,84,67,0,50,32,14,-3,-20,-36,-52,-68,-84,-100,0],
                [100,84,67,50,0,32,14,-3,-20,-36,-52,-68,-84,-100,0],[100,84,67,50,32,0,14,-3,-20,-36,-52,-68,-84,-100,0],
                [100,84,67,50,32,14,0,-3,-20,-36,-52,-68,-84,-100,0],[100,84,67,50,32,14,-3,0,-20,-36,-52,-68,-84,-100,0],
                [100,84,67,50,32,14,-3,-20,0,-36,-52,-68,-84,-100,0],[100,84,67,50,32,14,-3,-20,-36,0,-52,-68,-84,-100,0],
                [100,84,67,50,32,14,-3,-20,-36,-52,0,-68,-84,-100,0],[100,84,67,50,32,14,-3,-20,-36,-52,-68,0,-84,-100,0],
                [100,84,67,50,32,14,-3,-20,-36,-52,-68,-84,0,-100,0],[100,84,67,50,32,14,-3,-20,-36,-52,-68,-84,-100,0,0]],
        'MNAR': [[100,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[0,100,0,0,0,0,0,0,0,0,0,0,0,0,0],
                 [0,0,100,0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,100,0,0,0,0,0,0,0,0,0,0,0],
                 [0,0,0,0,100,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,100,0,0,0,0,0,0,0,0,0],
                 [0,0,0,0,0,0,100,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,100,0,0,0,0,0,0,0],
                 [0,0,0,0,0,0,0,0,100,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,100,0,0,0,0,0],
                 [0,0,0,0,0,0,0,0,0,0,100,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,100,0,0,0],
                 [0,0,0,0,0,0,0,0,0,0,0,0,100,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,100,0]],
        }
    },
    {
    'pattern': [[0],[0,1],[0,1,2],[0,1,2,3],[0,1,2,3,4],[0,1,2,3,4,5],[0,1,2,3,4,5,6],
                [0,1,2,3,4,5,6,7],[0,1,2,3,4,5,6,7,8],[0,1,2,3,4,5,6,7,8,9],[0,1,2,3,4,5,6,7,8,9,10],
                [0,1,2,3,4,5,6,7,8,9,10,11],[0,1,2,3,4,5,6,7,8,9,10,11,12],[0,1,2,3,4,5,6,7,8,9,10,11,12,13]],
    'freq': [0.08,0.07,0.07,0.07,0.07,0.07,0.07,0.07,0.07,0.07,0.07,0.07,0.07,0.08],
    'weights': {
        'MCAR': None,
        'MAR': [[0,100,84,67,50,32,14,-3,-20,-36,-52,-68,-84,-100,0],[0,0,100,82,64,46,28,10,-8,-26,-44,-62,-81,-100,0],
                [0,0,0,100,80,60,40,20,1,-20,-40,-60,-80,-100,0],[0,0,0,0,100,78,56,34,12,-10,-32,-54,-77,-100,0],
                [0,0,0,0,0,100,75,50,25,1,-25,-50,-75,-100,0],[0,0,0,0,0,0,100,71,42,13,-16,-44,-73,-100,0],
                [0,0,0,0,0,0,0,100,67,34,1,-32,-66,-100,0],[0,0,0,0,0,0,0,0,100,60,20,-20,-60,-100,0],
                [0,0,0,0,0,0,0,0,0,100,50,1,-50,-100,0],[0,0,0,0,0,0,0,0,0,0,100,33,-34,-100,0],
                [0,0,0,0,0,0,0,0,0,0,0,100,1,-100,0],[0,0,0,0,0,0,0,0,0,0,0,0,100,-100,0],
                [0,0,0,0,0,0,0,0,0,0,0,0,0,100,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,100]],
        'MNAR': [[100,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[100,-100,0,0,0,0,0,0,0,0,0,0,0,0,0],
                 [100,1,-100,0,0,0,0,0,0,0,0,0,0,0,0],[100,33,-34,-100,0,0,0,0,0,0,0,0,0,0,0],
                 [100,50,1,-50,-100,0,0,0,0,0,0,0,0,0,0],[100,60,20,-20,-60,-100,0,0,0,0,0,0,0,0,0],
                 [100,67,34,1,-32,-66,-100,0,0,0,0,0,0,0,0],[100,71,42,13,-16,-44,-73,-100,0,0,0,0,0,0,0],
                 [100,75,50,25,1,-25,-50,-75,-100,0,0,0,0,0,0],[100,78,56,34,12,-10,-32,-54,-77,-100,0,0,0,0,0],
                 [100,80,60,40,20,1,-20,-40,-60,-80,-100,0,0,0,0],[100,82,64,46,28,10,-8,-26,-44,-62,-81,-100,0,0,0],
                 [100,84,67,50,32,14,-3,-20,-36,-52,-68,-84,-100,0,0],[100,85,70,55,40,25,9,-7,-22,-38,-54,-69,-85,-100,0]],
        }
    }
    ]

for mech in mechs:
    for prop in l_prop:
        for pattern in ld_patternbase:
            d = {'patterns': [],
                 'prop': deepcopy(prop)}
            for i, p in enumerate(pattern['pattern']):
                d1 = {'missattr': deepcopy(p),
                      'freq': deepcopy(pattern['freq'][i]),
                      'mech': deepcopy(mech),
                      'type': 'sigmoid-right'
                     }
                if pattern['weights'][mech] == None:
                    d1['weights'] = None
                else:
                    d1['weights'] = deepcopy(pattern['weights'][mech][i])
                
                d['patterns'].append(d1)
            dmvisim['mvisim']['amputer'].append(d)


with open(YAMLOUT, 'w') as ymlout:
    yaml.dump(dmvisim, ymlout)