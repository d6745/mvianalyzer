'''
Automates creation of mvisim-Part for a config
'''

import yaml
from copy import deepcopy
YAMLOUT = r'D:\Downloads\x_mvisim.yaml' # <- CHANGE PATH

dmvisim = {'mvisim': {
            'amputer': []
        }
    }

l_prop = [0.1, 0.3, 0.5, 0.7, 0.9]

ld_patternbase = [{
    'pattern': [[0]],
    'freq': [1],
    'weights': {
        'MCAR': None,
        'MAR': [[0,100,50,-50,-100,0]],
        'MNAR': [[100,0,0,0,0,0]],
        }
    },
    {
    'pattern': [[1,2,3,4]],
    'freq': [1],
    'weights': {
        'MCAR': None,
        'MAR': [[100,0,0,0,0,-100]],
        'MNAR': [[0,100,50,-50,-100,0]],
    }
    },
    {
    'pattern': [[0],[1],[2],[3],[4]],
    'freq': [0.2,0.2,0.2,0.2,0.2],
    'weights': {
        'MCAR': None,
        'MAR': [[0,100,50,-50,-100,0],[100,0,50,-50,-100,0],[100,50,0,-50,-100,0],[100,50,-50,0,-100,0],[100,50,-50,-100,0,0]],
        'MNAR': [[100,0,0,0,0,0],[0,100,0,0,0,0],[0,0,100,0,0,0],[0,0,0,100,0,0],[0,0,0,0,100,0]],
    }
    },
    {
    'pattern': [[0],[0,1],[0,1,2],[0,1,2,3],[0,1,2,3,4]],
    'freq': [0.2,0.2,0.2,0.2,0.2],
    'weights': {
        'MCAR': None,
        'MAR': [[0,100,50,-50,-100,0],[0,0,100,50,-100,0],[0,0,0,100,-100,0],[0,0,0,0,100,0],[0,0,0,0,0,100]],
        'MNAR': [[100,0,0,0,0,0],[100,-100,0,0,0,0],[100,50,-100,0,0,0],[100,50,-50,-100,0,0],[100,50,1,-50,-100,0]],
    }
    },
    ]

for mech in ['MCAR', 'MAR', 'MNAR']:
    for prop in l_prop:
        for pattern in ld_patternbase:
            d = {'patterns': [],
                 'prop': deepcopy(prop)}
            for i, p in enumerate(pattern['pattern']):
                d1 = {'missattr': deepcopy(p),
                      'freq': deepcopy(pattern['freq'][i]),
                      'mech': deepcopy(mech),
                      'type': 'sigmoid-right'
                     }
                if pattern['weights'][mech] == None:
                    d1['weights'] = None
                else:
                    d1['weights'] = deepcopy(pattern['weights'][mech][i])
                
                d['patterns'].append(d1)
            dmvisim['mvisim']['amputer'].append(d)


with open(YAMLOUT, 'w') as ymlout:
    yaml.dump(dmvisim, ymlout)