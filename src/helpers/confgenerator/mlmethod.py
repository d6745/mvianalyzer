'''
Automates creation of ml-Part for a config
'''

from copy import deepcopy
import yaml

YAMLOUT = r'D:\Downloads\x_mlmethod.yaml' # <- CHANGE PATH

dmlmethod = {'mlmethod': []
        }

l_mvi = ['original','imputed','zerofill','delete']

l_mlp_hiddendims = [[5], [3,2], [2,2,1], [10], [6,4], [5,3,2], [20], [12,8], [10,6,4]]
#l_som_rowscols = [[5,5], [21,21], [84,84]]
l_som_rowscols = [[3,3], [10,10], [40,40]]

d_mlp_base = {'batch_size': 64,
              'epochs': 10,
              'learning_rate': 0.0001,
              'optimizer': 'Adam',
              'scale_data': True,
              'shuffle_dl': True,
              'target_col': 'class',
              'train_frac': 0.8,
              'use_relu': True,
              'valid_frac': 0.05
    }

d_som_base = {'epochs': 10,
            'learning_rate': 0.5,
            'learning_rate_dec': 0.1,
            'nb_rng_max': 5,
            'scaler': 'standard',
            'sigma_dec': 0.1,
            'sigma_sq': 1,
            'target_col': 'class',
            'train_frac': 0.8,
            'valid_frac': 0.05
    }

for method in ['mlp', 'som']:
    if method == 'mlp':
        for hd in l_mlp_hiddendims:
            for mvi in l_mvi:
                d_mlp_base['hidden_dims'] = deepcopy(hd)
                d = {'method': deepcopy(method),
                     'mvi': deepcopy(mvi),
                     'methodsettings': deepcopy(d_mlp_base)
                     }
                dmlmethod['mlmethod'].append(d)
    elif method == 'som':
        for rc in l_som_rowscols:
            for mvi in l_mvi:
                d_som_base['map_rows'] = deepcopy(rc[0])
                d_som_base['map_cols'] = deepcopy(rc[1])
                d = {'method': deepcopy(method),
                     'mvi': deepcopy(mvi),
                     'methodsettings': deepcopy(d_som_base)
                     }
                dmlmethod['mlmethod'].append(d)
    else:
        raise AssertionError 
    
with open(YAMLOUT, 'w') as ymlout:
    yaml.dump(dmlmethod, ymlout)