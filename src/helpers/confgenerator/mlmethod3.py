'''
Automates creation of ml-Part for a config
'''

from copy import deepcopy
import yaml

YAMLOUT = r'D:\Downloads\x_mlmethod.yaml' # <- CHANGE PATH

dmlmethod = {'mlmethod': []
        }

l_mvi = ['original','imputed','zerofill','delete']

hidden_dims = 15
hidden_dims = 10_5
hidden_dims = 7_5_3
hidden_dims = 92
hidden_dims = 61_31
hidden_dims = 46_30_16
hidden_dims = 184
hidden_dims = 122_62
hidden_dims = 92_61_31


l_mlp_hiddendims = [[15],[10,5],[7,5,3],[92],[61,31],[46,30,16],[184],[122,62],[92,61,31]]
l_som_rowscols = [[5,5], [17,17], [70,70]]
target_col = 'statezip'

d_mlp_base = {'batch_size': 64,
              'epochs': 10,
              'learning_rate': 0.01,
              'optimizer': 'Adam',
              'scale_data': True,
              'shuffle_dl': True,
              'target_col': target_col,
              'train_frac': 0.8,
              'use_relu': True,
              'valid_frac': 0.05
    }

d_som_base = {'epochs': 10,
            'learning_rate': 0.5,
            'learning_rate_dec': 0.1,
            'nb_rng_max': 5,
            'scaler': 'standard',
            'sigma_dec': 0.1,
            'sigma_sq': 1,
            'target_col': target_col,
            'train_frac': 0.8,
            'valid_frac': 0.05
    }

for method in ['mlp', 'som']:
    if method == 'mlp':
        for hd in l_mlp_hiddendims:
            for mvi in l_mvi:
                d_mlp_base['hidden_dims'] = deepcopy(hd)
                d = {'method': deepcopy(method),
                     'mvi': deepcopy(mvi),
                     'methodsettings': deepcopy(d_mlp_base)
                     }
                dmlmethod['mlmethod'].append(d)
    elif method == 'som':
        for rc in l_som_rowscols:
            for mvi in l_mvi:
                d_som_base['map_rows'] = deepcopy(rc[0])
                d_som_base['map_cols'] = deepcopy(rc[1])
                d = {'method': deepcopy(method),
                     'mvi': deepcopy(mvi),
                     'methodsettings': deepcopy(d_som_base)
                     }
                dmlmethod['mlmethod'].append(d)
    else:
        raise AssertionError 
    
with open(YAMLOUT, 'w') as ymlout:
    yaml.dump(dmlmethod, ymlout)