import random

def randlist(start, stop, num):
    '''
    Create a random shuffled list without double values
    '''
    listids = [*range(start, stop, 1)]
    random.shuffle(listids)
    res = sorted(listids[0:num])
    return res
