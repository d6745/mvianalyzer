import pandas as pd

def alignDtypes(origDf: pd.DataFrame, df: pd.DataFrame):
    '''
    Aligns the Dtypes in Df with Dtypes from OrigDf with respect to
    - Rounding Errors for Int-Values (e.g. 0.9999999 becomes int = 0)
    - NaN-Values in Columns
    '''
    df2 = df.copy(deep=True)
    for c, v in origDf.items():
        if 'int' in str(v.dtype):
            #print(f'COL {c} - {df2[c].isnull().any()}')
            if not df2[c].isnull().any():
                #print(f'COL {c}')
                df2[c] = df[c].round(0).astype(v.dtype)
            else:
                df2[c] = df[c].round(0).astype(pd.Int64Dtype())
        else:
            df2[c] = df[c].astype(v.dtype)
    
    return df2 
    