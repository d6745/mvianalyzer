'''
Manipulates a given Config File by changing specific methodsettins for mlmethod
'''

from helpers.filenames import filepathsuffix
import os
import yaml
import copy

FILEDIR = r'..\..\data\tests\SOM_Architecture' # <- CHANGE PATH
FILEIN = r'conf_00_som_lr_sigma_comparison.yaml' # <- CHANGE PATH

fpin = os.fsencode(os.path.join(FILEDIR, FILEIN))
fpout = filepathsuffix(fpin, suffix='_mod', newExt='yaml')

with open(os.fsdecode(fpin), 'r') as ymlin:
    confin = yaml.safe_load(ymlin)
    
templ = confin['mlmethod'][0]
#print(confin)
confin.pop('mlmethod', None)
confout = confin


d_list = []
for lr in [1, 0.5, 0.3, 0.1]:
    for lr_dec in [0.1, 0.01]:
        for sigma in [2, 1, 0.5, 0.1]:
            for sigma_dec in [0.1, 0.01]:
                template = copy.deepcopy(templ)
                template['methodsettings']['learning_rate'] = lr 
                template['methodsettings']['learning_rate_dec'] = lr_dec
                template['methodsettings']['sigma_sq'] = sigma
                template['methodsettings']['sigma_dec'] = sigma_dec
                d_list.append(template)

confout['mlmethod'] = d_list

with open(os.fsdecode(fpout), 'w') as ymlout:
    yaml.safe_dump(confout, ymlout)
