import yaml
import pandas as pd
from helpers.filenames import filepathsuffix
import os
import sys

def mlresultsconverter(mlresultsfile: str):

    '''
    Converts a given mlresults File into an overview 
    '''

    fileout = filepathsuffix(fp=os.fsencode(mlresultsfile), suffix='_overview', newExt='csv')
    with open(mlresultsfile) as f:
        lines = f.readlines()
        
    print(len(lines))
    print(type(lines))
    
    startrun = False
    runid = -1
    runs = []
    runlines = []
    d = None
    for lineid, l in enumerate(lines):
        if l[0:3] == '---':
            if d is not None:
                runs.append(d)
            runid += 1
            d = {'runid': runid,
                 'lines': []}
        d['lines'].append(l)
    runs.append(d)
    
    #Get MLConfigs
    l_mlconfs = []
    ld_mlconfs = []
    mlconfid = 0
    print(len(runs))
    for i, r in enumerate(runs):
        mlconf = yaml.load(r['lines'][1], Loader=yaml.Loader)
        if not mlconf in l_mlconfs:
            d_mlconf = {'mlconfid': mlconfid,
                        'mlconf': mlconf}
            mlconfid += 1
            l_mlconfs.append(mlconf)
            ld_mlconfs.append(d_mlconf)
        for mc in ld_mlconfs:
            if mlconf == mc['mlconf']:
                r['mlconf'] = mc
    
    #Get ImpConfs
    l_impconfs = []
    ld_impconfs = []
    impconfid = 0
    print(len(runs))
    for i, r in  enumerate(runs):
        if r['mlconf']['mlconf']['mvi'] == 'imputed':
            impconf = yaml.load(r['lines'][2], Loader=yaml.Loader)
            if not impconf in l_impconfs:
                d_impconf = {'impconfid': impconfid,
                             'impconf': impconf}
                impconfid += 1
                l_impconfs.append(impconf)
                ld_impconfs.append(d_impconf)
            for ic in ld_impconfs:
                if impconf == ic['impconf']:
                    r['impconf'] = ic
        else:
            d_impconf = {'impconfid': -1,
                             'impconf': None}
            r['impconf'] = d_impconf
    
        
    #Get AmpConfs (written for mvi=delete or zerofill)
    l_ampconfs = []
    ld_ampconfs = []
    ampconfid = 0
    print(len(runs))
    for i, r in enumerate(runs):
        if r['mlconf']['mlconf']['mvi'] == 'zerofill':
            ampconf = yaml.load(r['lines'][2], Loader=yaml.Loader)
            if not ampconf in l_ampconfs:
                d_ampconf = {'ampconfid': ampconfid,
                             'ampconf': ampconf}
                ampconfid += 1
                l_ampconfs.append(ampconf)
                ld_ampconfs.append(d_ampconf)
                
    last_mlconfid = 0
    ampid = -1
    print(len(runs))
    for i, r in enumerate(runs):
        mlconfid = r['mlconf']['mlconfid']
        if r['mlconf']['mlconf']['mvi'] == 'imputed':
            if mlconfid == last_mlconfid:
                if r['impconf']['impconfid'] == 0:
                    ampid += 1
            else:
                last_mlconfid = mlconfid
                ampid = -1
                if r['impconf']['impconfid'] == 0:
                    ampid += 1
            r['ampconf'] = ld_ampconfs[ampid]
        
        if r['mlconf']['mlconf']['mvi'] == 'zerofill':
            if mlconfid == last_mlconfid:
                ampid += 1
            else:
                last_mlconfid = mlconfid
                ampid = 0
            r['ampconf'] = ld_ampconfs[ampid]
            
        if r['mlconf']['mlconf']['mvi'] == 'delete':
            if mlconfid == last_mlconfid:
                ampid += 1
            else:
                last_mlconfid = mlconfid
                ampid = 0
            r['ampconf'] = ld_ampconfs[ampid]
        
        if r['mlconf']['mlconf']['mvi'] == 'original':
            d_ampconf = {'ampconfid': -1,
                         'ampconf': None}
            r['ampconf'] = d_ampconf
        #print(f'run = {i}\tMLConfId = {r["mlconf"]["mlconfid"]}\tImpConfId = {r["impconf"]["impconfid"]}\tAmpConfId = {r["ampconf"]["ampconfid"]}')
    
    ld = []
    for i, r in enumerate(runs):
        d = {'run': i,
             'mlconfid': r['mlconf']['mlconfid'],
             'impconfid': r['impconf']['impconfid'],
             'ampconfid': r['ampconf']['ampconfid'],
             'method': r['mlconf']['mlconf']['method'],
             'mvi': r['mlconf']['mlconf']['mvi']}
        
        
        if r['mlconf']['mlconf']['method'] == 'mlp':
            d['methodsettings'] = r['mlconf']['mlconf']['methodsettings']['hidden_dims']
        elif r['mlconf']['mlconf']['method'] == 'som':
            d['methodsettings'] = r['mlconf']['mlconf']['methodsettings']['map_cols']
        
        if r['ampconf']['ampconfid'] >= 0:
            d['ampmech'] = r['ampconf']['ampconf']['patterns'][0]['mech']
            d['ampprop'] = r['ampconf']['ampconf']['prop']
            
        if r['impconf']['impconf'] is not None:
            d['impmethod'] = r['impconf']['impconf'][0]['method']
            if r['impconf']['impconf'][0]['method'] == 'multiplemlp':
                d['impmethodsettings'] = r['impconf']['impconf'][0]['methodsettings']['hidden_dims']
        else:
            d['impmethod'] = r['mlconf']['mlconf']['mvi']
        
        for j, l in enumerate(r['lines']):
            ll = l.split('\t')
            if ll[0] == 'TEST':
                for k, v in enumerate(ll):
                    if v == 'EPOCH':
                        epoch = ll[k+1]
                    if v == 'F1Macro':
                        f1macro = ll[k+1]
                    if v == 'Acc':
                        acc = ll[k+1]
                if epoch == '9':
                    d['epoch'] = epoch
                    d['f1macro'] = f1macro
                    d['acc'] = acc
        
        ld.append(d)
    
    df = pd.DataFrame(ld)
    df.to_csv(os.fsdecode(fileout), sep='\t')
    '''
    #for i, r in  enumerate(runs):
    #    if i < 100:
    #        print(f'run = {i}\tMLConfId = {r["mlconf"]["mlconfid"]}\tImpConfId = {r["impconf"]["impconfid"]}\tAmpConfId = {r["ampconf"]["ampconfid"]}')
    '''
    return fileout

if __name__ == '__main__':
    MLRESULTSFILE = r'..\09_PseudoData_MLResult.csv' # <- CHANGE PATH
    if len(sys.argv) == 2:
        mlresfileout = mlresultsconverter(sys.argv[1])
        print(f'FINISHED {mlresfileout}')
    else:
        print('CANCELED - Use Filename as Arg')





