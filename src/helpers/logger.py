import logging
import sys
import os
from helpers import basedir
import datetime

def initLogger(loglevel:str = 'info', target: str = 'file') -> bytes:
    '''
    Initialize the use logger
    '''

    dt = datetime.datetime.now()
    logpath = os.path.join(basedir.getBaseDir(), 'log')
    logfilepath = os.path.join(logpath, 'mvianalyzer_' + dt.strftime('%Y%m%d_%H%M%S') + '.log')
    
    if (loglevel == 'debug'):
        ll = logging.DEBUG
    elif (loglevel == 'info'):
        ll = logging.INFO
    elif (loglevel == 'warning'):
        ll = logging.WARNING
    elif (loglevel == 'error'):
        ll = logging.ERROR
    else:
        ll = logging.INFO
    
    if target == 'file':
        logging.basicConfig(force=True,
                            level=ll,
                            format='%(asctime)s - %(levelname)s - %(module)s : %(message)s',
                            handlers=[
                                logging.FileHandler(os.fsdecode(logfilepath))
                            ])
    else:
        logging.basicConfig(force=True,
                            level=ll,
                            format='%(asctime)s - %(levelname)s - %(module)s : %(message)s',
                            handlers=[
                                logging.FileHandler(os.fsdecode(logfilepath)),
                                logging.StreamHandler(sys.stdout)
                            ])
    logging.info('Initialized Logger for LogFile ' + os.fsdecode(logfilepath))
    return logfilepath
    
if __name__ == '__main__':
    print(initLogger())
    logging.info('TESTMESSAGE')
    