'''
Checks if R_HOME Directory for use with rpy2 is set as environment variable and if exists
'''

import os
import logging
from datamodel.config.ConfigGlobal import ConfigGlobal

def check_r_home(cg: ConfigGlobal):
    env_var = dict(os.environ)
    if 'R_HOME' in env_var.keys():
        msg = 'Existing Environment Variable found for R_HOME in ' + env_var['R_HOME']
        logging.info(msg)
    else:
        msg = 'No R_HOME Environment Variable found. Will be set from Global Configuration to ' + cg.getRHome()
        logging.warning(msg)
        input(msg)
        os.environ['R_HOME'] = cg.getRHome()
