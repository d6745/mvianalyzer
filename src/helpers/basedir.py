import os
import logging

def getBaseDir():
    '''
    Returns current MVIAnalyzer BaseDir
    '''
    bdir = os.getcwd()
    bn = os.path.basename(bdir)
    i = 0
    while bn != 'MVIAnalyzer':
        i += 1
        bdir = os.path.abspath(os.path.join(bdir, os.pardir))
        bn = os.path.basename(bdir)
        if i > 100:
            msg = 'BaseDir not found. Check your current working Dir. Must be from MVIAnalyzer'
            logging.error(msg)
            raise OverflowError(msg)
    return bdir
        
    
if __name__ == '__main__':
    print(getBaseDir())