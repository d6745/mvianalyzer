import os

def filepathsuffix(fp: bytes, suffix: str, newExt: str=None) -> bytes:
    '''
    Removes file extension from filepath, adds a Suffix with new FileExtension
    If newExt is None the old will be used 
    '''
    folder, fname = os.path.split(fp)
    fnamewoext = os.path.splitext(fname)[0]
    if newExt is None:
        newExt = os.fsdecode(os.path.splitext(fname)[1]).replace('.','')
    fnameNew = os.fsdecode(fnamewoext) + suffix + '.' + newExt
    fpout = os.path.join(folder, os.fsencode(fnameNew))
    return fpout
    
