import torch
import logging

def torchdevice(dev: str = 'cpu'):
    '''
    Returns the torch device instance
    '''

    if dev == 'cuda':
        if torch.cuda.is_available():
            tdev = torch.device('cuda')
        else:
            tdev = torch.device('cpu')
            msg = 'Cuda Device not available. Using CPU.'
            logging.warn(msg)
            input(f'{msg} - PRESS TO CONTINUE...')
    else:
        tdev = torch.device('cpu')
    logging.info(f'Using Device {tdev}.')
    return tdev