import logging
from mviexecutor.MVIBase import MVIBase
from fancyimpute import IterativeImputer
import pandas as pd
from dataanalyzer.DataStructure import DataStructure
from datamodel.config.ConfigGlobal import ConfigGlobal


class MiceMVI(MVIBase):
    '''
    WARNING: IGNORES COLSTOAPPLY AND COLSTOUSE
    Performs a MICE MVI based on IterativeImputer from FancyImputer
    '''


    def __init__(self, df: pd.DataFrame, amp_df: pd.DataFrame, ds: DataStructure, mviconf: dict, cg: ConfigGlobal):
        logging.info('Start Init')
        super().__init__(df, amp_df, ds, mviconf, cg)

        #self.__cols = list(set(self._colstoapply) | set(self._colstouse))   #Set Union (Vereinigungsmenge) of ColsToapply and ColsToUse -> KNN always on all Cols mentioned!
        #self.__colsnot = list(set(self._df.columns.tolist()) - set(self.__cols))   #Cols not use including cols not to apply for dropping from Df for KNN Imputer
        self.__imputer = IterativeImputer()
        
        logging.info('End Init')

    def train(self):
        pass
    
    def test(self):
        pass
    
    def predict(self) -> pd.DataFrame:
        imp = self.__imputer.fit_transform(self._df)   #Returns a Numpy Array
        
        #Because self.__cols and thus self.__colsnot is not always in correct order like in Df
        #e.g. set union is ['A','B',C'] self.__cols can be ['B','C','A']
        #but cols in imp-returned Array are still in correct order like in dropped df
        #we need to iterrate through all cols in Df, check if they are in self.__cols and then from ordered
        #cols in imp-Np-Array take the values and replace them in Df        
        i = 0
        for i, col in enumerate(self._df.columns):
            self._df[col] = imp[:,i]
        return self._df