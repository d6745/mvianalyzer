import logging
import pandas as pd
from mviexecutor.MVIBase import MVIBase
from dataanalyzer.DataStructure import DataStructure
from datamodel.config.ConfigGlobal import ConfigGlobal

class MedianMVI(MVIBase):
    '''
    Performs a Median MVI by replacing all NaNs in Df by Median Value of Attribute
    '''

    def __init__(self, df: pd.DataFrame, amp_df: pd.DataFrame, ds: DataStructure, mviconf: dict, cg: ConfigGlobal):
        logging.info('Start Init')
        super().__init__(df, amp_df, ds, mviconf, cg)
        logging.info('End Init')
        
    def train(self):
        pass
    
    def test(self):
        pass
    
    def predict(self) -> pd.DataFrame:
        for col in self._colstoapply:
            m = self._df[col].median(skipna = True)
            self._df.loc[self._df[col].isna(), col] = m 
        return self._df
