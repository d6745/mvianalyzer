from abc import ABC, abstractmethod
import pandas as pd
import logging
from dataanalyzer.DataStructure import DataStructure
from datamodel.config.ConfigGlobal import ConfigGlobal

class MVIBase(ABC):
    '''
    Abstract Base Class for all MVI Methods 
    '''

    def __init__(self, df: pd.DataFrame, amp_df: pd.DataFrame, ds: DataStructure, mviconf: dict, cg: ConfigGlobal):
        '''
        df: Pandas Dataframe with Missing Values
            Should be a copy of amputed Df for later comparison because values will be directly imputed
        amp_df: NaN-Masked Dataframe with Column patternid as returned from Amputer.getPatternIds()
        ds: DataStructure from DataModel OriginalData
        mviconf: Dict with MVIExecutor settings from ConfigSession
        cg: Global Config
        '''
        logging.info('Start Init')
        self._df = df
        self._mviconf = mviconf
        self._amp_df = amp_df
        self._ds = ds
        self._cg = cg
        
        self._colstoapply = self._mviconf['colstoapply']
        if len(self._colstoapply) == 0:     #If empty List of ColToApply use it for all Columns
            self._colstoapply = self._df.columns.tolist()
        
        self._colstouse = self._mviconf['colstouse']
        if len(self._colstouse) == 0:    #If empty List of ColToUse use it for all Columns
            self._colstouse = self._df.columns.tolist()

        logging.info('End Init')
    
    @abstractmethod
    def train(self):
        pass
    
    @abstractmethod
    def test(self):
        pass
    
    @abstractmethod
    def predict(self) -> pd.DataFrame:
        pass
        