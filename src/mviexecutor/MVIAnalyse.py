import logging
from datamodel.data.ImputedData import ImputedData
from datamodel.data.OriginalData import OriginalData
from sklearn.metrics._regression import mean_squared_error
from sklearn.metrics._classification import accuracy_score
from helpers.aligndtypes import alignDtypes
import pandas as pd
import numpy as np
import pickle
from datamodel.data.MissingData import MissingData

class MVIAnalyse():
    '''
    Calculates and stores analysis data about imputed data in comparison with original data
    '''


    def __init__(self, impd: ImputedData, misd: MissingData, orgd: OriginalData):
        '''
        Parameters:
        impd: Instance of ImputedData Class
        misd: Instance of MissingData Class according to Imputed Data (needs Missing Matrix)
        orgd: Instance of OriginalData Class
        '''
        logging.debug('Start Init')
        self.__id = impd
        self.__md = misd
        self.__mm = misd.getMissingMatrix()
        self.__od = orgd
        self.__df_idaligned = alignDtypes(origDf=self.__od.getDf(), df=self.__id.getDf())
        logging.debug('End Init')
        
       
    def getAnalyse(self) -> pd.DataFrame:
        '''
        Returns a DataFrame with MSE and ACC for all cases of Aligned and OnlyNan
        '''
        dl = []
        dl.append(self.getMSE(aligned=True, onlyNan=True))
        dl.append(self.getMSE(aligned=True, onlyNan=False))
        dl.append(self.getMSE(aligned=False, onlyNan=True))
        dl.append(self.getMSE(aligned=False, onlyNan=False))
        
        dl.append(self.getRMSE(aligned=True, onlyNan=True))
        dl.append(self.getRMSE(aligned=True, onlyNan=False))
        dl.append(self.getRMSE(aligned=False, onlyNan=True))
        dl.append(self.getRMSE(aligned=False, onlyNan=False))
        
        dl.append(self.getNRMSE(aligned=True, onlyNan=True))
        dl.append(self.getNRMSE(aligned=True, onlyNan=False))
        dl.append(self.getNRMSE(aligned=False, onlyNan=True))
        dl.append(self.getNRMSE(aligned=False, onlyNan=False))

        dl.append(self.getNRMSE2(aligned=True, onlyNan=True))
        dl.append(self.getNRMSE2(aligned=True, onlyNan=False))
        dl.append(self.getNRMSE2(aligned=False, onlyNan=True))
        dl.append(self.getNRMSE2(aligned=False, onlyNan=False))
        
        dl.append(self.getAccuracy(aligned=True, onlyNan=True))
        dl.append(self.getAccuracy(aligned=True, onlyNan=False))
        dl.append(self.getAccuracy(aligned=False, onlyNan=True))
        dl.append(self.getAccuracy(aligned=False, onlyNan=False))
        
        df = pd.DataFrame(dl[0], index=[0])
        for i, d in enumerate(dl):
            if i > 0:
                df = pd.concat(objs=[df, pd.DataFrame(d, index=[i])])
        
        return df
        
    
    def getMSE(self, aligned: bool = True, onlyNan: bool = True) -> dict:
        '''
        Calculates Mean Squared Error for each Col of OriginalDf and ImputedDf
        If aligned=True the Dtype-Aligned Df of ImputedData is used
        Returns a Dict with Keys=Colnames and Values=MSE-Value
        '''
        d = {'score': 'MSE',
             'aligned': aligned,
             'onlyNan': onlyNan}
        for colname, ods in self.__od.getDf().items():
            #Perform DType Alignment of Original and Imputed Series
            if aligned:
                ids = self.__df_idaligned[colname].astype('float64')
            else:
                ids = self.__id.getDf()[colname].astype('float64')
            
            #Return only Rows from Series with initially Missing Values (Missing Matrix = 1)
            if onlyNan:
                ods = ods[self.__mm[colname]==1].astype('float64')
                ids = ids[self.__mm[colname]==1].astype('float64')
            
            d[colname + '_nans'] = self.__mm[colname].sum()
            
            if ods.empty:   #If no Missing Values in current Series
                mse = -1
            elif ids.isna().any():  #An empty Imputed Dataset can be returned if the MVIExecutor Method in Pipeline is enabled=False. This yields to error in mse-Function
                mse = -1
            else:
                mse = mean_squared_error(y_true=ods, y_pred=ids, squared=True)
            d[colname] = np.round(mse, 3)
            
        d['config'] = str(self.__id.getConfig()),
        return d
    
    def getAccuracy(self, aligned: bool = True, onlyNan: bool = True) -> dict:
        '''
        Calculates Accuracy
        See also getMSE
        '''
        d = {'score': 'ACC',
             'aligned': aligned,
             'onlyNan': onlyNan}
        for colname, ods in self.__od.getDf().items():
            if aligned:
                ids = self.__df_idaligned[colname].astype('int64')
            else:
                ids = self.__id.getDf()[colname].astype('int64')
            
            if onlyNan:
                ods = ods[self.__mm[colname]==1].astype('int64')
                ids = ids[self.__mm[colname]==1].astype('int64')
            
            d[colname + '_nans'] = self.__mm[colname].sum()
            
            if ods.empty:
                acc = -1
            else:
                try:
                    acc = accuracy_score(y_true=ods, y_pred=ids, normalize=True)
                except ValueError:  #Accuracy_Score can handle only Integer Values (Labels)
                    acc = np.nan
            d[colname] = np.round(acc*100, 2)
        d['config'] = str(self.__id.getConfig()),
        return d
    
    def getRMSE(self, aligned: bool = True, onlyNan: bool = True) -> dict:
        '''
        Calculates Root Mean Squared Error for each Col of OriginalDf and ImputedDf
        If aligned=True the Dtype-Aligned Df of ImputedData is used
        Returns a Dict with Keys=Colnames and Values=MSE-Value
        '''
        d = {'score': 'RMSE',
             'aligned': aligned,
             'onlyNan': onlyNan}
        for colname, ods in self.__od.getDf().items():
            #Perform DType Alignment of Original and Imputed Series
            if aligned:
                ids = self.__df_idaligned[colname].astype('float64')
            else:
                ids = self.__id.getDf()[colname].astype('float64')
            
            #Return only Rows from Series with initially Missing Values (Missing Matrix = 1)
            if onlyNan:
                ods = ods[self.__mm[colname]==1].astype('float64')
                ids = ids[self.__mm[colname]==1].astype('float64')
            
            d[colname + '_nans'] = self.__mm[colname].sum()
            
            if ods.empty:   #If no Missing Values in current Series
                mse = -1
                rmse = -1
            elif ids.isna().any():  #An empty Imputed Dataset can be returned if the MVIExecutor Method in Pipeline is enabled=False. This yields to error in mse-Function
                rmse = -1
            else:
                #mse = mean_squared_error(y_true=ods, y_pred=ids, squared=True)
                #rmse = np.sqrt(mse)
                rmse = mean_squared_error(y_true=ods, y_pred=ids, squared=False)
            
            d[colname] = np.round(rmse, 3)
            
        d['config'] = str(self.__id.getConfig()),
        return d
    
    def getNRMSE(self, aligned: bool = True, onlyNan: bool = True) -> dict:
        '''
        Calculates Normalized Root Mean Squared Error for each Col of OriginalDf and ImputedDf
        If aligned=True the Dtype-Aligned Df of ImputedData is used
        Returns a Dict with Keys=Colnames and Values=MSE-Value
        '''
        d = {'score': 'NRMSE',
             'aligned': aligned,
             'onlyNan': onlyNan}
        for colname, ods in self.__od.getDf().items():
            #Perform DType Alignment of Original and Imputed Series
            if aligned:
                ids = self.__df_idaligned[colname].astype('float64')
            else:
                ids = self.__id.getDf()[colname].astype('float64')
            
            #Return only Rows from Series with initially Missing Values (Missing Matrix = 1)
            if onlyNan:
                ods = ods[self.__mm[colname]==1].astype('float64')
                ids = ids[self.__mm[colname]==1].astype('float64')
            
            d[colname + '_nans'] = self.__mm[colname].sum()
            
            if ods.empty:   #If no Missing Values in current Series
                nrmse = -1
            elif ids.isna().any():  #An empty Imputed Dataset can be returned if the MVIExecutor Method in Pipeline is enabled=False. This yields to error in mse-Function
                nrmse = -1
            else:
                mse = mean_squared_error(y_true=ods, y_pred=ids, squared=True)
                #rmse = np.sqrt(mse)
                rmse = mean_squared_error(y_true=ods, y_pred=ids, squared=False)
                nrmse = rmse / (np.max(ods) - np.min(ods))
                if mse < 0:
                    msg = f'ODS = {ods} - IDS = {ids} - mse = {mse} - rmse = {rmse} - nrmse = {nrmse}'
                    logging.error(msg)
            d[colname] = np.round(nrmse, 3)
            
        d['config'] = str(self.__id.getConfig()),
        return d
    
    def getNRMSE2(self, aligned: bool = True, onlyNan: bool = True) -> dict:
        '''
        Calculates Normalized Root Mean Squared Error for each Col of OriginalDf and ImputedDf
        Uses for Delta Calculation of OriginalData all values instead of only Nan Values in NRMSE if only_nan = True
        For only_nan=False returns same as NRMSE
        If aligned=True the Dtype-Aligned Df of ImputedData is used
        Returns a Dict with Keys=Colnames and Values=MSE-Value
        '''
        d = {'score': 'NRMSE2',
             'aligned': aligned,
             'onlyNan': onlyNan}
        for colname, ods in self.__od.getDf().items():
            #Perform DType Alignment of Original and Imputed Series
            if aligned:
                ids = self.__df_idaligned[colname].astype('float64')
            else:
                ids = self.__id.getDf()[colname].astype('float64')
            
            #Return only Rows from Series with initially Missing Values (Missing Matrix = 1)
            if onlyNan:
                ods_nan = ods[self.__mm[colname]==1].astype('float64')
                ids = ids[self.__mm[colname]==1].astype('float64')
            else:
                ods_nan = ods
            
            d[colname + '_nans'] = self.__mm[colname].sum()
            
            if ods_nan.empty:   #If no Missing Values in current Series
                nrmse2 = -1
            elif ids.isna().any():  #An empty Imputed Dataset can be returned if the MVIExecutor Method in Pipeline is enabled=False. This yields to error in mse-Function
                nrmse2 = -1
            else:
                mse = mean_squared_error(y_true=ods_nan, y_pred=ids, squared=True)
                #rmse = np.sqrt(mse)
                rmse = mean_squared_error(y_true=ods_nan, y_pred=ids, squared=False)
                nrmse2 = rmse / (np.max(ods) - np.min(ods))
                if mse < 0:
                    msg = f'ODS = {ods} - IDS = {ids} - mse = {mse} - rmse = {rmse} - nrmse = {nrmse} - nrmse2 = {nrmse2}'
                    logging.error(msg)
            d[colname] = np.round(nrmse2, 3)
            
        d['config'] = str(self.__id.getConfig()),
        return d
        
if __name__ == '__main__':
    # CHANGE PATH ->
    with open(r'D:\Data\od.pkl', 'rb') as inp:
        od = pickle.load(inp)
    with open(r'D:\Data\md.pkl', 'rb') as inp:
        md = pickle.load(inp)
    with open(r'D:\Data\id_0.pkl', 'rb') as inp:
        id0 = pickle.load(inp)
    with open(r'D:\Data\id_1.pkl', 'rb') as inp:
        id1= pickle.load(inp)
    print(md)
    ma0 = MVIAnalyse(id0, md, od)
    ma0.getAnalyse().to_csv(r'D:\Data\temp0.csv', sep='\t')
    ma1 = MVIAnalyse(id1, md, od)
    ma1.getAnalyse().to_csv(r'D:\Data\temp1.csv', sep='\t')