import logging
import pandas as pd
from datamodel.data.DataModel import DataModel
from mviexecutor.MVIBase import MVIBase
from datamodel.data.ImputedData import ImputedData
from mviexecutor.mean.MeanMVI import MeanMVI
from mviexecutor.median.MedianMVI import MedianMVI
from mviexecutor.knn.KnnMVI import KnnMVI
#from pandas.core.frame import DataFrame
from mviexecutor.multiplemlp.MultipleMLPMVI import MultipleMLPMVI
from dataanalyzer.DataStructure import DataStructure
from datamodel.config.ConfigGlobal import ConfigGlobal
from helpers.aligndtypes import alignDtypes
from mviexecutor.MVIAnalyse import MVIAnalyse
from mviexecutor.mice.MiceMVI import MiceMVI

class MVIRunner(object):
    '''
    Execution Class to run MVI on DataModel with one or multiple of MVIBase Method(s) according to
    ConfigSession settings. 
    '''


    def __init__(self, dm: DataModel):
        '''
        Args:
        dm: data.DataModel Object with amputed Data
        '''
        logging.info('Start Init')
        self.__dm = dm
        self.__mviconf = self.__dm.getConfigSession().getMVIExecutor()
        logging.info('End Init')
        
    def __mvicatalog(self) -> dict:
        '''
        Dict with Base Information on all implemented MVI Methods
        '''
        c = {'mvimethods': [
                    {'name': 'mean',
                     'type': 'statistical',
                     'hasmodelfile': False,
                     'info': 'Replace Missing Values by Mean of Attribute'},
                    {'name': 'median',
                     'type': 'statistical',
                     'hasmodelfile': False,
                     'info': 'Replace Missing Values by Median of Attribute'},
                    {'name': 'knn',
                     'type': 'ml',
                     'hasmodelfile': False,
                     'info': 'Replace Missing Values with KNN Imputer from FancyImpute'},
                    {'name': 'mice',
                     'type': 'ml',
                     'hasmodelfile': False,
                     'info': 'Replace Missing Values with IterativeImputer from FancyImpute'},
                    {'name': 'multiplemlp',
                     'type': 'ann',
                     'hasmodelfile': True,
                     'info': 'Replace Missing Values with Multiple MLP Regressors/Classificators for each Missing Pattern'}
                 ]
            }
        return c
    
    def __getMVIBaseImpl(self, df: pd.DataFrame, amp_df: pd.DataFrame, ds: DataStructure, mviconf: dict, cg: ConfigGlobal) -> MVIBase:
        '''
        Returns an instantiaded implementation of MVIBase based on the method-Entry from Config
        '''
        method = mviconf['method']
        if method == 'mean':
            return MeanMVI(df=df, amp_df=amp_df, ds=ds, mviconf=mviconf, cg=cg)
        elif method == 'median':
            return MedianMVI(df=df, amp_df=amp_df, ds=ds, mviconf=mviconf, cg=cg)
        elif method == 'knn':
            return KnnMVI(df=df, amp_df=amp_df, ds=ds, mviconf=mviconf, cg=cg)
        elif method == 'mice':
            return MiceMVI(df=df, amp_df=amp_df, ds=ds, mviconf=mviconf, cg=cg)
        elif method == 'multiplemlp':
            return MultipleMLPMVI(df=df, amp_df=amp_df, ds=ds, mviconf=mviconf, cg=cg)
        else:
            msg = 'MVI Method ' + method + ' not found. Check ConfigSession.'
            logging.error(msg)
            raise NameError(msg)
            return None
        
    
    def getMVICatalog(self) -> dict:
        return self.__mvicatalog()
    
    def runMVI(self) -> DataModel:
        '''
        For each defined MVI Pipeline from ConfigSession execute according MVIs
        and get according entry from MVICatalog 
        '''
        
        missingdata_total = len(self.__dm.getDataContent().getMissingData())
        mviconf_total = len(self.__mviconf)
        all_total = missingdata_total * mviconf_total
        
        ds = self.__dm.getDataStructure()
        for i, md in enumerate(self.__dm.getDataContent().getMissingData()):  #Runs for each amputed Data
            amp_df = md.getAmputer().getPatternIds()
            for j, pipeline in enumerate(self.__mviconf):     #Runs for each Pipeline with current amputed Data
                dfimp = md.getDf().copy(deep=True)    #Copy amputed Df to perform imputation on it
                for k, mviconf in enumerate(pipeline['pipeline']):   #Runs all methods from current Pipeline on SAME Df from current amputed Data
                    if mviconf['enabled']:
                        print(f'Running MVI {(mviconf_total*i)+j}/{all_total} on amputed Data {i} with Pipeline {j} and Method {k}')
                        logging.info(f'Running MVI {(mviconf_total*i)+j}/{all_total} on amputed Data {i} with Pipeline {j} and Method {k}')
                        mvibase = self.__getMVIBaseImpl(dfimp, amp_df, ds, mviconf, self.__dm.getConfigGlobal())
                        mvibase.train()
                        mvibase.test()
                        dfimp = mvibase.predict()
                        #dfimp = alignDtypes(origDf=self.__dm.getDataContent().getOriginalData().getDf(), df=dfimp)
                impd = ImputedData(dfimp, pipeline['pipeline'])
                ma = MVIAnalyse(impd=impd, misd=md, orgd=self.__dm.getDataContent().getOriginalData())
                impd.setMVIAnalyseDf(ma.getAnalyse())
                md.addImputedData(impd)  #Adding IMputed Data only after running all methods from pipeline
        return self.__dm