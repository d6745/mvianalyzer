from torch import nn

class MLPModel(nn.Module):
    '''
    Creates an MLP Model with Linear Layers and Optional ReLu Layers based on Model Architecture in Constructor
    '''

    def __init__(self, in_dim: int, hidden_dims: list, out_dim: int, use_relu: bool):
        '''
        WARNING: Returns always only logits. For Classificator Softmax Activation must be applied on returned Logits
        in_dim: int - Number of Neurons in Input Layer
        hidden_dims: List of Ints - Number of Neurons for each hidden Layer (Number of Hidden Layers is according to len of lilst)
        out_dim: int - Number of Neurons on Output layer
        use_relu: Boolean - Add a ReLu Activation Function after each Linear Layer
        '''
        super().__init__()
        
        self.__counter = 0
        
        if len(hidden_dims) == 0:
            self.__linear_layer_stack = nn.Sequential(
                nn.Linear(in_features=in_dim, out_features=out_dim))
        else:
            self.__linear_layer_stack = nn.Sequential(
                nn.Linear(in_features=in_dim, out_features=hidden_dims[0]))
            if use_relu:
                self.__linear_layer_stack.append(nn.ReLU())
                #self.__linear_layer_stack.append(nn.Sigmoid())
            for i, hd in enumerate(hidden_dims):
                if i > 0:
                    self.__linear_layer_stack.append(
                        nn.Linear(in_features=hidden_dims[i-1], out_features=hidden_dims[i]))
                    if use_relu:
                        self.__linear_layer_stack.append(nn.ReLU())
                        #self.__linear_layer_stack.append(nn.Sigmoid())
            self.__linear_layer_stack.append(
                nn.Linear(in_features=hidden_dims[i], out_features=out_dim))
            
    def forward(self, x):
        out = self.__linear_layer_stack(x)
        out = out.squeeze(1)
        return out