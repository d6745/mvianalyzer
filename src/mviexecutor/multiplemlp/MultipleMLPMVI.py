import logging
from mviexecutor.MVIBase import MVIBase
import pandas as pd
import numpy as np
from dataanalyzer.DataStructure import DataStructure
from datamodel.data.DfDataset import DfDataset
import torch
from torch.utils.data.dataloader import DataLoader
#import matplotlib.pyplot as plt
from datamodel.config.ConfigGlobal import ConfigGlobal
from mviexecutor.multiplemlp.MLPModel import MLPModel
from helpers.torchdevice import torchdevice

#GLOBAL CONFIG SETTINGS
#DEVICE = 'cuda' #'cpu'

#METHODSETTINGS
#TRAIN_FRAC = 0.8   #Fraction of Data from complete Data Instances to use for Training
#VALID_FRAC = 0.05  #Fraction of Data for Validation calculated based on rest of TEST_FRAC = 1-TRAIN_FRAC
#SCALE_DATA = True  #Scale Data-Part with StandardScaler
#BATCH_SIZE = 64
#SHUFFLE_DL = True  #Shuffle Data in Dataloader
#HIDDEN_DIMS = [256,128,64] #[16] #[256,128,64]
#USE_RELU = True #False #True
#EPOCHS = 10
#LOSS_FUNCTION = 'mse'   #'l1'  #For Classificator Type CrossEntropyLoss is set automatically in __prepareModel
#OPTIMIZER = 'Adam' #'SGD' 'RMSProp'
#LEARNING_RATE = 1e-4   




class MultipleMLPMVI(MVIBase):
    '''
    WARNING: coltoapply from Session Config is ignored! Only colstouse is applied to input Df
    Performs a Multiple MLP MVI by estimating missing values with a MLP Regressor or Classificator for each Missing Pattern
    
    Uses all complete instances from Df for Train and Test. For each existing Missing Pattern a specific model is trained
    by setting the Missing Value Columns as Target Columns. Model differs based on current Missing Pattern
    '''

    def __init__(self, df: pd.DataFrame, amp_df: pd.DataFrame, ds: DataStructure, mviconf: dict, cg: ConfigGlobal):
        #TODO: Check Usage of saved model files
        logging.info('Start Init')
        super().__init__(df, amp_df, ds, mviconf, cg)
        self.__ms = mviconf['methodsettings']    #Dictionary for Method Settings from MVIExecutor Pipeline Settings in ConfigSession
        self.__dfred = self.__dropcols()
        self.__mplist = self.__splitMissingPatterns()
        self.__device = torchdevice(cg.getDevice())
        
        self.__prepareDataLoaders()
        self.__prepareModels()
        logging.info('End Init')
        
    def train(self):
        for d in self.__mplist:
            try:
                if d['patternid'] != -1:
                    logging.info(f'Running Train/Test for PatternId={d["patternid"]} with MLPType={d["mlptype"]}:')
                    for epoch in range(self.__ms['epochs']):
                        #RUN THE TRAINING
                        d['model'].train()
                        running_loss = 0
                        for batch, (X, y) in enumerate(d['traindl']):
                            d['optimizer'].zero_grad()   # 3. Optimizer zero grad
                            y_logits = d['model'](X) # model outputs raw logits
                            loss = d['lossfct'](y_logits, y.squeeze())
                            loss.backward()
                            d['optimizer'].step()
                            if d['usesoftmax']:
                                y_pred = torch.softmax(y_logits, dim=1).argmax(dim=1) # go from logits -> prediction probabilities -> prediction labels
                            else:
                                y_pred = y_logits
                            
                            running_loss += loss.item()
                            #print(f'TRAIN - Epoche {epoch} - Batch {batch} - Loss Batch: {loss:.5f}, Acc Batch: {acc_batch:.2f}%')
                        
                        train_loss=running_loss/len(d['traindl'])
                        logging.info(f'TRAIN - EPOCH {epoch} - Loss: {train_loss:.5f}') #, Acc: {acc:.2f}%')
                        
                        #RUN THE TEST
                        d['model'].eval()
                        running_loss = 0
                        for batch, (X,y) in enumerate(d['testdl']):
                            with torch.inference_mode():
                                y_logits = d['model'](X)
                                if d['usesoftmax']:
                                    y_pred = torch.softmax(y_logits, dim=1).argmax(dim=1)
                                else:
                                    y_pred = y_logits
                                loss = d['lossfct'](y_logits, y.squeeze())
                                running_loss += loss.item()
                        
                        test_loss=running_loss/len(d['testdl'])
                        logging.info(f'TEST - EPOCH {epoch} - Loss: {test_loss:.5f}') #, Acc: {acc:.2f}%')
            except:
                msg = 'ERROR IN TRAINING/TEST FOR' + str(d)
                logging.error(msg)
                #input(msg)
    
    def test(self):
        #Test is included in train function
        pass
    
    def predict(self) -> pd.DataFrame:
        '''
        Impute all Missing Values for each PatternId based on the according trained model
        '''
        for d in self.__mplist:
            try:
                if d['patternid'] != -1:
                    logging.info(f'Running Prediction for PatternId={d["patternid"]} with MLPType={d["mlptype"]}:')
                    d['model'].eval()
                    #Before starting Prediction we need to check if the available Data is scaled or not and if necessary apply the Data Scaler from Train Dataset
                    if self.__ms['scale_data']:
                        scaler_data = d['trainds'].getScalerData()
                        data = torch.tensor(scaler_data.transform(d['df'].drop(d['missattr'], axis=1).values), dtype=torch.float, device=self.__device, requires_grad=False)
                    else:
                        data = torch.tensor(d['df'].drop(d['missattr'], axis=1).values, dtype=torch.float, device=self.__device, requires_grad=False)
                    
                    #Perform Prediction for Logits
                    target_logits = d['model'](data)
                    
                    #For Classifier apply Softmax to find Class with Max Probability
                    if d['usesoftmax']:
                        target_pred = torch.softmax(target_logits, dim=1).argmax(dim=1)
                    else:
                        target_pred = target_logits
    
                    #Apply Back-Transformation from Target Scaler if necessary
                    #If we got only a 1D-Array (x,) back we need to reshape it first to a 2D-Array (x,1)
                    #to apply the target scaler. To be unique we use reshape for each 1D-Array 
                    if d['mlptype'] == 'classificator':    #Check for Target Scaling
                        #target_pred_renormed = target_pred.cpu().numpy()
                        if target_pred.dim() == 1:
                            target_pred_renormed = target_pred.detach().cpu().numpy().reshape(-1,1)
                        else:
                            target_pred_renormed = target_pred.detach().cpu().numpy()
                    else:
                        scaler_target = d['trainds'].getScalerTarget()
                        if target_pred.dim() == 1:
                            t = target_pred.detach().cpu().numpy().reshape(-1,1) 
                        else:
                            t = target_pred.detach().cpu().numpy()
                        target_pred_renormed = scaler_target.inverse_transform(t)
                        
                    #Go through each Missing Attribute and replace NaN-Values in Df by predicted
                    #Values from the returned npArray
                    #We check also if the initial Column Type is int and round the returned values with 0 decimals
                    #(at least necessary if a regressor is used but we apply for all)
                    d['df_pred'] = d['df'].copy(deep=True)
                    for i in range(len(d['missattr'])):
                        x = target_pred_renormed[:,i]
                        if (self._ds.checkIntDTypeForColname(d['missattr'][i])):
                            x = np.rint(x)
                        d['df_pred'][d['missattr'][i]] = x
                    
                    #Insert predicted Values based on Index and Missing Attributes to the complete Df
                    self._df.loc[d['df_pred'].index, d['missattr']] = d['df_pred']
            except:
                msg = 'ERROR IN TRAINING/TEST FOR' + str(d)
                logging.error(msg)
                #input(msg)
        return self._df
    
    def __dropcols(self) -> pd.DataFrame:
        '''
        Creates a reduced Dataframe based on self._df containing only colstouse as defined in Session Config
        '''
        colsnot = list(set(self._df.columns.tolist()) - set(self._colstouse))
        return self._df.drop(colsnot, axis = 1)
        
    def __splitMissingPatterns(self) -> list :
        '''
        Returns a list of Dicts containing the PatternId and the Df with Data for this Missing Pattern
        '''
        dflist = []
        for pid in self._amp_df['patternid'].drop_duplicates().sort_values().tolist():
            df_ids = self._amp_df[self._amp_df['patternid'] == pid].index
            df = self.__dfred.iloc[df_ids, :]
            
            #Find Columns with Missing Values and Create List of ColumnNames
            missattr = []    #List of Column Names with Missing Values
            for colname in df.columns:
                if df[colname].isnull().sum() > 0:
                    missattr.append(colname)
            
            #Columns with Complete Attributes
            allattr = df.columns.tolist()
            complattr = [ele for ele in allattr if ele not in missattr]
            
            #Check if Classificator or Regressor needed as MLP Model
            rep = self._ds.getReport()
            if len(missattr) == 0:
                mlptype = 'complete'
            elif len(missattr) == 1:
                iscategorical = rep[rep['column'] == missattr[0]]['categorical']
                if iscategorical.any():
                    mlptype = 'classificator'
                else:
                    mlptype = 'regressor'
            else:
                mlptype = 'regressor'
            
            dfdict = {'patternid': pid,
                      'df': df,
                      'missattr': missattr,
                      'complattr': complattr,
                      'mlptype': mlptype}
            dflist.append(dfdict)
        
        return dflist
    
    def __prepareDataLoaders(self):
        '''
        Adds to the Missing-Patterns-List self.__mplist from self.__splitMissingPatterns the DataSets and DataLoaders
        from all complete Data Instances for each Missing Pattern Id based on the Missing Attributes
        '''
        for d in self.__mplist:
            if d['patternid'] == -1:
                train_df = d['df'].sample(frac=self.__ms['train_frac'], axis=0, random_state=self._cg.getRandomSeed())   # get random sample
                test_df = d['df'].drop(index=train_df.index)          # get everything but the train sample
                valid_df = test_df.sample(frac=self.__ms['valid_frac'], random_state=self._cg.getRandomSeed())
                test_df = test_df.drop(valid_df.index)
        
        for d in self.__mplist:
            #If we use a Classificator we dont need to scale the target values as these will be directly used as target classes
            #and the used CrossEntropyLoss can directly work on the output-Neurons.
            #Because in this case we use also CrossEntropyLoss the Dtype must be set to Int
            if d['mlptype'] == 'classificator':
                scale_target = False
                target_dtype = torch.int64
            else:
                scale_target = True
                target_dtype = torch.float
                
            if d['patternid'] != -1:
                #Scaler will be initialized on Train Data and is None
                #The Scaler from Train Data will later be reused for Test- and Valid-Data
                train_ds = DfDataset(df=train_df, target_cols=d['missattr'], device=self.__device, 
                                     scale_data=self.__ms['scale_data'], scale_target=scale_target,
                                     scaler_data=None, scaler_target=None,
                                     target_dtype=target_dtype)
                test_ds = DfDataset(df=test_df, target_cols=d['missattr'], device=self.__device,
                                    scale_data=self.__ms['scale_data'], scale_target=scale_target,
                                    scaler_data=train_ds.getScalerData(), scaler_target=train_ds.getScalerTarget(),
                                    target_dtype=target_dtype)
                d['trainds'] = train_ds
                d['testds'] = test_ds
                d['traindl'] = DataLoader(dataset=train_ds, batch_size=self.__ms['batch_size'], shuffle=self.__ms['shuffle_dl'])
                d['testdl'] = DataLoader(dataset=test_ds, batch_size=self.__ms['batch_size'], shuffle=self.__ms['shuffle_dl'])

    def __prepareModels(self):
        '''
        Adds to the Missing Pattern List self.__mplist a MLP-Model based on MLPType Regressor or Classificator
        '''
        for d in self.__mplist:
            if d['patternid'] != -1:
                #If we use a Classificator the number of output-Dims is the same like the number of different values
                #(only applicable for 1 Attribute)
                if d['mlptype'] == 'classificator':
                    #num_classes = d['trainds'].getDf()[d['missattr']].drop_duplicates().shape[0]
                    #We need to use Unique values from OriginalData instead of TrainDs because due to shuffle some values may not be available in TrainDs
                    num_classes = self._ds.getStructureForColname(d['missattr'][0])['uniqueValues'].values[0]  #Get first value for this Series
                    d['usesoftmax'] = True
                else:
                    #For Regressor we use a Output-Neuron for each Missing Attribute
                    num_classes = len(d['missattr'])
                    d['usesoftmax'] = False
                
                #Get Loss Function according to MLP Type
                if d['mlptype'] == 'classificator':
                    d['lossfct'] = torch.nn.CrossEntropyLoss()
                else:
                    if self.__ms['loss_function'] == 'mse':
                        d['lossfct'] = torch.nn.MSELoss()
                    else:
                        d['lossfct'] = torch.nn.L1Loss()
                    
                    
                #Init the Model
                d['model'] = MLPModel(in_dim=len(d['complattr']), hidden_dims=self.__ms['hidden_dims'], 
                                      out_dim=num_classes, use_relu=self.__ms['use_relu']).to(self.__device)
                
                #Get Optimizer from Settings
                if self.__ms['optimizer'] == 'Adam':
                    d['optimizer'] = torch.optim.Adam(params=d['model'].parameters(), lr=self.__ms['learning_rate'])
                elif self.__ms['optimizer'] == 'SGD':
                    d['optimizer'] = torch.optim.SGD(params=d['model'].parameters(), lr=self.__ms['learning_rate'])
                elif self.__ms['optimizer'] == 'RMSProp':
                    d['optimizer'] = torch.optim.RMSprop(params=d['model'].parameters(), lr=self.__ms['learning_rate'])
                else:
                    d['optimizer'] = torch.optim.Adam(params=d['model'].parameters(), lr=self.__ms['learning_rate'])
                
                
                
        