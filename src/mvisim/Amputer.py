'''
Amputer Function based on
Schouten, R.M.; Lugtig, P.; Vink, G.:  Generating missing values for simulation purposes: a multivariate amputation procedure, Journal of Statistical Computation and Simulation, Bd. 88, Nr. 15, S. 2909–2930, Okt. 2018.

https://rianneschouten.github.io/mice_ampute/vignette/ampute.html
https://www.rdocumentation.org/packages/mice/versions/3.15.0/topics/ampute
'''

import os
import logging
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from helpers.logger import initLogger
from mvisim.ConfigurationErrorMVISim import ConfigurationErrorMVISim
from datamodel.config.ConfigSession import ConfigSession
from mvisim.pyampute import MultivariateAmputation
from mvisim.pyampute.exploration.md_patterns import mdPatterns
from helpers.filenames import filepathsuffix

class Amputer(object):
    '''
    Base Class for Amputing Data from a given Dataframe
    '''
    def __init__(self, df: pd.DataFrame, config: dict, ampid: int=0, rs: int = 0):
        logging.info('Start Init')
        self.__ampid = ampid
        self.__rs = rs
        self.__df = df
        self.__dfcols = self.__df.columns.to_list()
        self.__c = config
        self.__patterns_c = self.__c['patterns']
        self.__prop_c = self.__c['prop']
        
        check = self.__checkConfig()
        if not check[0]:
            for e in check[1]:
                logging.error(e)
            raise ConfigurationErrorMVISim(check[1])
        
        #Create Pattern-DF: Contains same columns as Df and one row for each pattern with 0/1 for missing/observed values
        mm_list = []
        for p in self.__patterns_c:
            mm = [1] * df.shape[1]          #Init List for this pattern with all ones
            if all(isinstance(elem, str) for elem in p['missattr']):        #Convert List of COlnames to List of ColIds
                missattrcolids = [df.columns.get_loc(c) for c in p['missattr']]
            else:
                missattrcolids = p['missattr']
            for i in missattrcolids:
                mm[i] = 0          #Set MissingMatrix Values for Missing Attribute Columns to 0 
            mm_list.append(mm)
        self.__pattern_df = pd.DataFrame(data = mm_list, columns=self.__dfcols)
        
        self.__amp_df = None    #Resulting Amputation DataFrame initialising
        logging.info('End Init')
    
    def getAmputeDf(self) -> pd.DataFrame:
        if self.__amp_df is None:
            msg = 'No Amputation DataFrame found. Call ampute first!'
            logging.error(msg)
            input(msg)
            return None
        else:
            return self.__amp_df
        
    def getAmputeConfig(self) -> dict:
        return self.__c 
    
    def ampute(self) -> pd.DataFrame:
        #Adapt Keys from Config to PyAmpute
        for p in self.__patterns_c:
            p['incomplete_vars'] = p['missattr']
            p['mechanism'] = p['mech']
            p['score_to_probability_func'] = p['type']
        
        ma = MultivariateAmputation(prop=self.__prop_c, patterns=self.__patterns_c, seed=self.__rs)
        self.__amp_df = ma.fit_transform(self.__df)
        
        return self.__amp_df
    
    def getPatternIds(self) -> pd.DataFrame:
        '''
        Returns a Df with all Indices from Data-Dfs and a column with the patternId according to ConfigSession
        The returned Df can be applied to a Data-Df with join to add a PatternId Column
        The returned Df contains also all columns from used Data masked with 1 for existing values and 0 for missing values 
        '''
        #Get PatternId for each row of amp_df by masking it and comparing with Pattern-List-Entries
        amp_mask = self.__amp_df.where(self.__amp_df.isna(), other=1)   #Replace Not-NaNs by 1
        amp_mask = amp_mask.mask(self.__amp_df.isna(), other = 0)       #Replace NaNs by 0
        amp_mask['patternid'] = -99                                     #Add PlaceHolder for PatternId
        amp_mask = amp_mask.apply(np.int32)                             #Convert all 0 and 1 to Integers
        amp_mask.index = amp_mask.index.astype(int)
        self.__pattern_df['patternid'] = self.__pattern_df.index
        
        for idx, row in amp_mask.iterrows():
            for idx2, row2 in self.__pattern_df.iterrows():
                amp_pattern = row.iloc[:-1].values          #Get masked pattern for row in amp_mask without PatternId-Columns
                base_pattern = row2.iloc[:-1].values        #Get Pattern-Values from Original Pattern DF without PatternId-Column
                if np.sum(amp_pattern) == len(amp_pattern):     #If Record is complete
                    amp_mask.loc[idx, 'patternid'] = -1
                elif ((amp_pattern == base_pattern).all()):
                    amp_mask.loc[idx, 'patternid'] = int(row2['patternid'])
        return amp_mask
        
    
    def plotMissing(self, outdir: bytes, plothoriz = True) -> bool:
        if self.__amp_df is None:
            msg = 'No Amputation DataFrame found. Call ampute first!'
            logging.error(msg)
            input(msg)
            return False
        
        amp_mask = self.getPatternIds()
        self.__df = self.__df.join(amp_mask['patternid'])
        self.__amp_df = self.__amp_df.join(amp_mask['patternid'])
        
        num_cols = self.__df.shape[1]
        num_rows = self.__df.shape[0]
        x_plot = np.arange(1, num_rows + 1)
        df1 = self.__df.sort_values(by=['patternid'])
        amp1 = self.__amp_df.sort_values(by=['patternid']) 

        #plt.figure(figsize=(500,200))        
        if plothoriz:
            fig, ax = plt.subplots(1, num_cols-1)
        else: 
            fig, ax = plt.subplots(num_cols-1, 1)
        for i in range(self.__df.shape[1] - 1):
            if df1.iloc[:,i].dtype != 'object':
                if plothoriz:
                    ax[i].barh(x_plot, df1.iloc[:,i], color='red')
                    ax[i].scatter(df1.iloc[:,i], x_plot, color='red')
                    ax[i].barh(x_plot, amp1.iloc[:,i], color='#00ff0a')
                    ax[i].scatter(amp1.iloc[:,i], x_plot, color='#00ff0a')
                    ax[i].set_ylabel('Datensatz')
                    ax[i].set_xlabel('Wert')
                else:
                    ax[i].bar(x_plot, df1.iloc[:,i], color='red')
                    ax[i].scatter(x_plot, df1.iloc[:,i], color='red')
                    ax[i].bar(x_plot, amp1.iloc[:,i], color='#00ff0a')
                    ax[i].scatter(x_plot, amp1.iloc[:,i], color='#00ff0a')
                    ax[i].set_xlabel('Datensatz')
                    ax[i].set_ylabel('Wert')
                ax[i].set_title(df1.iloc[:,i].name)
                ax[i].grid()
        
        sfx = '_amp{:>03}'.format(str(self.__ampid))
        fp = filepathsuffix(fp=outdir, suffix=sfx, newExt='png')
        plt.gcf().set_size_inches(20, 5)
        plt.tight_layout()
        plt.savefig(os.fsdecode(fp), dpi=300)
        logging.info('Exported Amputer-Diagram to ' + os.fsdecode(fp))
        plt.close()
        #plt.show()
        
        #Finally needs to remove again Col patternid for not to use in further Analysis e.g. MVI
        self.__amp_df.drop(['patternid'], axis=1, inplace=True)
        return True
    
    def plotMissingPatterns(self, outdir: bytes) -> bool:
        if self.__amp_df is None:
            msg = 'No Amputation DataFrame found. Call ampute first!'
            logging.error(msg)
            input(msg)
            return False
        mdp = mdPatterns()
        patterns, plotfigure = mdp.get_patterns(self.__amp_df, count_or_proportion='count', show_plot=True)
        #print(patterns)
        sfx = '_amp{:>03}_patterns'.format(str(self.__ampid))
        fp = filepathsuffix(fp=outdir, suffix=sfx, newExt='png')
        plt.savefig(fp, dpi=300)
        logging.info('Exported AmputerPatterns-Diagram to ' + os.fsdecode(fp))
        plt.close()
        
        
    
    def __checkConfig(self) -> bool:
        '''
        Checks all configuration settings and return False with List of Causes in case of wrong Configuration
        '''
        check = True
        cause = []
        
        freqs = []
        
        for i, p in enumerate(self.__patterns_c):
            #Warn if Weights not set for MAR or MNAR
            if (p['weights'] == None) and (not p['mech'] == 'MCAR'):
                cause.append(f'AmpID: {self.__ampid} - PatternId: {i} - Weights Vector for MAR/MNAR not set. Specify Weights')
                check = False
            
            #Mechanism must be one of MAR, MCAR, MNAR
            if p['mech'] not in ['MAR', 'MCAR', 'MNAR']:
                cause.append(f'AmpID: {self.__ampid} - PatternId: {i} - Mechanism must be one of MAR, MCAR, MNAR')
                check =False
            
            #Check length of weights vector
            if p['weights'] != None:
                if len(p['weights']) != len(self.__dfcols):
                    cause.append(f'AmpID: {self.__ampid} - PatternId: {i} - Weight Len not identical to Number of columns!')
                    check = False
            
            #Calculate Freq List:
            freqs.append(p['freq'])
            
        #Sum of Frequence Vector should be 1
        if round(sum(freqs), 8) != 1:
            cause.append(f'AmpID: {self.__ampid} - Sum of Frequence Vector should be 1')
            check = False
            
        #Proportion (Missing Rate) must be between 0 and 1
        if self.__prop_c > 1 or self.__prop_c < 0:
            cause.append(f'AmpID: {self.__ampid} - Prop (Missing Rate) must be between 0 and 1')
            check = False
            
        return check, cause


if __name__ == '__main__':
    AMPID = 0
    initLogger()
    cs = ConfigSession()
    cs.setDataFileName('PseudoData.csv')
    df = pd.read_csv(os.fsdecode(cs.getDataFilepath()), sep='\t')
    df = df.drop(labels=['id', 'name'], axis=1)
        
    a = Amputer(df = df, config = cs.getMVISimAmputer()[AMPID], ampid=AMPID)
    df_amp = a.ampute()
    #a.plotMissing()
    
    #for c in cs.getMVISimAmputer():
        #a = Amputer(df = df, config = c)
        #df_amp = a.ampute()
        
    