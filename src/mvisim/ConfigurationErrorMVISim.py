class ConfigurationErrorMVISim(Exception):
    '''
    Exeption raised for wrong configuration settings for MVISim
    '''


    def __init__(self, cause: list):
        self.__msg = ''
        for c in cause:
            self.__msg += '\n' + c
        super().__init__(self.__msg)
        