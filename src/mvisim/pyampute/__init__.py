"""
Library with functions to explore, generate and solve missing data problems
"""

__title__ = "pyampute"
__version__ = "0.0.1"
__author__ = "Rianne Schouten, Davina Zamanzadeh, Prabhant Singh"

from mvisim.pyampute.ampute import MultivariateAmputation
from mvisim.pyampute.utils import ArrayLike, Matrix
