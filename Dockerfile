FROM bitnami/pytorch:1.13.1
USER root

WORKDIR /app/MVIAnalyzer
COPY . .

RUN pip3 install --no-cache-dir -r ./requirements.txt \
    && chmod +x /app/MVIAnalyzer/src/main/Start.py

ENV PYTHONPATH "/app/MVIAnalyzer/src"
ENV MPLCONFIGDIR "/tmp/matplotlib"

ENTRYPOINT "/app/MVIAnalyzer/src/main/Start.py"

VOLUME /app/MVIAnalyzer/data
VOLUME /app/MVIAnalyzer/conf
VOLUME /app/MVIAnalyzer/log
